<?php


class FM_RSVP_options_page
{
    public function __construct()
    {
        add_action( 'rest_api_init', [ 'FM_RSVP_options_page', 'add_custom_routes' ] );
    }

    public function add_custom_routes()
    {
        register_rest_route( 'federmanager/v1', '/get_rsvp_options', array(
			'methods' => 'GET',
			'callback' => ['FM_RSVP_options_page', 'get_federmanager_rsvp_options']
		));
    }

    /**
	 * Endpoint per ricevere le opzioni di customizzazione dell'RSVP
	 * @return array $options
	 */
	public function get_federmanager_rsvp_options( WP_REST_Request $req )
	{
		$options = get_option( 'FM_RSVP_settings' );
		is_wp_error($options) ? wp_send_json_error( 'No options' ) : wp_send_json_success( $options );
	}
}

new FM_RSVP_options_page();

add_action( 'admin_menu', 'FM_RSVP_add_admin_menu' );
add_action( 'admin_init', 'FM_RSVP_settings_init' );

function FM_RSVP_add_admin_menu(  ) { 

	add_options_page( 'Federmanager RSVP Options', 'Federmanager RSVP Options', 'manage_categories', 'federmanager_rsvp_options', 'FM_RSVP_options_page' );

}


function FM_RSVP_settings_init(  ) { 

	register_setting( 'pluginPage', 'FM_RSVP_settings' );

	add_settings_section(
		'FM_RSVP_pluginPage_section', 
		__( 'Customizzazione Modulo Privacy RSVP - Federmanager', 'wordpress' ), 
		'FM_RSVP_settings_section_callback', 
		'pluginPage'
	);

	add_settings_field( 
		'fm_show_privacy_checkbox', 
		__( 'Nascondi il checkbox per la privacy? ', 'wordpress' ), 
		'fm_show_privacy_checkbox_render', 
		'pluginPage', 
		'FM_RSVP_pluginPage_section' 
	);

	add_settings_field( 
		'fm_privacy_label', 
		__( 'Inserisci contentuto della label per la checkbox ', 'wordpress' ), 
		'fm_privacy_label_render', 
		'pluginPage', 
		'FM_RSVP_pluginPage_section' 
	);

	add_settings_field( 
		'fm_participation_checkbox', 
		__( 'Imposta flag automatico su checkbox partecipazione evento ', 'wordpress' ), 
		'fm_participation_checkbox_render', 
		'pluginPage', 
		'FM_RSVP_pluginPage_section' 
    );
    
	add_settings_field( 
		'fm_limits_checkbox', 
		__( 'Imposta alert email automatico quando mancano meno di 6 iscritti ad un evento', 'wordpress' ), 
		'fm_limits_checkbox_render', 
		'pluginPage', 
		'FM_RSVP_pluginPage_section' 
    );
    
    add_settings_field( 
		'fm_alert_email_limits', 
		__( 'Inserisci indirizzo email valido per alert iscritti', 'wordpress' ), 
		'fm_alert_email_limits_render', 
		'pluginPage', 
		'FM_RSVP_pluginPage_section' 
	);

}


function fm_show_privacy_checkbox_render(  ) { 

	$options = get_option( 'FM_RSVP_settings' );
	?>
	<input type='checkbox' name='FM_RSVP_settings[fm_show_privacy_checkbox]' <?php checked( $options['fm_show_privacy_checkbox'], 1 ); ?> value='1'>
	<?php

}


function fm_privacy_label_render(  ) { 

	$options = get_option( 'FM_RSVP_settings' );
	?>
	<input type='text' name='FM_RSVP_settings[fm_privacy_label]' value='<?php echo $options['fm_privacy_label']; ?>'>
	<?php

}

function fm_participation_checkbox_render(  ) { 

	$options = get_option( 'FM_RSVP_settings' );
	?>
	<input type='checkbox' name='FM_RSVP_settings[fm_participation_checkbox]' <?php checked( $options['fm_participation_checkbox'], 1 ); ?> value='1'>
	<?php

}

function fm_limits_checkbox_render(  ) { 

	$options = get_option( 'FM_RSVP_settings' );
	?>
	<input type='checkbox' name='FM_RSVP_settings[fm_limits_checkbox]' <?php checked( $options['fm_limits_checkbox'], 1 ); ?> value='1'>
	<?php

}

function fm_alert_email_limits_render(  ) { 

	$options = get_option( 'FM_RSVP_settings' );
	?>
	<input type='text' name='FM_RSVP_settings[fm_alert_email_limits]' value='<?php echo $options['fm_alert_email_limits']; ?>'>
	<?php

}


function FM_RSVP_settings_section_callback(  ) { 

	echo __( '', 'wordpress' );

}


function FM_RSVP_options_page(  ) { 

	?>
	<form action='options.php' method='post'>

		<h2>Federmanager RSVP Options</h2>

		<?php
		settings_fields( 'pluginPage' );
		do_settings_sections( 'pluginPage' );
		submit_button();
		?>

	</form>
	<?php

}



?>