<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/

class sco_shortcode {
	var $cb_get_option;
	function __construct( $cb_get_option=false ){
		$this->cb_get_option = $cb_get_option;
		
		add_shortcode('social_login', array(&$this,'handle_shortcode'));
	}
	
	function handle_shortcode( $atts, $template='', $code="" ){
		extract(shortcode_atts(array(
			'class' 			=> '',
			'loading_label'		=> __('Log in...', 'rhsco'),
			'twitter_label'		=> __('Twitter', 'rhsco'),
			'facebook_label'	=> __('Facebook', 'rhsco'),
			'linkedin_label'	=> __('LinkedIn', 'rhsco'),
			'googleplus_label'	=> __('Google+', 'rhsco'),
			'microsoft_label'	=> __('Microsoft', 'rhsco'),
			'yahoo_label'		=> __('Yahoo', 'rhsco'),
			'wp_label'			=> __('Login', 'rhsco'),
			'window_close'		=> '1',
			'window_callback'	=> 'default_sco_popup',
			'skip_networks'		=> '',//comma separated networks not to show on this instance.
			'redirect_to'		=> $this->get_option( 'login_redirect_to', site_url('/'), true),
			'login_behavior'	=> $this->get_option( 'sco_login_behavior', 'reload', true),
			'enable_wp_login'	=> '1'
		), $atts));
		$skip_networks = explode(',', str_replace(' ','',$skip_networks) );
		if( '1'!=$enable_wp_login && !in_array('wp',$skip_networks)){
			$skip_networks[]='wp';
		}

		$allowed_network = apply_filters('sco_enabled_network', array() );		
		$out = '';	
		if(is_array($allowed_network) && count($allowed_network)>0 ){
			$has_buttons = false;
			$out.="<div class=\"sco-btn-holder $class\">";
			foreach($allowed_network as $network){
				if(in_array($network,$skip_networks))continue;
				
				if('wp'==$network){
					$out.= sprintf('<div class="sco-btn"><button type="button" class="sco-wp" data-href="%s" data-loading-text="%s"><i class="icon-wp"></i> <span>%s</span></button></div>',
						wp_login_url( ( $login_behavior=='redirect' ? $redirect_to : $_SERVER['REQUEST_URI'] ) ),
						$loading_label,
						$this->get_login_label($wp_label,$network,$atts)
					);
				}else{
					$label = $network.'_label';				
					$out.= sprintf('<div class="sco-btn"><button type="button" class="sco_social_login sco-%s" data-login_bahavior="%s" data-redirect_to="%s" data-network="%s" data-window-close="%s" data-window-callback="%s" data-loading-text="%s"><i class="icon-%s"></i> <span>%s</span></button></div>',
						$network,
						$login_behavior,
						$redirect_to,
						$network,
						$window_close,
						$window_callback,
						$loading_label,
						$network,
						$this->get_login_label((isset($$label)?$$label:sprintf(__('Login with %s', 'rhsco'),$network)),$network,$atts)
					);				
				}
				
				$has_buttons = true;
			}
			$out.='</div>';
			
			if($has_buttons){
				$out.= '<script>jQuery(document).ready(function($){ if( typeof init_sco=="function"){init_sco();} });</script>';
			}	
		}
		
		return $out;
	}
	
	function get_login_label($label, $network, $atts){
		$field = $network.'_label';
		if( isset($atts[$field]) && !empty($atts[$field])) return $label;//use shortcode defined.
		
		$option = $network.'_login_label';
		$option_label = $this->get_option($option,'',true);
		if(!empty($option_label)) return $option_label;//use options label.
		return $label;//use hardcoded default label.
	}
	
	function get_option($name, $default='', $default_if_empty=true){
		return call_user_func( $this->cb_get_option, $name, $default, $default_if_empty );
	}		
}

?>