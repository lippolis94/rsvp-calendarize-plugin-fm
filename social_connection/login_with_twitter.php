<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/
 
if('sco_ajax'==get_class($this)):
	$client_id 		=  $this->get_option('twitter_key','',true);
	$client_secret 	=  $this->get_option('twitter_secret','',true);
	//-----
	if(!class_exists('http_class'))			require( $path.'oauth-api/http.php');
	if(!class_exists('oauth_client_class'))	require( $path.'oauth-api/oauth_client.php');

	$client = new oauth_client_class;
	$client->debug = 1;
	$client->debug_http = 1;
	$client->server = 'Twitter';
	
	$client->redirect_uri 	= $redirect_uri;
	$client->client_id 		= $client_id;
	$client->client_secret 	= $client_secret;

	if(strlen($client->client_id) == 0
	|| strlen($client->client_secret) == 0)
		$this->error_message( __('Settings error, network api credentials not set.','rhsco') );

	if(($success = $client->Initialize())){
		if(($success = $client->Process())){
			if(strlen($client->access_token)){
				$success = $client->CallAPI(
					'https://api.twitter.com/1.1/account/verify_credentials.json', 
					'GET', array(), array('FailOnAccessError'=>true), $user);
			}
		}
		$success = $client->Finalize($success);
	}
	if($client->exit)
		exit;
	if($success){
		$client->ResetAccessToken();
		$SCO_Profile = new SCO_Profile( $network, (array)$user, $this->cb_get_option );
	}else{
		$client->ResetAccessToken();
		$this->error_message( sprintf( __('Error authenticating with social network: %s','rhsco'), HtmlSpecialChars($client->error) ) );
	}
else:
	$this->error_message( __('Settings error, cannot link directly to this file.', 'rhsco') );
endif; 
 


?>