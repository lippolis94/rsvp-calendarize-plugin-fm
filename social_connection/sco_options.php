<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/
if('rhsco_options'==get_class($this)):

		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'rhcsco-settings'; 
		$t[$i]->open 			= $this->open; 
		$t[$i]->label 		= __('General Settings','rhsco');
		$t[$i]->right_label	= __('General Settings','rhsco');
		$t[$i]->page_title	= __('General Settings','rhsco');
		$t[$i]->priority 	= 10;
		$t[$i]->theme_option = true;
		$t[$i]->plugin_option = true;		
		$t[$i]->options = array();
		
		$t[$i]->options[]=(object)array(
				'type'	=> 'subtitle',
				'label'	=> __('Login Behavior','rhsco')
			);	

		$t[$i]->options[] =	(object)array(
				'id'			=> 'sco_login_behavior',
				'label'			=> __('Login behavior','rhsco'),
				'description'	=> sprintf('<p>%s</p><p>%s</p>',
					__('<b>Reload</b>: Page will be reloaded after succesfull login with social network', 'rhsco'),
					__('<b>Redirect to custom url</b>: Page will be redirect to the specified url.', 'rhsco')
				),
				'type'			=> 'select',
				'options'		=> array(
					'reload'	=> __('Reload','rhsco'),
					'redirect'	=> __('Redirect to custom url', 'rhsco')
				),
				'default'		=> 'redirect',
				'hidegroup'		=> '#login_behavior_group',
				'hidevalues'	=> array('redirect'),
				'el_properties'	=> array(),
				'save_option'	=>true,
				'load_option'	=>true
			);
			
		$t[$i]->options[]=(object)array(
				'id'	=> 'login_behavior_group',
				'type'	=> 'div_start'
			);
			
		$t[$i]->options[] =	(object)array(
				'id'			=> 'login_redirect_to',
				'label'			=> __('Default post login redirect URL','rhsco'),
				//'description'	=> __('Specify the default url to redirect the user after social login completes.  This can be overwritten in the shortcode using the redirect_to argument.','rhsco'),
				'default'		=> '',
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);	
			
		$t[$i]->options[]=(object)array(
				'type'	=> 'div_end'
			);					
		$t[$i]->options[]=(object)array(
				'type'	=> 'clear'
			);					
		//---- other settings	
		$t[$i]->options[]=(object)array(
				'type'	=> 'subtitle',
				'label'	=> __('Local profile registration settings','rhsco')
			);		
			
		$t[$i]->options[] =	(object)array(
				'id'			=> 'dummy_email_domain',
				'label'			=> __('Dummy email domain','rhsco'),
				'description'	=> __('When visitors use any of the social networks to register on your website, and a email address is not available the Dummy email domain is used to generate a dummy email, when creating a local user profile.','rhsco'),
				'default'		=> '',
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat','placeholder'=>__('dummydomain.com','rhsco')),
				'save_option'	=>true,
				'load_option'	=>true
			);			
			
		$t[$i]->options[]=(object)array(
				'type'=>'clear'
			);		

		$t[$i]->options[] =	(object)array(
				'id'			=> 'enable_wp_login',
				'label'			=> __('Include a button to local login','rhsco'),
				'description'	=> __('This can be overwritten with the shortcode argument enable_wp_login .','rhsco'),
				'type'			=> 'onoff',
				'default'		=> '1',
				'el_properties'	=> array(),
				'save_option'	=>true,
				'load_option'	=>true
			);			

		$t[$i]->options[] =	(object)array(
				'id'			=> 'wp_login_label',
				'label'			=> __('Login label','rhsco'),
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);	
			
		$t[$i]->options[]=(object)array(
				'type'=>'clear'
			);				

		$t[$i]->options[]=(object)array(
				'type'	=> 'submit',
				'label'	=> __('Save','rhcs'),
				'class' => 'button-primary'
			);	

		//---------------
		$i = count($t);
		$t[$i]=(object)array();
		$t[$i]->id 			= 'rhcsco-layout'; 
		$t[$i]->open 			= $this->open; 
		$t[$i]->label 		= __('Layout Settings','rhsco');
		$t[$i]->right_label	= __('Layout Settings','rhsco');
		$t[$i]->page_title	= __('Layout Settings','rhsco');
		$t[$i]->priority 	= 20;
		$t[$i]->theme_option = true;
		$t[$i]->plugin_option = true;		
		$t[$i]->options = array();
		$t[$i]->options[]=(object)array(
				'type'	=> 'subtitle',
				'label'	=> __('Button Order','rhsco')
			);	
			
		$t[$i]->options[]=(object)array(
				'id'			=> 'network_order',
				'name'			=> 'network_order[]',
				'label'			=> __('Network order','rhsco'),
				'description'	=> sprintf('<p>%s</p><p>%s</p>',
					__('Drag and reorder the network buttons.  Elements on the top are printed first.','rhsco'),
					__('Only enabled networks appear on this tab.','rhsco')
				),
				'type'			=> 'callback',
				'callback'		=> array(&$this,'cb_layout'),
				'save_option'	=>true,
				'load_option'	=>true
			);		

		$t[$i]->options[]=(object)array(
				'type'=>'clear'
			);				

		$t[$i]->options[]=(object)array(
				'type'	=> 'submit',
				'label'	=> __('Save','rhcs'),
				'class' => 'button-primary'
			);	
			
	if( apply_filters('rhsco_used_network',false,'googleplus',$this->id) ):		
		//-- Google+ -----------------------	
		$description = sprintf( __('<p>For more information on how to setup Google+ please click on the Help tab in the upper right hand corner. Make sure you have the latest version of English Help for Calendarize it!</p><b>WEB ORIGIN:</b><br>%s<br><br><b>REDIRECT URI:</b><br>%s<br>','rhsco'), 
			site_url(''),
			$this->get_network_redirect_uri('googleplus')
		);		
					
		$i = count($t);

		$t[$i]=(object)array();
		$t[$i]->id 			= 'rhcsco-googleplus'; 
		$t[$i]->open 		= false; 
		$t[$i]->label 		= __('Google+','rhsco');
		$t[$i]->right_label	= __('Google+','rhsco');
		$t[$i]->page_title	= __('Google+','rhsco');
		$t[$i]->theme_option = true;
		$t[$i]->plugin_option = true;		
		$t[$i]->options = array();	

		$t[$i]->options[]=(object)array(
				'type'	=> 'subtitle',
				'label'	=> __('Google+','rhsco')
			);
		
		$t[$i]->options[] =	(object)array(
				'id'			=> 'googleplus_enable',
				'label'			=> __('Enable Google+','rhsco'),
				//'description'	=> __('Specify a maximun number of taxonomies that a user can create by default.','rhsco'),
				'type'			=> 'onoff',
				'default'		=> '0',
				'hidegroup'		=> '#googleplus_group',
				'el_properties'	=> array(),
				'save_option'	=>true,
				'load_option'	=>true
			);
			
		$t[$i]->options[]=(object)array(
				'id'	=> 'googleplus_group',
				'type'	=> 'div_start'
			);
							
		$t[$i]->options[] =	(object)array(
				'id'			=> 'googleplus_clientid',
				'label'			=> __('Google+ CLIENT ID','rhsco'),
				'description'	=> $description,
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);		
							
		$t[$i]->options[] =	(object)array(
				'id'			=> 'googleplus_apikey',
				'label'			=> __('Google+ CLIENT SECRET','rhsco'),
				//'description'	=> __('Specify a maximun number of taxonomies that a user can create by default.','rhsco'),
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);
			
		$t[$i]->options[] =	(object)array(
				'id'			=> 'googleplus_login_label',
				'label'			=> __('Login label','rhsco'),
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);			
			
		$t[$i]->options[]=(object)array(
				'type'	=> 'div_end'
			);
			
		$t[$i]->options[]=(object)array(
				'type'=>'clear'
			);	

		$t[$i]->options[]=(object)array(
				'type'	=> 'submit',
				'label'	=> __('Save','rhcs'),
				'class' => 'button-primary'
			);				
	endif;		
			
	if( apply_filters('rhsco_used_network',false,'facebook',$this->id) ):	
		//-- Facebook -----------------------	
		
		$i = count($t);

		$t[$i]=(object)array();
		$t[$i]->id 			= 'rhcsco-facebook'; 
		$t[$i]->open 		= false; 
		$t[$i]->label 		= __('Facebook','rhsco');
		$t[$i]->right_label	= __('Facebook','rhsco');
		$t[$i]->page_title	= __('Facebook','rhsco');
		$t[$i]->theme_option = true;
		$t[$i]->plugin_option = true;		
		$t[$i]->options = array();		
		
		$t[$i]->options[]=(object)array(
				'type'	=> 'subtitle',
				'label'	=> __('Facebook','rhsco')
			);
		
		$t[$i]->options[] =	(object)array(
				'id'			=> 'facebook_enable',
				'label'			=> __('Enable Facebook','rhsco'),
				//'description'	=> __('Specify a maximun number of taxonomies that a user can create by default.','rhsco'),
				'type'			=> 'onoff',
				'default'		=> '0',
				'hidegroup'		=> '#facebook_group',
				'el_properties'	=> array(),
				'save_option'	=>true,
				'load_option'	=>true
			);
			
		$t[$i]->options[]=(object)array(
				'id'	=> 'facebook_group',
				'type'	=> 'div_start'
			);
							
		$t[$i]->options[] =	(object)array(
				'id'			=> 'facebook_app_id',
				'label'			=> __('Facebook App id','rhsco'),
				'description'	=> sprintf('<p>%s</p>',
					__('<p>For more information on how to setup Facebook please click on the Help tab in the upper right hand corner. Make sure you have the latest version of English Help for Calendarize it!</p>', 'rhsco')
				),
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);
							
		$t[$i]->options[] =	(object)array(
				'id'			=> 'facebook_app_secret',
				'label'			=> __('Facebook App secret','rhsco'),
				//'description'	=> __('Specify a maximun number of taxonomies that a user can create by default.','rhsco'),
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);

		$t[$i]->options[] =	(object)array(
				'id'			=> 'facebook_login_label',
				'label'			=> __('Login label','rhsco'),
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);	
							
		$t[$i]->options[]=(object)array(
				'type'	=> 'div_end'
			);
			
		$t[$i]->options[]=(object)array(
				'type'=>'clear'
			);	

		$t[$i]->options[]=(object)array(
				'type'	=> 'submit',
				'label'	=> __('Save','rhcs'),
				'class' => 'button-primary'
			);	
	endif;			
		//-- Yahoo -----------------------	
		
		$description = sprintf( __('Please go to Yahoo Apps page https://developer.apps.yahoo.com/projects/ , create a project, set the Consumer key and Consumer secret on this tab. The Callback URL must be %s Make sure you enable the necessary permissions to execute the API calls your application needs.','rhsco'), 
			$this->get_network_redirect_uri('yahoo')
		);		
								
		$i++;

		$t[$i]=(object)array();
		$t[$i]->id 			= 'rhcsco-yahoo'; 
		$t[$i]->open 		= false; 
		$t[$i]->label 		= __('Yahoo','rhsco');
		$t[$i]->right_label	= __('Yahoo settings','rhsco');
		$t[$i]->page_title	= __('Yahoo','rhsco');
		$t[$i]->theme_option = true;
		$t[$i]->plugin_option = true;		
		$t[$i]->options = array();	

		$t[$i]->options[]=(object)array(
				'type'	=> 'subtitle',
				'label'	=> __('Yahoo','rhsco')
			);
		
		$t[$i]->options[] =	(object)array(
				'id'			=> 'yahoo_enable',
				'label'			=> __('Yahoo enable','rhsco'),
				'type'			=> 'onoff',
				'default'		=> '0',
				'hidegroup'		=> '#yahoo_group',
				'el_properties'	=> array(),
				'save_option'	=>true,
				'load_option'	=>true
			);
			
		$t[$i]->options[]=(object)array(
				'id'	=> 'yahoo_group',
				'type'	=> 'div_start'
			);
							
		$t[$i]->options[] =	(object)array(
				'id'			=> 'yahoo_consumer_key',
				'label'			=> __('Consumer Key','rhsco'),
				'description'	=> $description,
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);		
							
		$t[$i]->options[] =	(object)array(
				'id'			=> 'yahoo_consumer_secret',
				'label'			=> __('Yahoo consumer secret','rhsco'),
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);
			
		$t[$i]->options[]=(object)array(
				'type'	=> 'div_end'
			);
			
		$t[$i]->options[]=(object)array(
				'type'=>'clear'
			);	

		$t[$i]->options[]=(object)array(
				'type'	=> 'submit',
				'label'	=> __('Save','rhcs'),
				'class' => 'button-primary'
			);	
		
	if( apply_filters('rhsco_used_network',false,'linkedin',$this->id) ):													
		//-- LinkedIn -----------------------		
		$description = sprintf( __('<p>For more information on how to setup LinkedIn please click on the Help tab in the upper right hand corner. Make sure you have the latest version of English Help for Calendarize it!</p><b>Javascript API domain:</b><br>%s','rhsco'), 
			$this->get_network_redirect_uri('linkedin')
		);
					
		$i = count($t);

		$t[$i]=(object)array();
		$t[$i]->id 			= 'rhcsco-linkedin'; 
		$t[$i]->open 		= false; 
		$t[$i]->label 		= __('LinkedIn','rhsco');
		$t[$i]->right_label	= __('LinkedIn','rhsco');
		$t[$i]->page_title	= __('LinkedIn','rhsco');
		$t[$i]->theme_option = true;
		$t[$i]->plugin_option = true;		
		$t[$i]->options = array();		
			
		$t[$i]->options[]=(object)array(
				'type'	=> 'subtitle',
				'label'	=> __('LinkedIn','rhsco')
			);
		
		$t[$i]->options[] =	(object)array(
				'id'			=> 'linkedin_enable',
				'label'			=> __('Enable LinkeIn','rhsco'),
				//'description'	=> __('Specify a maximun number of taxonomies that a user can create by default.','rhsco'),
				'type'			=> 'onoff',
				'default'		=> '0',
				'hidegroup'		=> '#linkedin_group',
				'el_properties'	=> array(),
				'save_option'	=>true,
				'load_option'	=>true
			);
			
		$t[$i]->options[]=(object)array(
				'id'	=> 'linkedin_group',
				'type'	=> 'div_start'
			);
							
		$t[$i]->options[] =	(object)array(
				'id'			=> 'linkedin_app_key',
				'label'			=> __('LinkedIn App key','rhsco'),
				'description'	=> $description,
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);
							
		$t[$i]->options[] =	(object)array(
				'id'			=> 'linkedin_app_secret',
				'label'			=> __('LinkedIn App secret','rhsco'),
				//'description'	=> __('Specify a maximun number of taxonomies that a user can create by default.','rhsco'),
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);

		$t[$i]->options[] =	(object)array(
				'id'			=> 'linkedin_login_label',
				'label'			=> __('Login label','rhsco'),
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);	
										
		$t[$i]->options[]=(object)array(
				'type'	=> 'div_end'
			);
			
		$t[$i]->options[]=(object)array(
				'type'=>'clear'
			);	
		$t[$i]->options[]=(object)array(
				'type'	=> 'submit',
				'label'	=> __('Save','rhcs'),
				'class' => 'button-primary'
			);	
	
	endif;		
	
	if( apply_filters('rhsco_used_network',false,'microsoft',$this->id) ):
		//-- Microsoft -----------------------		

		$description = sprintf( __('<p>For more information on how to setup Microsoft Live Connect please click on the Help tab in the upper right hand corner. Make sure you have the latest version of English Help for Calendarize it!</p><b>Redirect domain:</b><br>%s','rhsco'), 
			$this->get_network_redirect_uri('microsoft')
		);
		
		$i = count($t);

		$t[$i]=(object)array();
		$t[$i]->id 			= 'rhcsco-microsoft'; 
		$t[$i]->open 		= false; 
		$t[$i]->label 		= __('Microsoft Live','rhsco');
		$t[$i]->right_label	= __('Microsoft Live','rhsco');
		$t[$i]->page_title	= __('Microsoft Live','rhsco');
		$t[$i]->theme_option = true;
		$t[$i]->plugin_option = true;		
		$t[$i]->options = array();	
		
		$t[$i]->options[]=(object)array(
				'type'	=> 'subtitle',
				'label'	=> __('Microsoft Live Connect','rhsco')
			);
		
		$t[$i]->options[] =	(object)array(
				'id'			=> 'microsoft_enable',
				'label'			=> __('Enable Microsoft Live Connect','rhsco'),
				//'description'	=> __('Specify a maximun number of taxonomies that a user can create by default.','rhsco'),
				'type'			=> 'onoff',
				'default'		=> '0',
				'hidegroup'		=> '#microsoft_group',
				'el_properties'	=> array(),
				'save_option'	=>true,
				'load_option'	=>true
			);
			
		$t[$i]->options[]=(object)array(
				'id'	=> 'microsoft_group',
				'type'	=> 'div_start'
			);
							
		$t[$i]->options[] =	(object)array(
				'id'			=> 'microsoft_client_id',
				'label'			=> __('Client id','rhsco'),
				'description'	=> sprintf('<p>%s</pa>',
					$description
				),
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);
							
		$t[$i]->options[] =	(object)array(
				'id'			=> 'microsoft_client_secret',
				'label'			=> __('Client secret','rhsco'),
				//'description'	=> __('Specify a maximun number of taxonomies that a user can create by default.','rhsco'),
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);

		$t[$i]->options[] =	(object)array(
				'id'			=> 'microsoft_login_label',
				'label'			=> __('Login label','rhsco'),
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);	
										
		$t[$i]->options[]=(object)array(
				'type'	=> 'div_end'
			);
			
		$t[$i]->options[]=(object)array(
				'type'=>'clear'
			);	
		$t[$i]->options[]=(object)array(
				'type'	=> 'submit',
				'label'	=> __('Save','rhcs'),
				'class' => 'button-primary'
			);			
	endif;		
			
	if( apply_filters('rhsco_used_network',false,'twitter',$this->id) ):
		//-- Twitter -----------------------	
		$description = sprintf( __('<p>For more information on how to setup Twitter please click on the Help tab in the upper right hand corner. Make sure you have the latest version of English Help for Calendarize it!</p><b>Callback URL:</b><br>%s', 'rhsco'), 
			$this->get_network_redirect_uri('twitter')
		);		
	
		$i = count($t);

		$t[$i]=(object)array();
		$t[$i]->id 			= 'rhcsco-twitter'; 
		$t[$i]->open 		= false; 
		$t[$i]->label 		= __('Twitter','rhsco');
		$t[$i]->right_label	= __('Twitter','rhsco');
		$t[$i]->page_title	= __('Twitter','rhsco');
		$t[$i]->theme_option = true;
		$t[$i]->plugin_option = true;		
		$t[$i]->options = array();	
		
		$t[$i]->options[]=(object)array(
				'type'	=> 'subtitle',
				'label'	=> __('Twitter','rhsco')
			);
							
		$t[$i]->options[] =	(object)array(
				'id'			=> 'twitter_enable',
				'label'			=> __('Enable Twitter','rhsco'),
				//'description'	=> __('Specify a maximun number of taxonomies that a user can create by default.','rhsco'),
				'type'			=> 'onoff',
				'default'		=> '0',
				'hidegroup'		=> '#twitter_group',
				'el_properties'	=> array(),
				'save_option'	=>true,
				'load_option'	=>true
			);
			
		$t[$i]->options[]=(object)array(
				'id'	=> 'twitter_group',
				'type'	=> 'div_start'
			);
							
		$t[$i]->options[] =	(object)array(
				'id'			=> 'twitter_key',
				'label'			=> __('Consumer Key','rhsco'),
				'description'	=> $description,
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);
							
		$t[$i]->options[] =	(object)array(
				'id'			=> 'twitter_secret',
				'label'			=> __('Consumer Secret','rhsco'),
				//'description'	=> __('Specify a maximun number of taxonomies that a user can create by default.','rhsco'),
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);	
				
		$t[$i]->options[] =	(object)array(
				'id'			=> 'twitter_login_label',
				'label'			=> __('Login label','rhsco'),
				'type'			=> 'text',
				'el_properties'	=> array('class'=>'widefat'),
				'save_option'	=>true,
				'load_option'	=>true
			);	
			
		$t[$i]->options[]=(object)array(
				'type'	=> 'div_end'
			);
			
		$t[$i]->options[]=(object)array(
				'type'=>'clear'
			);
			
		$t[$i]->options[]=(object)array(
				'type'	=> 'submit',
				'label'	=> __('Save','rhcs'),
				'class' => 'button-primary'
			);		
	endif;
endif;

?>