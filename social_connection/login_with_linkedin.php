<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/
 
if('sco_ajax'==get_class($this)):
	$client_id 		=  $this->get_option('linkedin_app_key','',true);
	$client_secret 	=  $this->get_option('linkedin_app_secret','',true);
	//-----
	if(!class_exists('http_class'))			require( $path.'oauth-api/http.php');
	if(!class_exists('oauth_client_class'))	require( $path.'oauth-api/oauth_client.php');

	$client = new oauth_client_class;
	$client->debug = 1;
	$client->debug_http = 1;
	$client->server = 'LinkedIn';

	$client->redirect_uri 	= $redirect_uri;
	$client->client_id 		= $client_id;
	$client->client_secret 	= $client_secret;

	/*  API permission scopes
	 *  Separate scopes with a space, not with +
	 */
	$client->scope = 'r_emailaddress';

	if(strlen($client->client_id) == 0
	|| strlen($client->client_secret) == 0)
		$this->error_message( __('Settings error, network api credentials not set.','rhsco') );

	if(($success = $client->Initialize())){
		if(($success = $client->Process())){
			if(strlen($client->access_token)){
				$success = $client->CallAPI(
					'http://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,public-profile-url)', 
					'GET', array(
						'format'=>'json'
					), array('FailOnAccessError'=>true), $user);
			}
		}
		$success = $client->Finalize($success);
	}
	if($client->exit)
		exit;
	if(strlen($client->authorization_error)){
		$client->error = $client->authorization_error;
		$success = false;
	}
	if($success){
		$SCO_Profile = new SCO_Profile( $network, (array)$user, $this->cb_get_option );
		if( intval($SCO_Profile->wp_user_id)==0 ){
			$this->error_message( $SCO_Profile->last_error );
		}
	}else{
		$this->error_message( sprintf( __('Error authenticating with social network: %s','rhsco'), HtmlSpecialChars($client->error) ) );
	}
else:
	$this->error_message( __('Settings error, cannot link directly to this file.', 'rhsco') );
endif; 
 


?>