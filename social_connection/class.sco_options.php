<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/
if(!class_exists('rhsco_options')): 
 
class rhsco_options {
	function __construct( $plugin_id='rhsco', $capability='manage_options', $open=false){
		if(current_user_can($capability)){
			$this->id = $plugin_id;
			$this->open = $open;
			add_filter("pop-options_{$this->id}",array(&$this,'options'),9999,1);		
	
			add_action('pop_head_'.$this->id, array(&$this,'head'), 10);
		}
	}
	
	function get_network_redirect_uri($network){
		switch($network){
			case 'googleplus':
				return sprintf('%s<br>%s',
					admin_url('/admin-ajax.php?action=sco_social_connect&network=googleplus&window_close=0'),
					admin_url('/admin-ajax.php?action=sco_social_connect&network=googleplus&window_close=1')
				);
			case 'microsoft':
				return admin_url('/admin-ajax.php');
		}
		return sprintf('%s', site_url('/') );
	}
	
	function options($t){
		$i = count($t);
		//--  -----------------------		
		include 'sco_options.php';	
		//---------------
		return apply_filters( 'sco_options', $t, $i, $this );	
	}
	
	function head(){	
		wp_print_scripts('jquery-ui-sortable');
?>
<script>
jQuery(document).ready(function($){
	if($('.network_order_holder').sortable){
		$('.network_order_holder').sortable();	
	}
});
</script>
<?php
	}
	
	function cb_layout($tab,$i,$o,&$save_fields){
		include 'map_network_names.php';
		$networks = apply_filters('sco_enabled_network_names',array());
		$str='<div id="" class="network_order_holder">';

		foreach($networks as $network => $label){
			$tpl = '<div class="network_order_item sco-%s">'; 
			$tpl.= '<i class="icon-%s"></i> %s';
			$tpl.= '<input type="hidden" id="network_order_%s" name="network_order[]" value="%s" />';
			$tpl.= '</div>';
			
			$str .= sprintf($tpl,
				$network,
				$network,
				$label,
				$network,
				$network
			);		
		}
		$str.='</div>';
		if(true===$o->save_option){
			$save_fields[]='network_order';	
		}
		return $str;		
		
	}
}

endif;
?>