<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/

$sco_installed_networks = array( 
	'wp' 				=> (object)array(
		'name'		=>	__('Local WordPress', 'rhsco'),
		'option' 	=> 'enable_wp_login'
	), 
	'facebook'			=> (object)array(
		'name'		=> __('Facebook','rhsco'),
		'option'	=> 'facebook_enable'
	),
	'googleplus'		=> (object)array(
		'name'		=> __('Google+','rhsco'),
		'option'	=> 'googleplus_enable'
	),
	'yahoo'				=> (object)array(
		'name'		=> __('Yahoo','rhsco'),
		'option'	=> 'yahoo_enable'
	),  
	'linkedin'			=> (object)array(
		'name'		=> __('LinkedIn','rhsco'),
		'option'	=> 'linkedin_enable'
	),
	'twitter'			=> (object)array(
		'name'		=> __('Twitter','rhsco'),
		'option'	=> 'twitter_enable'
	),
	'microsoft'			=> (object)array(
		'name'		=> __('Microsoft', 'rhsco'),
		'option'	=> 'microsoft_enable'
	)			
);

$sco_installed_networks = apply_filters( 'get_sco_installed_networks', $sco_installed_networks );
?>