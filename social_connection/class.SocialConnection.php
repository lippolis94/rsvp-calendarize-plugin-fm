<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/

class SocialConnection {
	var $version = '1.1.0';
	var $api=false;
	var $options=false;
	function __construct( $args=array() ){
		//------------
		$defaults = array(
			'id'				=> 'rhsco',
			'plugin_id'			=> 'rhsco',
			'plugin_code'		=> 'RHSCO',
			'menu_id'			=> 'rhsco',
			'options_varname'	=> 'rhsco_options',
			'options_capability'=> 'manage_options',
			'option_menu_parent'=> 'options-general.php',
			'options_open'		=> false,
			'post_type'			=> '',
			'path'				=> dirname(__FILE__),
			'url'				=> plugins_url('/',__FILE__),
			'cb_get_option'		=> false,
			'user_list'			=> false,
			'stylesheet'		=> '',
			'page_title'		=> __('Social Connection','rhsco'),
			'menu_text'			=> __('Social Connection','rhsco'),
			'pop_path'			=> '',
			'pop_url'			=> '',
			'networks'			=> array('googleplus','facebook','linkedin','microsoft','twitter'),
			'wp_admin'			=> false,
			'wp_login'			=> false,
			'layout'			=> 'vertical',
			'language_path'		=> dirname( plugin_basename( __FILE__ ) ).'/languages/'
		);
		foreach($defaults as $property => $default){
			$this->$property = isset($args[$property])?$args[$property]:$default;
		}

		if( did_action('after_setup_theme') ){
			$this->after_setup_theme_7();
		}else{
			add_action('after_setup_theme', array(&$this,'after_setup_theme_7'), 7 );	
		}	
		
		$this->id = md5( $this->id.$this->option_menu_parent );
		$this->networks = is_array($this->networks)?$this->networks:array();		
		//-----------	
		$this->user_list = '1'==$this->get_option('enable_sco_user_list','1',true)?true:false;
		//-----------		
		add_action('rhsco_load_once', array(&$this,'rhsco_load_once') );
		do_action('rhsco_load_once');	

		$this->init_options_panel();
	
		add_filter('rhsco_used_network', array(&$this,'rhsco_used_network'),10,3);
	}

	function after_setup_theme_7(){
		$res =	load_plugin_textdomain('rhsco', null, $this->language_path );		
	}
	
	function rhsco_used_network($default,$network,$plugin_id){
		if( $default || ( $this->id!=$plugin_id) ) return $default;
		if( empty($this->networks) ) return true;

		return ( empty($this->networks) ) ? true : in_array($network,$this->networks) ;
	}
	
	function init_options_panel(){
		if( is_admin() && apply_filters( 'rhsco_init_pop_'.$this->option_menu_parent, true) ){
			//only run PluginOptionsPanelModule per parent, else will show duplicated menu.
			//------- 
			add_filter('rhsco_init_pop_'.$this->option_menu_parent, create_function('','return false;'));
					
			require_once $this->path.'/class.sco_options.php';
			new rhsco_options( $this->id, $this->options_capability, $this->options_open );
						
			do_action('rh-php-commons');	

			if(class_exists('PluginOptionsPanelModule')){
				$settings = array(				
					'id'					=> $this->id,
					'plugin_id'				=> $this->id,
					'capability'			=> $this->options_capability,
					'options_varname'		=> $this->options_varname,
					'menu_id'				=> $this->menu_id,
					'page_title'			=> translate( $this->page_title, 'rhsco' ),
					'menu_text'				=> translate( $this->menu_text, 'rhsco' ),
					'option_menu_parent'	=> $this->option_menu_parent,
					'stylesheet'			=> $this->stylesheet,
					'option_show_in_metabox'=> true,
					'path'					=> $this->pop_path,
					'url'					=> $this->pop_url,
					'autoupdate'			=> false,
					'layout'				=> $this->layout
				);

				new PluginOptionsPanelModule($settings);			
			}
			//-------	
		}
	}
	
	function rhsco_load_once(){
		if( is_admin() ){
			add_action('pop_head_'.$this->id, array(&$this,'pop_head'));
		}
	
		if( intval(did_action('rhsco_load_once')) > 1 )return;
		//-----	load shortcode
		require_once 'class.sco_shortcode.php';
		new sco_shortcode( array( &$this, 'get_option' ) );	
				
		//load option
		if( is_admin() ){
			//extra css for options
			
			add_action( 'admin_enqueue_scripts', array(&$this,'admin_enqueue_scripts') );	
			
			if($this->user_list){
				require_once $this->path.'/class.sco_user_list.php';
				new sco_user_list();
			}	
		}	
	
		add_action( 'wp_enqueue_scripts', array( &$this, 'wp_enqueue_scripts') );
		if( $this->wp_admin ){
			add_action( 'admin_enqueue_scripts', array( &$this, 'wp_enqueue_scripts') );
		}
		if( $this->wp_login ){
			add_action( 'login_init', array( &$this, 'wp_enqueue_scripts') );
		}	

		//------ cb_login callback ajax --- 2nd part of the social login----
		require_once 'class.sco_ajax.php';
		new sco_ajax( array( &$this, 'get_option' ) );
	}
	
	function pop_head(){
		wp_print_styles('pop-sco');
	}
	
	function admin_enqueue_scripts(){
		wp_register_style( 'pop-sco', $this->url.'pop.css', array(),'1.0.0');
	}
	
	function wp_enqueue_scripts(){
		wp_enqueue_script( 'sco', $this->url.'social_connection_options.js', array('jquery'),'1.0.3');
		wp_localize_script('sco', 'SCO', array(
			'siteurl' 		=> get_site_url(),
			'ajaxurl'		=> admin_url('/admin-ajax.php'),
			'timer'			=> null,
			'child_window'	=> null,
			'btn'			=> null,
			'complete'		=> 'sco_complete'
		));    
	}
	
	function get_social_connection_settings(){
		$settings = array();
		foreach( array( 'twitter_key', 'twitter_secret', 'googleplus_apikey', 'googleplus_clientid', 'facebook_app_id', 'linkedin_app_key' ) as $option){
			$option_name = $option;
			//some typos in the varname mapping:
			switch($option){
				case 'googleplus_apikey':
					$option_name = 'googleplus_apiKey';
					break;
				case 'googleplus_clientid':
					$option_name = 'googleplus_clientId';
					break;
			}
			$settings[$option_name] = $this->get_option( $option, '', true);
		}
		
		$settings['post_type'] = $this->post_type;	
		return $settings;
	}
	
	function get_option( $name, $default='', $default_if_empty=false ){
		if( false!==$this->cb_get_option && is_callable( $this->cb_get_option ) ){
			return call_user_func( $this->cb_get_option, $name, $default, $default_if_empty );
		}
		
		if($this->options===false){
			$this->options = get_option($this->options_varname);
			$this->options = is_array($this->options)?$this->options:array();		
		}
		$value = isset($this->options[$name])?$this->options[$name]:$default;
		if($default_if_empty){
			$value = ''==$value?$default:$value;
		}
		return $value;		
	}
}
?>