<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/
 
if('sco_ajax'==get_class($this)):
	$client_id 		=  $this->get_option('yahoo_consumer_key','',true);
	$client_secret 	=  $this->get_option('yahoo_consumer_secret','',true);
	//-----
	if(!class_exists('http_class'))			require( $path.'oauth-api/http.php');
	if(!class_exists('oauth_client_class'))	require( $path.'oauth-api/oauth_client.php');

	$client = new oauth_client_class;
	$client->debug = false;
	$client->debug_http = true;
	$client->server = 'Yahoo';
	//--
	$client->redirect_uri 	= $redirect_uri;
	$client->client_id 		= $client_id;
	$client->client_secret 	= $client_secret;

	if(strlen($client->client_id) == 0
	|| strlen($client->client_secret) == 0)
		$this->error_message( __('Settings error, network api credentials not set.','rhsco') );

	if(($success = $client->Initialize())){
		if(($success = $client->Process())){
			if(strlen($client->access_token)){
				$success = $client->CallAPI(
					'http://query.yahooapis.com/v1/yql', 
					'GET', array(
						'q'=>'select * from social.profile where guid=me',
						'format'=>'json'
					), array('FailOnAccessError'=>true), $user);
			}
		}
		$success = $client->Finalize($success);
	}
	if($client->exit)
		exit;
	if(strlen($client->authorization_error)){
		$client->error = $client->authorization_error;
		$success = false;
	}
	if($success){
		die('You shouldnt be using this, as it is not complete.')
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Yahoo OAuth client results</title>
</head>
<body>
<?php
		echo '<h1>', HtmlSpecialChars($user->query->results->profile->nickname), 
			' you have logged in successfully with Yahoo!</h1>';
		echo '<pre>', HtmlSpecialChars(print_r($user, 1)), '</pre>';
?>
</body>
</html>
<?php
		die('TODO');
	}else{
		$this->error_message( sprintf( __('Error authenticating with social network: %s','rhsco'), HtmlSpecialChars($client->error) ) );
	}
else:
	$this->error_message( __('Settings error, cannot link directly to this file.', 'rhsco') );
endif; 
 


?>