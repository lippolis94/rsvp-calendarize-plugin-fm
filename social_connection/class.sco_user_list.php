<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/

class sco_user_list {
	var $networks=false;
	function __construct(){
		add_filter('manage_users_columns', array(&$this,'manage_users_columns'), 10, 1);
		add_action('manage_users_custom_column', array(&$this,'manage_users_custom_column'), 10, 3);
	}
	
	function manage_users_columns( $columns ){
		$columns['sco_network']=__('Networks','rhsco');
		return $columns;
	}
	
	function manage_users_custom_column($value, $column_name, $user_id){
		if ( 'sco_network' == $column_name ){
			if(false===$this->networks){
				$this->networks = apply_filters('sco_installed_networks', array());
			}
			
			$out = array();
			foreach($this->networks as $network => $n){
				$meta_key = 'sco_'.$network;
				$id = get_user_meta($user_id,$meta_key,true);
				if(!empty($id)){
					$class = apply_filters('sco_user_list_network_class',$network);
					$out[]=sprintf('<span class="sco-%s %s" title="%s" alt="%s">%s</span>', 
						$network, 
						$class,
						(__('Network id:','rhsco').$id),
						(__('Network id:','rhsco').$id),
						$n->name
					);
				}
			}
			
			return implode('',$out);
		}
    	return $value;
	}
}
?>