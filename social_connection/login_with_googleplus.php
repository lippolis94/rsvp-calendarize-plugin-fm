<?php

/**
 * 
 *
 * @version $Id$
 * @copyright 2003 
 **/
 
if('sco_ajax'==get_class($this)):
	$client_id 		=  $this->get_option('googleplus_clientid','',true);
	$client_secret 	=  $this->get_option('googleplus_apikey','',true);
	//-----
	if(!class_exists('http_class'))			require( $path.'oauth-api/http.php');
	if(!class_exists('oauth_client_class'))	require( $path.'oauth-api/oauth_client.php');

	$client = new oauth_client_class;
	$client->debug = false;
	$client->debug_http = true;
	$client->server = 'Google';
	// set the offline access only if you need to call an API
	// when the user is not present and the token may expire
	//$client->offline = true;
	//--
	$client->redirect_uri 	= $redirect_uri;
	$client->client_id 		= $client_id;
	$client->client_secret 	= $client_secret;

	if(strlen($client->client_id) == 0
	|| strlen($client->client_secret) == 0)
		$this->error_message( __('Settings error, network api credentials not set.','rhsco') );

	/* API permissions
	 */
	$client->scope = 'https://www.googleapis.com/auth/userinfo.email '.
		'https://www.googleapis.com/auth/userinfo.profile';
	if(($success = $client->Initialize())){
		if(($success = $client->Process())){
			if(strlen($client->authorization_error)){
				$client->error = $client->authorization_error;
				$success = false;
			}elseif(strlen($client->access_token)){
				$success = $client->CallAPI(
					'https://www.googleapis.com/oauth2/v1/userinfo',
					'GET', array(), array('FailOnAccessError'=>true), $user);
			}
		}
		$success = $client->Finalize($success);
	}
	if($client->exit)
		exit;
	if($success){
		$SCO_Profile = new SCO_Profile( $network, (array)$user, $this->cb_get_option );
	}else{
		$this->error_message( sprintf( __('Error authenticating with social network: %s','rhsco'), HtmlSpecialChars($client->error) ) );
	}
else:
	$this->error_message( __('Settings error, cannot link directly to this file.', 'rhsco') );
endif; 
 


?>