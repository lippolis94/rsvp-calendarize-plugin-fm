
function init_sco(){
	jQuery(document).ready(function($){
		$('.sco_social_login').each(function(i,btn){
			$(btn).unbind('click').click(function(e){
				//if($(this).twbutton)$(this).twbutton('loading');
				
				var network = $(btn).data('network');
				var window_close = $(this).data('window-close') || '1';
				
				var url = SCO.ajaxurl + '?action=sco_social_connect&network=' + network + '&window_close='+window_close;				
								
				cb_window_open = $(this).data('window-callback');				
				fn = window[cb_window_open];
				if( 'function' == typeof fn ){
					fn( url, this, network );
				}else{
					default_sco_popup( url, this, network );		
				}		
			});
		});
		$('.sco-wp').each(function(i,btn){
			$(btn).unbind('click').click(function(e){
				window.location.href = $(this).data('href');
				return false;
			});
		});
	});
}

function default_sco_popup( url, btn, network ){
	settings = jQuery.extend({
		width:626,
		height:436
	},jQuery(btn).data('sco_settings'));
	
	SCO.btn = btn;				
	child_window_options = 'toolbar=0,status=0,width=' + settings.width + ',height=' + settings.height;
	SCO.child_window = window.open(url, '', child_window_options );
	SCO.timer = setInterval( 'check_child_window()' , 500);
}

function check_child_window() {
    if (SCO.child_window.closed) {
		if(jQuery(SCO.btn).twbutton)jQuery(SCO.btn).twbutton('reset');
        clearInterval(SCO.timer);
		
		fn = window[SCO.complete];
		if( 'function' == typeof fn ){
			fn();
		}else{
			sco_complete();	
		}			
    }
}

function sco_complete(){
	behavior = jQuery(SCO.btn).data('login_bahavior');
	if( 'reload'==behavior ){
		location.reload(true);
	}else if( 'redirect'==behavior ){
		url = jQuery(SCO.btn).data('login_redirect_to') || jQuery(SCO.btn).data('redirect_to');	
		window.location.replace(url);
	}	
}