<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Dinah!' );
}

?>

<div class="rsvp_enabled_group">
	<div class="rsvp-btn-group-top">
		<div class="rsvp-btn-group-top-label">
			<?php _e( 'What type of answer would you like to start with?', 'rhcrsvp' ); ?>
		</div>
		<div class="rsvp-btn-group-top-buttons">
			<a class="rsvp-btn rsvp-add" data-type="0" href="javascript:void(0);"><i class="icon-circle-plus"></i><?php _e( 'Yes', 'rhcrsvp' ); ?></a>
			<a class="rsvp-btn rsvp-add" data-type="1" href="javascript:void(0);"><i class="icon-circle-plus"></i><?php _e( 'Maybe', 'rhcrsvp' ); ?></a>
			<a class="rsvp-btn rsvp-add" data-type="2" href="javascript:void(0);"><i class="icon-circle-plus"></i><?php _e( 'No', 'rhcrsvp' ); ?></a>
			<a class="rsvp-btn rsvp-add" data-type="3" href="javascript:void(0);"><i class="icon-circle-plus"></i><?php _e( 'Custom', 'rhcrsvp' ); ?></a>
		</div>
	</div>
</div>

<div class="rsvp_enabled_group">
<table id="rhc-rsvp-fields-table" class="wp-list-table widefat">
	<thead>
		<tr>
			<th><?php _e( 'Title', 'rhcrsvp' ); ?></th>
			<th><?php _e( 'Description', 'rhcrsvp' ); ?></th>
			<th><?php _e( 'Slug', 'rhcrsvp' ); ?></th>
			<th><?php _e( 'Limit', 'rhcrsvp' ); ?></th>
			<th><?php _e( 'Actions', 'rhcrsvp' ); ?></th>
		</tr>
	</thead>
	<tbody class="rsvp-field-container">
		<tr class="rsvp-field-detail">
			<td data-th="<?php _e( 'Title', 'rhcrsvp' ); ?>">
				<input type="hidden" class="rsvp-input" name="rsvp_choose_id[]" value="">
				<input type="hidden" class="rsvp-input" name="rsvp_choose_type[]" value="">
				<input type="text" class="rsvp-input rsvp-input-main" name="rsvp_choose_text[]" value="">
			</td>
			<td data-th="<?php _e( 'Description', 'rhcrsvp' ); ?>">
				<input type="text" class="rsvp-input rsvp-input-main" name="rsvp_choose_subtext[]" value="">
			</td>
			<td data-th="<?php _e( 'Slug', 'rhcrsvp' ); ?>">
				<input type="text" class="rsvp-input rsvp-input-main" name="rsvp_choose_slug[]" value="">
			</td>
			<td class="rsvp-col-limit" data-th="<?php _e( 'Limit', 'rhcrsvp' ); ?>">
				<input type="text" class="rsvp-input rsvp-input-main" name="rsvp_choose_limit[]" value="">
			</td>
			<td>
				<a class="rsvp-btn-sortable tooltipster" title="<?php _e( 'Drag and Drop', 'rhcrsvp' ); ?>" href="javascript:void(0);"><span>s</span></a>
				<a class="rsvp-btn-settings tooltipster" title="<?php _e( 'Settings', 'rhcrsvp' ); ?>" href="javascript:void(0);"><span>s</span></a>
				<a class="rsvp-btn-delete tooltipster" title="<?php _e( 'Delete', 'rhcrsvp' ); ?>" href="javascript:void(0);"><span>d</span></a>
			</td>
		</tr>
		<tr class="rsvp-field-settings" style="display: none;">
			<td colspan="5">
				<div class="rsvp-field-settings-container">
					<div class="rsvp-field-settings">
						<div class="rsvpfs-head">
							<div class="rsvpfs-main-title"><?php _e( 'Field Settings', 'rhcrsvp' ); ?></div>
							<hr>
						</div>
						<div class="rsvpfs-group">
							<div class="rsvpfs-subtitle"><?php _e( 'Colors', 'rhcrsvp' ); ?></div>
							<div class="tag-row">
								<div class="tag-col">
									<input type="text" class="rsvp-input rsvp-input-color" name="rsvp_normal_color[]"> 
								</div>
								<div class="tag-col">
									<input type="text" class="rsvp-input rsvp-input-color" name="rsvp_hover_color[]">
								</div>
								<div class="tag-col">
									<input type="text" class="rsvp-input rsvp-input-color" name="rsvp_text_color[]">
								</div>
							</div>
							<div class="tag-row">
								<div class="tag-col"><?php _e( 'Background', 'rhcrsvp' ); ?></div>
								<div class="tag-col"><?php _e( 'Hover', 'rhcrsvp' ); ?></div>
								<div class="tag-col"><?php _e( 'Font', 'rhcrsvp' ); ?></div>
							</div>
						</div>
						<div class="rsvpfs-group">
							<input type="checkbox" class="rsvp-input" name="rsvp_choose_allow[]"> <?php _e( 'Send Email / Show Avatar in list', 'rhcrsvp' ); ?>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<th colspan="5">
				<a class="rsvp-btn rsvp-add" data-type="0" href="javascript:void(0);"><i class="icon-circle-plus"></i><?php _e( 'Yes', 'rhcrsvp' ); ?></a>
				<a class="rsvp-btn rsvp-add" data-type="1" href="javascript:void(0);"><i class="icon-circle-plus"></i><?php _e( 'Maybe', 'rhcrsvp' ); ?></a>
				<a class="rsvp-btn rsvp-add" data-type="2" href="javascript:void(0);"><i class="icon-circle-plus"></i><?php _e( 'No', 'rhcrsvp' ); ?></a>
				<a class="rsvp-btn rsvp-add" data-type="3" href="javascript:void(0);"><i class="icon-circle-plus"></i><?php _e( 'Custom', 'rhcrsvp' ); ?></a>
			</th>
		</tr>
	</tfoot>
</table>
</div>