<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Dinah!' );
}

class RHC_RSVP_Options extends tools_rsvp {
	public $added_rules;
	public $id;
	public $checkup = 0;

	public function __construct() {
		parent::__construct();

		$this->id = 'rsvp';

		add_filter( "pop-options_{$this->id}", array( $this, 'options' ) );
		
		if ( ! empty( $_REQUEST['page'] ) && ! empty( $_REQUEST['action'] ) && 'rsvp-options' == $_REQUEST['page'] && 'update' == $_REQUEST['action'] ) {
			$this->pop_handle_save_data();
		}
	}
	
	public function pop_handle_save_data() {
		$count = 0;
		
		if(!empty($_POST['layout_user_input_fields']) && is_numeric($_POST['layout_user_input_fields'])){
			for($i=0;$i<$_POST['layout_user_input_fields'];$i++){
			
				if(empty($_POST['layout_user_input_label_'.$i])){
					$_POST['layout_user_input_label_'.$i] = 'Info';
				}
				
				if(empty($_POST['layout_user_input_slug_'.$i])){
					$_POST['layout_user_input_slug_'.$i] = $this->get_title_slug_name($_POST['layout_user_input_label_'.$i]);
				} else {
					$_POST['layout_user_input_slug_'.$i] = $this->get_title_slug_name($_POST['layout_user_input_slug_'.$i]);
				}
				$this->checkup = 0;
				$this->nodublicate($_POST['layout_user_input_fields'],'layout_user_input_slug_',$_POST['layout_user_input_slug_'.$i],$i);
			
				if(!empty($this->checkup)){
					$_POST['layout_user_input_slug_'.$i] = $_POST['layout_user_input_slug_'.$i].'_'.$this->checkup;
				}
			}
		}
		
		if(!empty($_POST['layout_choose_fields']) && is_numeric($_POST['layout_choose_fields'])){
			for($i=0;$i<$_POST['layout_choose_fields'];$i++){
			
				if(empty($_POST['layout_choose_text_'.$i])){
					$_POST['layout_choose_text_'.$i] = 'Title';
				}
				
				if(empty($_POST['layout_choose_subtext_'.$i])){
					$_POST['layout_choose_subtext_'.$i] = 'Sub title';
				}				
				
				if(empty($_POST['layout_choose_slug_'.$i])){
					$_POST['layout_choose_slug_'.$i] = $this->get_title_slug_name($_POST['layout_choose_text_'.$i].' '.$_POST['layout_choose_subtext_'.$i]);
				} else {
					$_POST['layout_choose_slug_'.$i] = $this->get_title_slug_name($_POST['layout_choose_slug_'.$i]);
				}
				$this->checkup = 0;
				$this->nodublicate($_POST['layout_choose_fields'],'layout_choose_slug_',$_POST['layout_choose_slug_'.$i],$i);
			
				if(!empty($this->checkup)){
					$_POST['layout_choose_slug_'.$i] = $_POST['layout_choose_slug_'.$i].'_'.$this->checkup;
				}
			}
		}	
	}
	
	public function nodublicate($number_field,$slug,$slug_data,$current_id){
		for($j=0;$j<$current_id;$j++){
			if($this->checkup > 0 && $j != $current_id && !empty($_POST[$slug.$j]) && $slug_data.'_'.$this->checkup == $_POST[$slug.$j]){
				$this->checkup++;
				$this->nodublicate($number_field,$slug,$slug_data,$current_id);
			} else if($this->checkup == 0 && $j != $current_id && !empty($_POST[$slug.$j]) && $slug_data == $_POST[$slug.$j]){
				$this->checkup++;
				$this->nodublicate($number_field,$slug,$slug_data,$current_id);
			}
		}
	}

	public function list_of_pages_with_shortcode(){
		$tmp_arrray = array(
			'[SYSTEM]'                => __( 'Login with', 'rhcrsvp' ),
			'[USER_FIRSTNAME]'        => __( 'First name', 'rhcrsvp' ),
			'[USER_LASTNAME]'         => __( 'Last name', 'rhcrsvp' ),
			'[USER_EMAIL]'            => __( 'User email', 'rhcrsvp' ),
			'[USER_URL]'              => __( 'User media url', 'rhcrsvp' ),
			'[USER_PHOTO_URL]'        => __( 'User Avatar/image Url', 'rhcrsvp' ),
			'[USER_PHOTO_IMG]'        => __( 'User Avatar/image <img> block', 'rhcrsvp' ),
			'[THUMBNAIL_URL]'         => __( 'Event default thumbnail', 'rhcrsvp' ),
			'[THUMBNAIL_IMG]'         => __( 'Event default thumbnail <img> block', 'rhcrsvp' ),
			'[TOP_IMAGE_URL]'         => __( 'Event top image', 'rhcrsvp' ),
			'[TOP_IMAGE_IMG]'         => __( 'Event top image <img> block', 'rhcrsvp' ),
			'[DBOX_IMAGE_URL]'        => __( 'Event Dbox image', 'rhcrsvp' ),
			'[DBOX_IMAGE_IMG]'        => __( 'Event Dbox image <img> block', 'rhcrsvp' ),
			'[TOOLTIP_IMAGE_URL]'     => __( 'Event tooltip image', 'rhcrsvp' ),
			'[TOOLTIP_IMAGE_IMG]'     => __( 'Event tooltip image  <img> block', 'rhcrsvp' ),
			'[ANSWER]'                => __( 'Answer', 'rhcrsvp' ),
			'[POST_ID]'               => __( 'Event Post id', 'rhcrsvp' ),
			'[LIMIT]'                 => __( 'Event Limit count', 'rhcrsvp' ),
			'[CURRENT_ATTENDING]'     => __( 'Event current attending count', 'rhcrsvp' ),
			'[LINK]'                  => __( 'Link to event', 'rhcrsvp' ),
			'[TITLE]'                 => __( 'Event Title', 'rhcrsvp' ),
			'[EXCERPT]'               => __( 'Event Excerpt', 'rhcrsvp' ),
			'[STARTDATE]'             => __( 'Start Date', 'rhcrsvp' ),
			'[STARTTIME]'             => __( 'Start Time', 'rhcrsvp' ),
			'[ENDDATE]'               => __( 'End Date', 'rhcrsvp' ),
			'[ENDTIME]'               => __( 'End Time', 'rhcrsvp' ),
			'[ORGANIZER_NAME]'        => __( 'Organizer Name', 'rhcrsvp' ),
			'[ORGANIZER_EMAIL]'       => __( 'Organizer Email', 'rhcrsvp' ),
			'[ORGANIZER_PHONE]'       => __( 'Organizer Phone', 'rhcrsvp' ),
			'[ORGANIZER_WEBSITE]'     => __( 'Organizer Website', 'rhcrsvp' ),
			'[ORGANIZER_DESCRIPTION]' => __( 'Organizer Description', 'rhcrsvp' ),
			'[VENUE_NAME]'            => __( 'Venue Name', 'rhcrsvp' ),
			'[VENUE_ADDRESS]'         => __( 'Venue Address', 'rhcrsvp' ),
			'[VENUE_EMAIL]'           => __( 'Venue Email', 'rhcrsvp' ),
			'[VENUE_CITY]'            => __( 'Venue City', 'rhcrsvp' ),
			'[VENUE_ZIP]'             => __( 'Venue Zip', 'rhcrsvp' ),
			'[VENUE_STATE]'           => __( 'Venue State', 'rhcrsvp' ),
			'[VENUE_COUNTRY]'         => __( 'Venue Country', 'rhcrsvp' ),
			'[VENUE_PHONE]'           => __( 'Venue Phone', 'rhcrsvp' ),
			'[VENUE_WEBSITE]'         => __( 'Venue Website', 'rhcrsvp' ),
			'[VENUE_GADDRESS]'        => __( 'Venue Guide Address', 'rhcrsvp' ),
			'[VENUE_DESCRIPTION]'     => __( 'Venue Description', 'rhcrsvp' ),
			'[ICAL_FEED]'             => __( 'iCal feed link', 'rhcrsvp' )
		);
		
		for($i=0;$i<$this->get_layout_user_input_fields_count($this->tmp_pack['post_id']);$i++){
			$tmp_title = $this->get_layout_user_input_fields_title($i,0);
			$tmp_title_slug = $this->get_title_slug_name($tmp_title);
			
			$tmp_arrray['[FIELD_'.strtoupper($tmp_title_slug).']'] = __('Info fields','rhcrsvp').' ' .$tmp_title;
			
		}

		$return_array = __('All Email notification fields uses this tags','rhcrsvp').'<br><br><div class="rsvp_table rsvp_first">';
		$devide = ceil((count($tmp_arrray)/2)-0.5);
		$count = 0;
		foreach($tmp_arrray as $tmp_key => $tmp_data){
			
			if($count == $devide){
				$return_array .= '</div><div class="rsvp_table rsvp_secount">';
			}
			
			$return_array .= '<div class="rsvp_object">'.$tmp_key.' </div>';
			$return_array .= '<div class="rsvp_item_text">&nbsp;- '.__($tmp_data,'rhcrsvp').'</div>';
			$count++;
		}
		
		$return_array .= '</div>';

		return $return_array;
	}

	public function options( $t ) {
		wp_enqueue_style( 'rhcrsvp-backend', RHCRSVP_URL . 'css/backend_option.css', array(), RHCRSVP_VERSION );
		wp_enqueue_script( 'rhcrsvp-backend', RHCRSVP_URL . 'js/backend_option.js', array( 'jquery-ui-sortable' ), RHCRSVP_VERSION );

		$i = count( $t );
		$t[ $i ]                = (object) array();
		$t[ $i ]->id            = 'rsvp-generalt-settings';
		$t[ $i ]->label         = __( 'General Settings', 'rhcrsvp' );
		$t[ $i ]->right_label   = __( 'General Settings', 'rhcrsvp' );
		$t[ $i ]->page_title    = __( 'General Settings', 'rhcrsvp' );
		$t[ $i ]->priority      = 1;
		$t[ $i ]->theme_option  = true;
		$t[ $i ]->plugin_option = true;
		$t[ $i ]->options       = array();

		$t[ $i ]->options[] = (object) array(
			'type'  => 'subtitle',
			'label' => __( 'RSVP Box Settings', 'rhcrsvp' )
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'overwritealleventpost',
			'type'          => 'select',
			'label'         => __( 'Set all RSVP Boxes to', 'rhcrsvp' ),
			'options'       => array(
				''    => '-Select-',
				'yes' => 'On',
				'no'  => 'Off'
			),
			'description'   => __( 'WARNING: Saving this setting will affect all Events in your Calendar. After saving the setting returns to Select.', 'rhcrsvp' ),
			'el_properties' => array(),
			'default'       => '',
			'save_option' => true,
			'load_option' => true
		);
		
		$t[ $i ]->options[] = (object) array(
			'type' => 'clear'
		);

		$t[ $i ]->options[] = (object) array(
			'type'  => 'subtitle',
			'label' => __( 'Attendee Settings', 'rhcrsvp' )
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_attendee_time',
			'type'          => 'select',
			'label'         => __( 'Attendee Time Settings', 'rhcrsvp' ),
			'options'       => array(
				'6'   => __( '6 hours', 'rhcrsvp' ),
				'12'  => __( '12 hours', 'rhcrsvp' ),
				'24'  => __( '24 hours', 'rhcrsvp' ),
				'48'  => __( '48 hours', 'rhcrsvp' ),
				'72'  => __( '72 hours', 'rhcrsvp' ),
				'168' => __( '7 days', 'rhcrsvp' ),
				'336' => __( '14 days', 'rhcrsvp' ),
				'720' => __( '30 days', 'rhcrsvp' ),
				'-'   => __( 'no limit', 'rhcrsvp' )
			),
			'description'   => __( 'Select the time before the event takes place where the user can change their status.', 'rhcrsvp' ),
			'el_properties' => array(),
			'default'       => '6',
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'type' => 'clear'
		);

		$t[ $i ]->options[] = (object) array(
			'type'  => 'subtitle',
			'label' => __( 'General Application Settings', 'rhcrsvp' )
		);

		$t[ $i ]->options[] = (object) array(
			'id'          => 'rsvp_newpost_on',
			'type'        => 'onoff',
			'label'       => __( 'Default RSVP Box', 'rhcrsvp' ),
			'description' =>__( 'Turn the RSVP Box ON or OFF by default on all new events.', 'rhcrsvp' ),
			'default'     => 0,
			'save_option' => true,
			'load_option' => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_allow_on_wp_users',
			'type'          => 'onoff',
			'label'         => __( 'Only for WP users', 'rhcrsvp' ),
			'el_properties' => array(),
			'default'       => 0,
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_remove_manaul_input',
			'type'          => 'onoff',
			'label'         => __( 'Remove Manual input', 'rhcrsvp' ),
			'description'   => __( 'If set to ON can user only use social or wp logins system.', 'rhcrsvp' ),
			'el_properties' => array(),
			'default'       => '0',
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_attend_show',
			'type'          => 'onoff',
			'label'         => __( 'Show attendee list', 'rhcrsvp' ),
			'el_properties' => array(),
			'default'       => $this->panel_settings['rsvp_attend_show'],
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_attend_show_name',
			'type'          => 'onoff',
			'label'         =>__( 'Show name of attendee', 'rhcrsvp' ),
			'default'       => $this->panel_settings['rsvp_attend_show_name'],
			'el_properties' => array(),
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_attend_show_link',
			'type'          => 'onoff',
			'label'         => __( 'Enable link on Gravatar', 'rhcrsvp' ),
			'el_properties' => array(),
			'default'       => '0',
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_show_dashbord_widget',
			'type'          => 'onoff',
			'label'         => __( 'Enable Dashboard Widget', 'rhcrsvp' ),
			'el_properties' => array(),
			'default'       => '1',
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'type' => 'clear'
		);

		$t[ $i ]->options[] = (object) array(
			'type'  => 'subtitle',
			'label' => __( 'Field Settings and Labels', 'rhcrsvp' )
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'element_show_text_title',
			'label'         => __( 'Show Title', 'rhcrsvp' ),
			'type'          => 'onoff',
			'default'       => empty( $this->panel_settings['element_show_text_title'] ) ? true : $this->panel_settings['element_show_text_title'],
			'el_properties'	=> array(),
			'hidegroup'     => '#element_show_text_title_group',
			'save_option'   => true,
			'load_option'   => true
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'element_show_text_title_group',
			'type'          => 'div_start',
			'el_properties' => array()
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'text_title',
			'label'         => __( 'Title Label', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => empty( $this->panel_settings['text_title'] ) ? '' : $this->panel_settings['text_title'],
			'el_properties'	=> array( 'class' => 'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);
		
		$t[ $i ]->options[] = (object) array(
			'type' => 'div_end'
		);

		$t[ $i ]->options[] =	(object)array(
			'id'            => 'element_show_text_info_title',
			'label'         => __( 'Show Contact Title', 'rhcrsvp' ),
			'type'          => 'onoff',
			'default'       => empty( $this->panel_settings['element_show_text_info_title'] ) ? true : $this->panel_settings['element_show_text_info_title'],
			'el_properties' => array(),
			'hidegroup'     => '#element_show_text_info_title_group',
			'save_option'	=> true,
			'load_option'	=> true
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'element_show_text_info_title_group',
			'type'          => 'div_start',
			'el_properties' => array()
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'text_info_title',
			'label'         => __( 'Contact Title Label', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => empty( $this->panel_settings['text_info_title'] ) ? '' : $this->panel_settings['text_info_title'],
			'el_properties'	=> array( 'class' => 'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'type'  => 'div_end'
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'element_show_text_list_title',
			'label'         => __( 'Show list tile', 'rhcrsvp' ),
			'type'          => 'onoff',
			'default'       => empty( $this->panel_settings['element_show_text_list_title'] ) ? true : $this->panel_settings['element_show_text_list_title'],
			'el_properties' => array(),
			'hidegroup'     => '#element_show_text_list_title_group',
			'save_option'   => true,
			'load_option'   => true
		);
		
		$t[ $i ]->options[] =	(object)array(
			'id'            => 'element_show_text_list_title_group',
			'type'          => 'div_start',
			'el_properties' => array()
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'text_list_title',
			'label'         => __( 'List title', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => empty( $this->panel_settings['text_list_title'] ) ? '' : $this->panel_settings['text_list_title'],
			'el_properties'	=> array( 'class' => 'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);
		
		$t[ $i ]->options[] = (object) array(
			'type' => 'div_end'
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'element_show_text_sold_out',
			'label'         => __( 'Show Sold out', 'rhcrsvp' ),
			'type'          => 'onoff',
			'default'       => empty( $this->panel_settings['element_show_text_sold_out'] ) ? true : $this->panel_settings['element_show_text_sold_out'],
			'el_properties' => array(),
			'hidegroup'     => '#element_show_text_sold_out_group',
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'element_show_text_sold_out_group',
			'type'          => 'div_start',
			'el_properties' => array()
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'text_sold_out',
			'label'         => __( 'Sold out', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => empty( $this->panel_settings['text_sold_out'] ) ? __( '(Sold out)', 'rhcrsvp' ) : $this->panel_settings['text_sold_out'],
			'el_properties'	=> array( 'class' => 'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'type' => 'div_end'
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'element_show_text_email_receive',
			'label'         => __( 'Show "Send Email"', 'rhcrsvp' ),
			'type'          => 'onoff',
			'default'       => empty( $this->panel_settings['element_show_text_email_receive'] ) ? true : $this->panel_settings['element_show_text_email_receive'],
			'el_properties' => array(),
			'hidegroup'     => '#element_show_text_email_receive_group',
			'save_option'   => true,
			'load_option'   => true
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'element_show_text_email_receive_group',
			'type'          => 'div_start',
			'el_properties' => array()
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'text_email_receive',
			'label'         => __( 'Send Email Label', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => empty( $this->panel_settings['text_email_receive'] ) ? '' : $this->panel_settings['text_email_receive'],
			'el_properties'	=> array( 'class' => 'text-width-full' ),
			'save_option' => true,
			'load_option' => true
		);

		$t[ $i ]->options[] = (object) array(
			'type' => 'div_end'
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'element_show_input_comment',
			'label'         => __( 'Comment Show', 'rhcrsvp' ),
			'type'          => 'onoff',
			'default'       => empty( $this->panel_settings['element_show_input_comment'] ) ? true : $this->panel_settings['element_show_input_comment'],
			'el_properties' => array(),
			'hidegroup'     => '#element_show_input_comment_group',
			'save_option'   => true,
			'load_option'   => true
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'element_show_input_comment_group',
			'type'          => 'div_start',
			'el_properties' => array()
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'layout_user_input_label_comment',
			'label'         => __( 'Comment Label', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => empty( $this->panel_settings['layout_user_input_label_comment'] ) ? '' : $this->panel_settings['layout_user_input_label_comment'],
			'el_properties' => array( 'class' => 'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);
		
		$t[ $i ]->options[]=(object)array(
			'type'  => 'div_end'
		);

		$t[ $i ]->options[] = (object) array(
			'type' => 'clear'
		);

		$t[ $i ]->options[] = (object) array(
			'type'  => 'submit',
			'label' => __( 'Save', 'rhc' ),
			'class' => 'button-primary'
		);

		$i = count( $t );
		$t[ $i ]                = (object) array();
		$t[ $i ]->id            = 'rhc-rsvp-default-fields'; 
		$t[ $i ]->label         = __( 'Default Fields', 'rhcrsvp' );
		$t[ $i ]->right_label   = __( 'Default Fields', 'rhcrsvp' );
		$t[ $i ]->page_title    = __( 'Default Fields', 'rhcrsvp' );
		$t[ $i ]->theme_option  = true;
		$t[ $i ]->plugin_option = true;
		$t[ $i ]->priority      = 2;
		$t[ $i ]->options       = array();	
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'layout_choose_fields',
			'label'         => __( 'Number of fields', 'rhcrsvp' ),
			'default'       => '3',
			'description'   => __( 'Change the value to enable more fields, Save and return to this panel', 'rhcrsvp' ),
			'type'          => 'text',
			'el_properties' => array(),
			'save_option'   => true,
			'load_option'   => true
		);

		for ( $a = 0; $a < $this->panel_settings['layout_choose_fields']; $a++ ) {
			$t[ $i ]->options[] = (object) array(
				'id'            => 'layout_choose_small' . $a,
				'type'          => 'div_start',
				'el_properties'	=> array(
					'class' => 'choose_fields choose_colobox'
				)
			);		
		
			$t[ $i ]->options[] = (object) array(
				'type'  => 'subtitle',
				'label' => __( 'Button', 'rhcrsvp' ) . ' ' . ( $a + 1 ),
			);

			$t[ $i ]->options[] = (object) array(
				'id'            => 'layout_choose_text_' . $a,
				'label'         => __( 'Text', 'rhcrsvp' ),
				'type'          => 'text',
				'default'       => empty( $this->panel_settings["layout_choose_text_{$a}"] ) ? '' : $this->panel_settings["layout_choose_text_{$a}"],
				'el_properties'	=> array(
					'class'       => 'layout_choose_text choose_fields_label',
					'placeholder' => __( 'Text', 'rhcrsvp' )
				),
				'save_option'   => true,
				'load_option'   => true
			);
			
			$t[ $i ]->options[] = (object) array(
				'id'            => 'layout_choose_subtext_' . $a,
				'label'         => __( 'Sub Text', 'rhcrsvp' ),
				'type'          => 'text',
				'default'       => empty( $this->panel_settings["layout_choose_subtext_{$a}"] ) ? '' : $this->panel_settings["layout_choose_subtext_{$a}"],
				'el_properties' => array(
					'class'       => 'layout_choose_subtext custom_field',
					'placeholder' => __( 'Sub Text', 'rhcrsvp' )
				),
				'save_option'   => true,
				'load_option'   => true
			);
			
			$t[ $i ]->options[] = (object) array(
				'id'            => 'layout_choose_slug_' . $a,
				'label'         => __( 'Slug', 'rhcce' ),
				'type'          => 'text',
				'default'       => empty( $this->panel_settings["layout_choose_slug_{$a}"] ) ? '' : $this->panel_settings["layout_choose_slug_{$a}"],
				'el_properties' => array(
					'class'       => 'layout_choose_slug custom_field',
					'placeholder' =>__( 'Automatically generate if empty', 'rhcrsvp' )
				),
				'save_option'   => true,
				'load_option'   => true
			);
			
			$t[ $i ]->options[] = (object) array(
				'id'            => 'layout_choose_limit_' . $a,
				'label'         =>  sprintf( '%s<br><small>(%s)</small>',
					__( 'Limit amount', 'rhcrsvp' ),
					__( '0 = unlimited', 'rhcrsvp' )
				),
				'type'          => 'text',
				'default'       => empty( $this->panel_settings["layout_choose_limit_{$a}"] ) ? '' : $this->panel_settings["layout_choose_limit_{$a}"],
				'el_properties' => array(
					'class'       => 'layout_choose_limit choose_fields_checkbox',
					'placeholder' =>__( '0', 'rhcrsvp' )
				),
				'save_option'   => true,
				'load_option'   => true
			);
			
			$t[ $i ]->options[] = (object) array(
				'type' => 'clear'
			);
			
			$t[ $i ]->options[] = (object) array(
				'id'            => 'layout_normal_color_' . $a,
				'type'          => 'farbtastic',
				'label'         => __( 'Normal Bg Color', 'rhcrsvp' ),
				'el_properties' => array(
					'class' => 'layout_normal_color'
				),
				'default'       => empty( $this->panel_settings["layout_normal_color_{$a}"] ) ? '#ffffff' : $this->panel_settings["layout_normal_color_{$a}"],
				'save_option'   => true,
				'load_option'   => true
			);
	
			$t[ $i ]->options[] = (object) array(
				'id'            => 'layout_hover_color_' . $a,
				'type'          => 'farbtastic',
				'label'         => __( 'Hover Bg Color', 'rhcrsvp' ),
				'default'       => empty( $this->panel_settings["layout_hover_color_{$a}"] ) ? '#ffffff' : $this->panel_settings["layout_hover_color_{$a}"],
				'el_properties' => array(
					'class' => 'layout_hover_color'
				),
				'save_option'   => true,
				'load_option'   => true
			);
			
			$t[ $i ]->options[] = (object) array(
				'id'            => 'layout_text_color_' . $a,
				'type'          => 'farbtastic',
				'label'         => __( 'Text Color', 'rhcrsvp' ),
				'default'       => empty( $this->panel_settings["layout_text_color_{$a}"] ) ? '#000000' : $this->panel_settings["layout_text_color_{$a}"],
				'el_properties' => array(
					'class' => 'layout_text_color'
				),
				'save_option'   => true,
				'load_option'   => true
			);
							
			$t[ $i ]->options[] = (object) array(
				'type' => 'clear'
			);
			
			$t[ $i ]->options[] = (object) array(
				'id'            => 'layout_choose_allow_' . $a,
				'label'         => sprintf( '%s<br>%s',
					__( 'Send Email', 'rhcrsvp' ),
					__( 'Show Avatar on list', 'rhcrsvp' )
				),
				'type'          => 'checkbox',
				'el_properties' => array(
					'class' => 'layout_choose_allow choose_fields_checkbox
				'),
				'default'       => empty( $this->panel_settings["layout_choose_allow_{$a}"] ) ? '' : $this->panel_settings["layout_choose_allow_{$a}"],
				'save_option'   => true,
				'load_option'   => true
			);

			$t[ $i ]->options[] = (object) array(
				'type' => 'div_end'
			);
		}

		$t[ $i ]->options[] = (object) array(
			'type' => 'clear'
		);

		$t[ $i ]->options[] = (object) array(
			'type'  => 'subtitle',
			'label' => __( 'Count Color Settings', 'rhcrsvp' )
		);

		$t[ $i ]->options[] =	(object) array(
			'id'          => 'layout_count_color_body',
			'type'        => 'farbtastic',
			'label'       => __( 'Avatar icon color', 'rhcrsvp' ),
			'default'     => empty( $this->panel_settings['layout_count_color_body'] ) ? '#ffffff' : $this->panel_settings['layout_count_color_body'],
			'save_option' => true,
			'load_option' => true
		);

		$t[ $i ]->options[] = (object) array(
			'type' => 'clear'
		);

		$t[ $i ]->options[] = (object) array(
			'id'          => 'layout_count_color_boble',
			'type'        => 'farbtastic',
			'label'       => __( 'Bubble color', 'rhcrsvp' ),
			'default'     => empty( $this->panel_settings['layout_count_color_boble'] ) ? '#ffffff' : $this->panel_settings['layout_count_color_boble'],
			'save_option' => true,
			'load_option' => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'          => 'layout_count_color_text',
			'type'        => 'farbtastic',
			'label'       => __( 'Bubble text color', 'rhcrsvp' ),
			'default'     => empty( $this->panel_settings['layout_count_color_text'] ) ? '#ffffff' : $this->panel_settings['layout_count_color_text'],
			'save_option' => true,
			'load_option' => true
		);
		
		$t[ $i ]->options[] = (object) array(
			'type' => 'clear'
		);

		$t[ $i ]->options[] = (object) array(
			'type'  => 'submit',
			'label' => __( 'Save', 'rhcrsvp' ),
			'class' => 'button-primary'
		);

		$i = count( $t );
		$t[ $i ]                = (object) array();
		$t[ $i ]->id            = 'rhc-rsvp-custom-fields';
		$t[ $i ]->label         = __( 'Custom Fields', 'rhcrsvp' );
		$t[ $i ]->right_label   = __( 'Custom Fields', 'rhcrsvp' );
		$t[ $i ]->page_title    = __( 'Custom Fields', 'rhcrsvp' );
		$t[ $i ]->theme_option  = true;
		$t[ $i ]->plugin_option = true;
		$t[ $i ]->priority      = 3;
		$t[ $i ]->options       = array();

		$t[ $i ]->options[] = (object) array(
			'id'            => 'layout_user_input_fields',
			'label'         => __( 'Number of User Input fields', 'rhcrsvp' ),
			'default'       => '3',
			'description'   => __( 'Change the value to enable more fields, Save and return to this panel', 'rhcrsvp' ),
			'type'          => 'text',
			'el_properties' => array(),
			'save_option'   => true,
			'load_option'   => true
		);
		
		for ( $a = 0; $a < $this->panel_settings['layout_user_input_fields']; $a++ ) {
			$t[ $i ]->options[] = (object) array(
				'id'            => 'layout_user_input' . $a,
				'type'          => 'div_start',
				'el_properties' => array(
					'class' => 'choose_fields user_input_fields choose_colobox'
				)
			);
			
			$t[ $i ]->options[] = (object) array(
				'type'  => 'subtitle',
				'label' => __( 'Field', 'rhcrsvp' ) . ' ' . ( $a + 1 )
			);

			$t[ $i ]->options[] = (object) array(
				'id'            => 'layout_user_input_label_' . $a,
				'label'         => __( 'Text', 'rhcrsvp' ),
				'type'          => 'text',
				'default'       => empty( $this->panel_settings["layout_user_input_label_{$a}"] ) ? '' : $this->panel_settings["layout_user_input_label_{$a}"],
				'el_properties' => array(
					'class'       => 'layout_user_input_label',
					'placeholder' => __( 'Label', 'rhcrsvp' )
				),
				'save_option'   => true,
				'load_option'   => true
			);
				
			$t[ $i ]->options[] = (object) array(
				'id'            => 'layout_user_input_slug_' . $a,
				'label'         => __( 'Slug', 'rhcrsvp' ),
				'type'          => 'text',
				'default'       => empty( $this->panel_settings["layout_user_input_slug_{$a}"] ) ? '' : $this->panel_settings["layout_user_input_slug_{$a}"],
				'el_properties' => array(
					'class'       => 'layout_user_input_slug',
					'placeholder' => __( 'Automatically generate if empty', 'rhcrsvp' )
				),
				'save_option'   => true,
				'load_option'   => true
			);
			
			$t[ $i ]->options[] = (object) array(
					'id'            => 'layout_user_input_type_' . $a,
					'label'         => __( 'Type', 'rhcrsvp' ),
					'type'          => 'select',
					'options'       => array(
						'text'  => __( 'Text', 'rhcrsvp' ),
						'tlf'   => __( 'Phone', 'rhcrsvp' ),
						'email' => __( 'Email', 'rhcrsvp' )
					),
					'default'       => empty( $this->panel_settings["layout_user_input_type_{$a}"] ) ? '' : $this->panel_settings["layout_user_input_type_{$a}"],
					'el_properties' => array(
						'class'       => 'layout_user_input_type',
						'placeholder' => __( 'Label', 'rhcrsvp' )
					),
					'save_option'   => true,
					'load_option'   => true
			);
			
			$t[ $i ]->options[] = (object) array(
				'id'            => 'layout_user_input_main_name_' . $a,
				'label'         => __( 'Use as Name', 'rhcrsvp' ),
				'type'          => 'checkbox',
				'default'       => empty( $this->panel_settings["layout_user_input_main_name_{$a}"] ) ? '' : $this->panel_settings["layout_user_input_main_name_{$a}"],
				'el_properties'	=> array(),
				'save_option'   => true,
				'load_option'   => true
			);
			
			$t[ $i ]->options[] = (object) array(
				'id'            => 'layout_user_input_main_email_' . $a,
				'label'         => __( 'Use as Email', 'rhcrsvp' ),
				'type'          => 'checkbox',
				'default'       => empty( $this->panel_settings["layout_user_input_main_email_{$a}"] ) ? '' : $this->panel_settings["layout_user_input_main_email_{$a}"],
				'el_properties' => array(),
				'save_option'   => true,
				'load_option'   => true
			);

			$t[ $i ]->options[] = (object) array(
					'id'            => 'layout_user_input_check_' . $a,
					'label'         => __( 'Mandatory Field', 'rhcrsvp' ),
					'type'          => 'checkbox',
					'default'       => empty( $this->panel_settings["layout_user_input_check_{$a}"] ) ? '' : $this->panel_settings["layout_user_input_check_{$a}"],
					'el_properties' => array(),
					'save_option'   => true,
					'load_option'   => true
			);

			$t[ $i ]->options[] = (object) array(
				'type' => 'div_end'
			);
		}

		$t[ $i ]->options[] = (object) array(
			'type' => 'clear'
		);

		$t[ $i ]->options[] = (object) array(
			'type'  => 'submit',
			'label' => __( 'Save', 'rhc' ),
			'class' => 'button-primary'
		);

		$i = count( $t );
		$t[ $i ]                    = (object) array();
		$t[ $i ]->id                = 'rsvp-email-notification';
		$t[ $i ]->label             = __( 'Email Notification', 'rhcrsvp' );
		$t[ $i ]->right_label       = __( 'Email Settings', 'rhcrsvp' );
		$t[ $i ]->page_title        = __( 'Email Notification', 'rhcrsvp' );
		$t[ $i ]->theme_option      = true;
		$t[ $i ]->plugin_option     = true;
		$t[ $i ]->priority          = 4;
		$t[ $i ]->options           = array();
		
		$t[ $i ]->options[] = (object) array(
			'type'  => 'subtitle',
			'label' => __( 'Replacement tag', 'rhcrsvp' )
		);
		$t[ $i ]->options[] = (object) array(
			'type'     => 'callback',
			'callback' => array( $this, 'list_of_pages_with_shortcode' )
		);

		$t[ $i ]->options[] = (object) array(
			'type'  => 'subtitle',
			'label' => __( 'Attendee Notification', 'rhcrsvp' )
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_attendee_email',
			'type'          => 'onoff',
			'label'         => __( 'Enable Email Notification', 'rhcrsvp' ),
			'default'       => $this->panel_settings['rsvp_attendee_email'],
			'el_properties' => array(),
			'hidegroup'     => '#hide_attendee_email_group',
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'   => 'hide_attendee_email_group',
			'type' => 'div_start'
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_receive_emailfrom',
			'label'         => __( 'Email From', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => $this->panel_settings['rsvp_receive_emailfrom'],
			'el_properties' => array( 'class' => 'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_receive_emailsub',
			'label'         => __( 'Email Subject', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => 'RSVP for [TITLE]',
			'el_properties' => array( 'class' =>'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_receive_email',
			'type'          => 'textarea',
			'label'         => __( 'Email content', 'rhcrsvp' ),
			'description'   => '',
			'el_properties' => array( 'class' => 'text-width-full', 'rows' => 10 ),
			'default'       => $this->panel_settings['rsvp_receive_email'],
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'   => 'hide_attendee_email_group',
			'type' => 'div_end'
		);

		$t[ $i ]->options[] = (object) array(
			'type'  => 'subtitle',
			'label' => __( 'Administrator Notification', 'rhcrsvp' )
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_admin_email',
			'type'          => 'onoff',
			'label'         => __( 'Enable Email Notification', 'rhcrsvp' ),
			'default'       => $this->panel_settings['rsvp_admin_email'],
			'el_properties' => array(),
			'hidegroup'     => '#hide_admin_email_group',
			'save_option'   => true,
			'load_option'   => true
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'hide_admin_email_group',
			'el_properties' => array(),
			'type'          => 'div_start'
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_admin_receive_emailfrom',
			'label'         => __( 'Email To', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => $this->panel_settings['rsvp_admin_receive_emailfrom'],
			'el_properties' => array( 'class' => 'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_admin_receive_emailsub',
			'label'         => __( 'Email Subject for RSVP Status', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => $this->panel_settings['rsvp_admin_receive_emailsub'],
			'el_properties' => array( 'class' => 'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_admin_receive_subchange',
			'label'         => __( 'Email Subject for RSVP Change Status', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => $this->panel_settings['rsvp_admin_receive_subchange'],
			'el_properties' => array( 'class' => 'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_admin_receive_email',
			'type'          => 'textarea',
			'label'         => __( 'Email content', 'rhcrsvp' ),
			'default'       => $this->panel_settings['rsvp_admin_receive_email'],
			'el_properties' => array( 'class' => 'text-width-full', 'rows' => 10 ),
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'   => 'hide_admin_email_group',
			'type' => 'div_end'
		);

		$t[ $i ]->options[] = (object) array(
			'type'  => 'subtitle',
			'label' => __( 'Organizer Notification', 'rhcrsvp' )
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_organizer_email',
			'type'          => 'onoff',
			'label'         => __( 'Enable Organizer Notification', 'rhcrsvp' ),
			'default'       => $this->panel_settings['rsvp_organizer_email'],
			'el_properties' => array(),
			'hidegroup'     => '#hide_organizer_email_group',
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'   => 'hide_organizer_email_group',
			'type' => 'div_start'
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_organizer_receive_emailfrom',
			'label'         => __( 'Email From', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => $this->panel_settings['rsvp_organizer_receive_emailfrom'],
			'el_properties' => array( 'class' => 'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_organizer_receive_emailsub',
			'label'         => __( 'Email Subject for RSVP Status', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => $this->panel_settings['rsvp_organizer_receive_emailsub'],
			'el_properties' => array( 'class' =>'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_organizer_receive_subchange',
			'label'         => __( 'Email Subject for RSVP Change Status', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => $this->panel_settings['rsvp_organizer_receive_subchange'],
			'el_properties' => array( 'class' =>'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_organizer_receive_email',
			'type'          => 'textarea',
			'label'         => __( 'Email content', 'rhcrsvp' ),
			'default'       => $this->panel_settings['rsvp_organizer_receive_email'],
			'el_properties' => array( 'class' => 'text-width-full', 'rows' => 10 ),
			'save_option'  => true,
			'load_option'  => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'   => 'hide_organizer_email_group',
			'type' => 'div_end'
		);

		$t[ $i ]->options[] = (object) array(
			'type' => 'clear'
		);

		$t[ $i ]->options[] = (object) array(
			'type'  => 'submit',
			'label' => __( 'Save', 'rhc' ),
			'class' => 'button-primary'
		);
		
		$i = count( $t );
		$t[ $i ]                = (object) array();
		$t[ $i ]->id            = 'rsvp-cap'; 
		$t[ $i ]->label         = __( 'Feature Access', 'rhcrsvp' );
		$t[ $i ]->right_label   = __( 'Feature Access', 'rhcrsvp' );
		$t[ $i ]->page_title    = __( 'Feature Access', 'rhcrsvp' );
		$t[ $i ]->priority      = 5;
		$t[ $i ]->theme_option  = true;
		$t[ $i ]->plugin_option = true;
		$t[ $i ]->options = array();

		$excluded_roles = array();
		$roles = get_editable_roles();
		
		if ( is_array( $roles ) && count( $roles ) > 0 ) {
			foreach( $roles as $role => $r ) {
				if ( isset( $r['capabilities'] ) && isset( $r['capabilities']['manage_options'] ) && '1' == $r['capabilities']['manage_options'] ) {
					$excluded_roles[] = $role;
				}
			}
		}

		$capabilities = array(
			'rsvp_read_list'     => __( 'Read List', 'rhcrsvp' ),
			'rsvp_delete'        => __( 'Delete RSVP', 'rhcrsvp' ),
			'rsvp_change_status' => __( 'Change status RSVP', 'rhcrsvp' )
		);

		$t[ $i ]->options[] = (object) array(
			'id'             => 'rsvp_caps',
			'label'          => '',
			'type'           => 'rolemanager',
			'capabilities'   => $capabilities,
			'excluded_roles' => $excluded_roles,
			'el_properties'  => array()
		);

		$t[ $i ]->options[] = (object) array(
			'type'=>'clear'
		);

		$t[ $i ]->options[] = (object) array(
			'type'  => 'submit',
			'label' => __( 'Save', 'rhc' ),
			'class' => 'button-primary'
		);

		$i = count( $t );
		$t[ $i ]                = (object) array();
		$t[ $i ]->id            = 'rhc-rsvp-social-connect'; 
		$t[ $i ]->label         = __( 'Social Connect', 'rhcrsvp' );
		$t[ $i ]->right_label   = __( 'Social Connect', 'rhcrsvp' );
		$t[ $i ]->page_title    = __( 'Social Connect', 'rhcrsvp' );
		$t[ $i ]->theme_option  = true;
		$t[ $i ]->plugin_option = true;
		$t[ $i ]->priority      = 6;
		$t[ $i ]->options       = array();

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_facebook_show',
			'type'          => 'onoff',
			'label'         => __( 'Show Facebook Button', 'rhcrsvp' ),
			'description'   => __( 'Adds a Facebook button to the RSVP box', 'rhcrsvp' ),
			'default'       => '1',
			'el_properties' => array(),
			'hidegroup'     => '#hide_facebook_show_group',
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'   => 'hide_facebook_show_group',
			'type' => 'div_start'
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'text_social_facebook',
			'label'         => __( 'Facebook Button', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => empty( $this->panel_settings['text_social_facebook'] ) ? '' : $this->panel_settings['text_social_facebook'],
			'el_properties' => array( 'class' => 'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);
		
		$t[ $i ]->options[] = (object) array(
			'type' => 'div_end'
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_googleplus_show',
			'type'          => 'onoff',
			'label'         => __( 'Show Google+ Button', 'rhcrsvp' ),
			'description'   => __( 'Adds a Google+ button to the RSVP Box', 'rhcrsvp' ),
			'default'       => '0',
			'el_properties' => array(),
			'hidegroup'     => '#hide_rsvp_googleplus_show',
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'   => 'hide_rsvp_googleplus_show',
			'type' => 'div_start'
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'text_social_googleplus',
			'label'         => __( 'Googleplus button', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => empty( $this->panel_settings['text_social_googleplus'] ) ? : '', $this->panel_settings['text_social_googleplus'],
			'el_properties' => array( 'class' => 'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'type' => 'div_end'
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_linkedin_show',
			'type'          => 'onoff',
			'label'         => __( 'Show LinkedIn Button', 'rhcrsvp' ),
			'description'   => __( 'Adds a LinkedIn button to the Ratings and Reviews Box', 'rhcrsvp' ),
			'default'       => '0',
			'el_properties' => array(),
			'hidegroup'     => '#hide_rsvp_linkedin_show',
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'   => 'hide_rsvp_linkedin_show',
			'type' => 'div_start'
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'text_social_linkedin',
			'label'         => __( 'LinkedIn Button', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => empty( $this->panel_settings['text_social_linkedin'] ) ? '' : $this->panel_settings['text_social_linkedin'],
			'el_properties' => array( 'class' => 'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);
		
		$t[ $i ]->options[] = (object) array(
			'type' => 'div_end'
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_twitter_show',
			'type'          => 'onoff',
			'label'         => __( 'Show Twitter Button', 'rhcrsvp' ),
			'description'   => __( 'Adds a Twitter button to the RSVP box', 'rhcrsvp' ),
			'default'       => '1',
			'el_properties' => array(),
			'hidegroup'      => '#hide_rsvp_twitter_show',
			'save_option'    => true,
			'load_option'    => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'hide_rsvp_twitter_show',
			'type'          => 'div_start',
			'el_properties' => array()
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'text_social_twitter',
			'label'         => __( 'Twitter Button', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => empty( $this->panel_settings['text_social_twitter'] ) ? '' : $this->panel_settings['text_social_twitter'],
			'el_properties' => array( 'class' => 'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);
		
		$t[ $i ]->options[] = (object) array(
			'type' => 'div_end'
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_wp_show',
			'type'          => 'onoff',
			'label'         => __( 'Show WordPress Button', 'rhcrsvp' ),
			'description'   => __( 'Adds a WordPress button to the RSVP box', 'rhcrsvp' ),
			'default'       => 0,
			'el_properties' => array(),
			'hidegroup'      => '#hide_rsvp_wp_show',
			'save_option'    => true,
			'load_option'    => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'   => 'hide_rsvp_wp_show',
			'type' => 'div_start'
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'text_social_wp',
			'label'         => __( 'WordPress Button', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => empty( $this->panel_settings['text_social_wp'] ) ? '' : $this->panel_settings['text_social_wp'],
			'el_properties' => array( 'class' => 'text-width-full' ),
			'save_option'   => true,
			'load_option'   => true
		);
		
		$t[ $i ]->options[] = (object) array(
			'type' => 'div_end'
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_manualjoin_show',
			'type'          => 'onoff',
			'label'         => __( 'Show Manual RSVP Button', 'rhcrsvp' ),
			'description'   => __( 'Adds a button for manual RSVP', 'rhcrsvp' ),
			'default'       => 0,
			'el_properties' => array(),
			'hidegroup'     => '#hide_rsvp_manualjoin_show',
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'   => 'hide_rsvp_manualjoin_show',
			'type' => 'div_start'
		);
		
		$t[ $i ]->options[] = (object) array(
			'id'            => 'text_button_manual_join',
			'label'         => __( 'Manual RSVP Button', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => empty( $this->panel_settings['text_button_manual_join'] ) ? '' : $this->panel_settings['text_button_manual_join'],
			'el_properties' => array( 'class' => 'text-width-full' ),
			'save_option'    => true,
			'load_option'    => true
		);
		
		$t[ $i ]->options[] = (object) array(
			'type' => 'div_end'
		);

		$t[ $i ]->options[] = (object) array(
			'type' => 'clear'
		);
		
		$t[ $i ]->options[] = (object) array(
			'type'  => 'submit',
			'label' => __( 'Save', 'rhc' ),
			'class' => 'button-primary'
		);

		$i = count( $t );
		$t[ $i ]                = (object) array();
		$t[ $i ]->id            = 'rsvp-facebook-settings';
		$t[ $i ]->label         = __( 'Facebook Settings', 'rhcrsvp' );
		$t[ $i ]->right_label   = __( 'Facebook Settings', 'rhcrsvp' );
		$t[ $i ]->page_title    = __( 'Facebook Settings', 'rhcrsvp' );
		$t[ $i ]->priority      = 7;
		$t[ $i ]->theme_option  = true;
		$t[ $i ]->plugin_option = true;
		$t[ $i ]->options = array();

		$t[ $i ]->options[] = (object) array(
			'type'  => 'subtitle',
			'label' => __( 'Replacement tag', 'rhcrsvp' )
		);

		$t[ $i ]->options[] = (object) array(
			'type'     => 'callback',
			'callback' => array( $this, 'list_of_pages_with_shortcode' )
		);

		$t[ $i ]->options[] = (object) array(
			'type'  => 'subtitle',
			'label' => __( 'Facebook Application Settings', 'rhcrsvp' )
		);

		$t[ $i ]->options[] = (object) array(
			'id'           => 'rsvp_facebook_publish',
			'type'         => 'onoff',
			'label'        => __( 'Publish RSVP to Facebook', 'rhcrsvp' ),
			'description'  => __( 'Publish attending status to Facebook', 'rhcrsvp' ),
			'default'      => '0',
			'save_option'  => true,
			'load_option'  => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_public_facebook_title',
			'label'         => __( 'Facebook title', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => 'I am going to [TITLE]. Join me!',
			'el_properties' => array(
				'class' => 'text-width-full'
			),
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_public_facebook_subtitle',
			'label'         => __( 'Facebook subtitle', 'rhcrsvp' ),
			'type'          => 'text',
			'default'       => '[STARTDATE] @ [STARTTIME]',
			'el_properties' => array(
				'class' => 'text-width-full'
			),
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_public_facebook_text',
			'label'         => __( 'Facebook content', 'rhcrsvp' ),
			'type'          => 'textarea',
			'default'       => '[EXCERPT]',
			'el_properties' => array(
				'class' => 'text-width-full'
			),
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'type'  => 'submit',
			'label' => __( 'Save', 'rhc' ),
			'class' => 'button-primary'
		);

		$i = count( $t );
		$t[ $i ]                = (object) array();
		$t[ $i ]->id            = 'rsvp-twitter-settings';
		$t[ $i ]->label         = __( 'Twitter Settings', 'rhcrsvp' );
		$t[ $i ]->right_label   = __( 'Twitter Settings', 'rhcrsvp' );
		$t[ $i ]->page_title    = __( 'Twitter Settings', 'rhcrsvp' );
		$t[ $i ]->theme_option  = true;
		$t[ $i ]->plugin_option = true;
		$t[ $i ]->priority      = 8;
		$t[ $i ]->options = array();

		$t[ $i ]->options[] = (object) array(
			'type'  => 'subtitle',
			'label' => __( 'Replacement tag', 'rhcrsvp' )
		);

		$t[ $i ]->options[] = (object) array(
			'type'     => 'callback',
			'callback' => array( $this, 'list_of_pages_with_shortcode' )
		);

		$t[ $i ]->options[] = (object) array(
			'type'  => 'subtitle',
			'label' => __( 'Twitter Application Settings', 'rhcrsvp' )
		);

		$t[ $i ]->options[] = (object) array(
			'id'           => 'rsvp_twitter_publish',
			'type'         => 'onoff',
			'label'        => __( 'Publish RSVP to Twitter', 'rhcrsvp' ),
			'description'  => __( 'Publish attending status to Twitter', 'rhcrsvp' ),
			'default'      => '0',
			'save_option'  => true,
			'load_option'  => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_public_twitter_text',
			'label'         => __( 'Twitter content', 'rhcrsvp' ),
			'type'          => 'textarea',
			'default'       => 'I am going to [TITLE]. Join me! [LINK]',
			'el_properties' => array(
				'class' => 'text-width-full'
			),
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'type' => 'clear'
		);

		$t[ $i ]->options[] = (object) array(
			'type'  => 'submit',
			'label' => __( 'Save', 'rhc' ),
			'class' => 'button-primary'
		);

		$i = count( $t );
		$t[ $i ] = (object) array();
		$t[ $i ]->id            = 'rsvp-troubleshooting';
		$t[ $i ]->label         = __( 'Troubleshooting', 'rhcrsvp' );
		$t[ $i ]->right_label   = __( 'Troubleshooting Settings', 'rhcrsvp' );
		$t[ $i ]->page_title	= __( 'Troubleshooting', 'rhcrsvp' );
		$t[ $i ]->priority      = 98;
		$t[ $i ]->theme_option  = true;
		$t[ $i ]->plugin_option = true;
		$t[ $i ]->options = array();

		$t[ $i ]->options[] = (object) array(
			'id'            => 'rsvp_csv_symbole',
			'type'          => 'select',
			'label'         => __( 'Change CSV format', 'rhcrsvp' ),
			'options'       => array(
				''  => '(;) Semicolon',
				',' => '(,) Comma'
			),
			'description'   => __( 'If you have problems the imported data properly in Excel (Microsoft) or Numbers (Mac) this can be due to the character-separated value used. You can switch between comma and semicolon.', 'rhcrsvp' ),
			'el_properties' => array(),
			'default'       => '',
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => 'convertolddata',
			'type'          => 'onoff',
			'label'         => __( 'Convert old data', 'rhcrsvp' ),
			'description'   => __( 'Warning. make backup of your sql system to be safe, of no data lose in rsvp.', 'rhcrsvp' ),
			'el_properties' => array(),
			'default'       => false,
			'save_option'   => true,
			'load_option'   => true
		);

		$t[ $i ]->options[] = (object) array(
			'type' => 'clear'
		);
		
		$t[ $i ]->options[] = (object) array(
			'type'  => 'submit',
			'label' => __( 'Save', 'rhc' ),
			'class' => 'button-primary'
		);

		$i = count( $t );
		$t[ $i ]                = (object) array();
		$t[ $i ]->id            = 'rsvp-help';
		$t[ $i ]->label         = __( 'Help', 'rhcrsvp' );
		$t[ $i ]->right_label   = __( 'Help', 'rhcrsvp' );
		$t[ $i ]->page_title    = __( 'Help', 'rhcrsvp' );
		$t[ $i ]->priority      = 99;
		$t[ $i ]->theme_option  = true;
		$t[ $i ]->plugin_option = true;
		$t[ $i ]->options       = array();

		$t[ $i ]->options[] = (object) array(
			'type'     => 'callback',
			'callback' => array( $this, 'cb_help_tab' ),
		);
		
		return $t;
	}

	public function cb_help_tab() {
		return sprintf( '%s<br><br>',
			sprintf( __( 'For detailed information on how to use the RSVP Events add-on, please visit our knowledge base at the %s', 'rhcrsvp' ),
				sprintf( '<a href="https://righthere.zendesk.com/">%s</a>.', __( 'Help Center', 'rhcrsvp' )
				)
			)
		);
	}
}

return new RHC_RSVP_Options();