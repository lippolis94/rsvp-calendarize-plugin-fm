<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Dinah!' );
}

class RHC_RSVP_Metabox extends tools_rsvp {
	public function __construct( $post_type = RHC_EVENTS ) {
		parent::__construct();

		if ( ! class_exists( 'post_meta_boxes' ) ) {
			require_once RHC_PATH . 'includes/class.post_meta_boxes.php';
		}

		$this->post_meta_boxes = new post_meta_boxes( array(
			'post_type'           => $post_type,
			'ptclass'             => 'rhc_rsvp_metabox',
			'options'             => $this->metaboxes(),
			'styles'              => array( 'rhc-rsvp-metabox', 'wp-color-picker', 'tooltipster' ),
			'scripts'             => array( 'rhc-rsvp-metabox', 'tooltipster' ),
			'metabox_meta_fields' => 'rhc_rsvp_metabox_meta_fields',
			'pluginpath'          => RHC_PATH
		) );

		$this->post_meta_boxes->save_fields = array(
			'_enable_rsvp'
		);

		add_action( 'wp_ajax_rhc_rsvp_add_field', array( $this, 'ajax_add_field' ) );
		add_action( 'wp_ajax_rhc_rsvp_get_fields', array( $this, 'ajax_get_fields' ) );
		add_action( 'wp_ajax_rhc_rsvp_delete_field', array( $this, 'ajax_manage_fields' ) );
		add_action( 'wp_ajax_rhc_rsvp_update_field', array( $this, 'ajax_manage_fields' ) );
		add_action( 'wp_ajax_rhc_rsvp_sort_fields', array( $this, 'ajax_manage_fields' ) );
	}

	public function metaboxes( $t = array() ) {
		$i = count( $t );
		$t[ $i ]                = (object) array();
		$t[ $i ]->id            = 'rhc-rsvp';
		$t[ $i ]->label         = __( 'RSVP Settings', 'rhcrsvp' );
		$t[ $i ]->right_label   = __( 'RSVP Settings', 'rhcrsvp' );
		$t[ $i ]->page_title    = __( 'RSVP Settings', 'rhcrsvp' );
		$t[ $i ]->theme_option  = true;
		$t[ $i ]->plugin_option = true;
		$t[ $i ]->priority      = 'default';
		$t[ $i ]->type          = '';
		$t[ $i ]->options       = array();

		$t[ $i ]->options[] = (object) array(
			'type'    => 'subtitle',
			'label'   => __( 'Enable RSVP', 'rhcrsvp' ),
			'ptclass' => 'enable_rsvp_subtitle'
		);

		$t[ $i ]->options[] = (object) array(
			'id'            => '_enable_rsvp',
			'type'          => 'onoff',
			'default'       => '0',
			'description'   => __( 'Turn ON this option in order to enable Calendarize it! RSVP', 'rhcrsvp' ),
			'el_properties'	=> array(),
			'ptclass'		=> 'rhc_enable_rsvp_holder',
			'hidegroup'     => '#rsvp_enable_group',
			'save_option'	=> true,
			'load_option'	=> true
		);

		$t[ $i ]->options[] = (object) array(
			'type' => 'clear'
		);

		$t[ $i ]->options[] = (object) array(
			'id'   => 'rsvp_enable_group',
			'type' => 'div_start'
		);

		$t[ $i ]->options[] = (object) array(
			'type'    => 'subtitle',
			'label'   => __( 'Fields Setup', 'rhcrsvp' ),
			'ptclass' => 'wct_enabled_group create_fields_subtitle'
		);

		$t[ $i ]->options[] = (object) array(
			'id'          => 'cb_fields',
			'type'        => 'callback',
			'callback'    => array( $this, 'cb_fields'),
			'save_option' => false,
			'load_option' => false
		);

		$t[ $i ]->options[] = (object) array(
			'type' => 'div_end'
		);

		return $t;
	}

	public function cb_fields() {
		include( RHCRSVP_PATH . 'templates/html-metabox-field-setup.php' );
	}

	public function ajax_add_field() {
		$post_id = absint( $_POST['post_id'] );
		$field_type = absint( $_POST['field_type'] );
		
		$existing_fields = $this->get_fields( $post_id );
		$fields = is_array( $existing_fields ) && $existing_fields ? $existing_fields : array();
		
		$field_id = count( $fields ) + 1;
		$field_colors = $this->get_layout_choose_fields_color( $field_type );
		$field = array(
			'choose_id'      => $field_id,
			'choose_type'    => $field_type,
			'choose_text'    => $this->get_layout_choose_fields_title( $field_type ),
			'choose_subtext' => $this->get_layout_choose_fields_subtitle( $field_type ),
			'choose_slug'    => $this->get_layout_choose_fields_slug( $field_type ),
			'choose_limit'   => $this->get_layout_choose_fields_limit( $field_type ),
			'normal_color'   => $field_colors[0],
			'hover_color'    => $field_colors[1],
			'text_color'     => $field_colors[2],
			'choose_allow'   => $this->get_layout_choose_fields_allow( $field_type )
		);

		$fields[ 'id_' . $field_id ] = $field;

		$this->update_post_fields( $_POST['post_id'], $fields );

		$response = (object) array(
			'R'    => 'OK',
			'DATA' => $field,
		);

		die( json_encode( $response ) );
	}

	public function ajax_get_fields() {
		$response = (object) array(
			'R'    => 'OK',
			'DATA' => $this->get_fields( $_POST['post_id'], true )
		);

		die( json_encode( $response ) );
	}

	public function ajax_manage_fields() {
		$fields = $this->get_fields( $_POST['post_id'] );

		if ( ! is_array( $fields ) && ! $fields ) {
			return;
		}

		if ( ! empty( $_POST['field_id'] ) ) {
			$key = 'id_' . $_POST['field_id'];
			
			if ( ! empty( $fields[ $key ] ) ) {
				if ( strpos( $_POST['action'], 'update' ) ) {
					$fields[ $key ] = $_POST['field'];
				} elseif ( strpos( $_POST['action'], 'delete' ) ) {
					unset( $fields[ $key ] );
				}
			}
		} elseif ( ! empty( $_POST['order'] ) && is_array( $_POST['order'] ) ) {
			if ( strpos( $_POST['action'], 'sort' ) ) {
				$old_fields = $fields;
				$fields = array();
				
				foreach ( $_POST['order'] as $order_id ) {
					$key = 'id_' . $order_id;

					if ( ! empty( $old_fields[ $key ] ) ) {
						$fields[ $key ] = $old_fields[ $key ];
					}
				}
			}
		}

		$this->update_post_fields( $_POST['post_id'], $fields );

		$response = (object) array(
			'R' => 'OK',
		);

		die( json_encode( $response ) );
	}
}

return new RHC_RSVP_Metabox();