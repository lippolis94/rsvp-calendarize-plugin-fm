<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Dinah!' );
}

class RHC_RSVP_Assets {
	public $is_admin = false;
	public $is_debug = false;
	public $in_footer = false;

	function __construct() {
		global $rhc_plugin;

		$this->is_admin = is_admin();

		if ( '1' == $rhc_plugin->get_option( 'debug_javascript', false, true ) ) {
			$this->is_debug = true;
		}

		if ( '1' == $rhc_plugin->get_option( 'in_footer', false, true ) ) {
			$this->in_footer = true;
		}

		if ( $this->is_admin ) {
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_and_frontend_assets' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_only_assets' ) );
		} else {
			add_action( 'wp_enqueue_scripts', array( $this, 'admin_and_frontend_assets' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'frontend_only_assets' ) );
		}
	}

	function admin_and_frontend_assets() {
	}

	function admin_only_assets() {
		$suffix = '';

		wp_register_style( 'rhc-rsvp-metabox', RHCRSVP_URL . 'css/metabox'. $suffix . '.css', false, RHCRSVP_VERSION );

		wp_register_script( 'rhc-rsvp-metabox', RHCRSVP_URL . 'js/metabox' . $suffix . '.js', array( 'jquery', 'wp-color-picker', 'rhc-rsvp-sortable' ), RHCRSVP_VERSION, $this->in_footer );
		wp_register_script( 'rhc-rsvp-sortable', RHCRSVP_URL . 'js/sortable' . $suffix . '.js', array(), '1.5.0-rc1', $this->in_footer );
	}

	function frontend_only_assets() {
	}

}

return new RHC_RSVP_Assets();