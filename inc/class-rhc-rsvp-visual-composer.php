<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Dinah!' );
}

class RHC_RSVP_Visual_Composer {
	public function __construct() {
		add_action( 'rhc_vc_before_init', array( $this, 'before_init' ), 11 );
		add_filter( 'rhc_vc_shortcode_subcategories', array( $this, 'shortcode_subcategories' ) );
	}

	public function before_init() {
		vc_map( array(
			'name'        => __( 'RSVP Box', 'rhcrsvp' ),
			'base'        => 'rhc_rsvp',
			'category'    => 'Calendarize It!',
			'description' => __( 'Add RSVP Box', 'rhcrsvp' ),
			'params' => array(
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'class'       => '',
					'heading'     => __( 'Event ID', 'rhcrsvp' ),
					'param_name'  => 'post_id',
					'value'       => '',
					'description' => __( 'Render RSVP Box from a specific Event', 'rhcrsvp' ),
					'group'       => __( 'Base', 'rhcrsvp' )
				)
			)
		) );
	}

	public function shortcode_subcategories( $data ) {
		$data['rhc_rsvp'] = array( 'rhc-box', 'rhc-custom' );
		
		return $data;
	}
}

return new RHC_RSVP_Visual_Composer();