<?php

class RSVP_Limits_FM
{
    public function __construct()
    {
        
    }

    /**
     * Metodo per ricavare il totale dei posti disponibili di RSVP relativi ad un post
     * @param int $post_id
     * @return int
     */
    protected static function getEventParticipantsLimits ( $post_id = 0 )
    {
      $fields = get_post_meta( $post_id, '_rhc_rsvp_fields', true );
      
      if($fields && is_array($fields)) 
        $fields = array_values( $fields );

      return (int)$fields[0]['choose_limit'];
    }

    /**
     * Metodo per ricavare il numero di partecipanti dato un post id
     * @param int $post_id
     * @return int
     */
    protected static function getParticipantsInt ( $post_id = 0 )
    {
      global $wpdb;

      $conteggio_utenti = $wpdb->get_var(
        $wpdb->prepare( 
          "
            SELECT COUNT(*)
            FROM {$wpdb->prefix}rhc_rsvp
            WHERE postID = %s
          ", 
          $post_id
        ) 
      );

      return (int)$conteggio_utenti;
    }

    /**
     * Invio email per avvisare del raggiungimento del limite di iscrizioni
     * @param int $post_id
     * @param int $subscription_left
     */
    protected static function sendLimitEmail($post_id = 0, $subscription_left = 0)
    {
        $event = get_post($post_id);
        $event_title = $event->post_title;

        $fm_rsvp_options = get_option( 'FM_RSVP_settings' );
        $to = $fm_rsvp_options['fm_alert_email_limits'];

        $message = "Mancano {$subscription_left} posti all'evento: {$event_title}";
        wp_mail($to, "Raggiungimento posti limite - Evento: {$event_title}", $message);
    }

    /**
     * Metodo che controlla se si sta raggiungendo il limite di iscrizioni per un evento, in caso
     * manchino solo 6 posti invia una mail all'admin
     * @param int $post_id
     */
    public static function checkLimits($post_id = 0)
    {
      $limit = self::getEventParticipantsLimits($post_id);
      $totalSubscription = self::getParticipantsInt($post_id);
      if(($limit - $totalSubscription) <= 6)
      {
        self::sendLimitEmail($post_id, ($limit - $totalSubscription));
      }
    }

}

new RSVP_Limits_FM();