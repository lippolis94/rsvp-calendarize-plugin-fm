<?php

if ( ! class_exists( 'uh_social_loader' ) ) :

class uh_social_loader {
	protected $first_time = false;
	protected $version_file_loaded = '';
	protected $version = null;
	protected $inputdate = array();

	function __construct( $inputdata = array(), $file_location = '', $version = '1.0.0' ) {
		if ( ! $this->first_time ) {
			add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ), 9999 );
			
			$this->first_time = true;
		}

		$this->inputdate[] = $inputdata;

		if ( version_compare( $version, $this->version, '>' ) ) {
			$this->version = $version;
			$this->version_file_loaded = $file_location . 'social_connection.php';
		}
	}
	
	function plugins_loaded() {
		if ( ! empty( $this->version_file_loaded ) ) {
			require_once $this->version_file_loaded;

			if ( ! empty( $this->inputdate ) ) {
				$social_class = new uh_social_connection_versioncontrole();

				foreach ( $this->inputdate as $tmp ) {
					$social_class->init( $tmp );
				}
			}
		}
	}
}

endif;

global $uh_social_loader;
$uh_social_loader = new uh_social_loader();
