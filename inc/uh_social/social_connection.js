if ( 'function' !== typeof uh_social_connection ) {
	var uh_already_init_fb = false;
	var uh_tmp_data = '';
	var uh_tmp_function = '';
	var uh_tmp_extra = '';
	var uh_tmp_direction = '';
	var uh_tmp_alwayslogin = '';
	var uh_kill_count = 0;
	
	function uh_social_connection_facebook_init(){
		FB.init({
				appId      : UH_social_vars.fb_appID, // App ID
				channelUrl : UH_social_vars.siteurl,
				xfbml      : true,  // parse XFBML
				version	   : 'v2.8'
		});
		
		FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
			    uh_already_init_fb = true;
			}
		});
	}
	

	function uh_social_connection(vars){
		var vars = vars;
		var twitter_data = 0;
		var self = this;

		if(typeof(UH_social_vars) != 'undefined' && typeof(UH_social_vars.fb_appID) != 'undefined' && UH_social_vars.fb_appID != ''){
			window.fbAsyncInit = function() {uh_social_connection_facebook_init();};
		}
		
		if(typeof(this.vars) == 'undefined'){
			this.vars = new Object();
			if(typeof(UH_social_vars) != 'undefined'){
				if(typeof(UH_social_vars.fb_appID) != 'undefined'){
					this.vars.fb_appID = parseFloat(UH_social_vars.fb_appID);
				}
				if(typeof(UH_social_vars.siteurl) != 'undefined'){
					this.vars.siteurl = UH_social_vars.siteurl;
				}
				
				if(typeof(UH_social_vars.googleplus_clientId) != 'undefined'){
					this.vars.googleplus_clientId = UH_social_vars.googleplus_clientId;
				}	
				
				if(typeof(UH_social_vars.googleplus_apiKey) != 'undefined'){
					this.vars.googleplus_apiKey = UH_social_vars.googleplus_apiKey;
				}	
				
				if(typeof(UH_social_vars.wp_redirect_url) != 'undefined'){
					this.vars.wp_redirect_url = UH_social_vars.wp_redirect_url;
				}	
				
				if(typeof(UH_social_vars.wp_is_login) != 'undefined'){
					this.vars.wp_is_login = UH_social_vars.wp_is_login;
				}			
				
				if(typeof(UH_social_vars.wp_runthrow) != 'undefined'){
					this.vars.wp_runthrow = UH_social_vars.wp_runthrow;
				}						
			}
		}
		
		// access
		this.facebook_get=facebook_get;		
		this.facebook_logout = facebook_logout;
		this.facebook_publish = facebook_publish;

		this.twitter_czget=twitter_czget;
		this.twitter_get=twitter_get;
		this.twitter_publish=twitter_publish;
		this.twitter_logout=twitter_logout;
		
		this.linkedin_get=linkedin_get;
		this.linkedin_publish = linkedin_publish;
		this.linkedin_logout=linkedin_logout;
		
		this.googleplus_get=googleplus_get;
		this.googleplus_publish = googleplus_publish;
		this.googleplus_logout=googleplus_logout;	
		
		this.loginby = loginby;
		
		function loginby(tmp_name,tmp_function,tmp_extra){
			uh_tmp_function = tmp_function;
			uh_tmp_extra = tmp_extra;
			
			if(tmp_name == 'wp'){
				wp_get_loginby(tmp_function,tmp_extra);
			} else if(tmp_name == 'twitter'){
				twitter_get_loginby(tmp_function,tmp_extra);
			} else if(tmp_name == 'facebook'){
				//facebook_logout();
				facebook_get(tmp_function,'',tmp_extra);
			} else if(tmp_name == 'linkedin'){
				IN.User.logout(function(){linkedin_get(tmp_function,'',tmp_extra)});
			} else if(tmp_name == 'googleplus'){
				uh_tmp_direction = 'googleplus_return';
				uh_tmp_alwayslogin = true;
				googleplus_logout()
				window.setTimeout(function(){googleplus_init()},100);
			}
			
		}
		
		
		// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		//facebook
		// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


		function facebook_init(){
			uh_social_connection_facebook_init();
		}

		function facebook_get(tmp_function,tmp_data,tmp_extra){
			uh_tmp_data = tmp_data;
			uh_tmp_function = tmp_function;
			uh_tmp_extra = tmp_extra;
			
			if(uh_already_init_fb){
				uh_kill_count = 0;
				FB.getLoginStatus(facebook_login_check);
			} else {
				if(uh_kill_count < 10){
					setTimeout(function() {
						uh_kill_count++;
						facebook_init();
						window.setTimeout(function(){facebook_get(tmp_function,tmp_data,tmp_extra)},100);	
					}, 25);
					

				} else {
					uh_kill_count = 0;
					uh_already_init_fb = true;
					facebook_get(tmp_function,tmp_data,tmp_extra);	
				}

			}
			
			
			return false;
		}
		
		function facebook_login_check(response){
			if (response.status === 'connected') {
				facebook_getdata(uh_tmp_function,uh_tmp_data);
				return true;
			} else {
				FB.login(facebook_login, {scope: 'email'});
			};
		}

		function facebook_logout(){
			FB.logout();
		}
		
		function facebook_publish( tmp_name, tmp_link, tmp_pic, tmp_cap, tmp_des ) {
			FB.ui(
				{
					method: 'feed',
					name: tmp_name,
					link: tmp_link,
					picture: tmp_pic,
					caption: tmp_cap,
					description: tmp_des
				}
			);
		}

		function facebook_login(response){
			if (response.authResponse) {
				facebook_getdata(uh_tmp_function,uh_tmp_data);
				return true;
			} else {
				console.log('User cancelled login or did not fully authorize.');
			}
		}
		
		function facebook_getdata( tmp_function, tmp_data ) {
			if ( 'undefined' === typeof tmp_data || '' === tmp_data ) {
				tmp_data = '/me?fields=id,picture.width(75).height(75),email,first_name,last_name,link';
				var fields = { fields: 'id,picture.width(75).height(75),email,first_name,last_name,link' };
			}
			
			FB.api( '/me', fields, function(response) {
				var fn = window[ tmp_function ];

				if( 'function' === typeof fn ) {
					fn( response, uh_tmp_extra, 'facebook' );

					uh_tmp_data = '';
					uh_tmp_function = '';
					uh_tmp_extra = '';
				}
			});
		}
		
		
		// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		// wordpress
		// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		
		function wp_get_loginby(tmp_returnfunction,tmp_data){
			var tmp_query = '';
			if(typeof tmp_data != 'undefined' && tmp_data != ''){
				tmp_query = serialize(tmp_data);
			}
			if(self.vars.wp_is_login){
				var win = window.open(self.vars.wp_runthrow+'&return_function='+tmp_returnfunction+'&'+tmp_query,'wp','width=500,height=700');	
			} else {
				tmp_query = tmp_query.replace('&','%26');
				tmp_query = tmp_query.replace('=','%3D');
				
				var win = window.open(self.vars.wp_redirect_url+'%26return_function%3D'+tmp_returnfunction+'%26'+tmp_query,'wp','width=500,height=700');	
			}
			return false;
		}

		// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		// twitter
		// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		function twitter_czget(tmp_returnfunction,tmp_data,tmp_getdata){
			var tmp_query = '';
			if(typeof tmp_data != 'undefined' && tmp_data != ''){
				tmp_query = serialize(tmp_data);
			}
			if(typeof tmp_getdata == 'undefined' || tmp_getdata == ''){
				tmp_getdata = 'account/verify_credentials';
			}	
			var win = window.open(self.vars.siteurl+'/?uh_social_data=true&uh_fn=twitter&return_function='+tmp_returnfunction+'&get_data='+tmp_getdata+'&'+tmp_query,'twitter','width=500,height=300');
			return false;
		}
		
		function twitter_get_loginby(tmp_returnfunction,tmp_data){
			var tmp_query = '';
			if(typeof tmp_data != 'undefined' && tmp_data != ''){
				tmp_query = serialize(tmp_data);
			}
			var win = window.open(self.vars.siteurl+'/?uh_social_data=true&uh_fn=twitter&loginalways=true&return_function='+tmp_returnfunction+'&get_data=account/verify_credentials&'+tmp_query,'twitter','width=500,height=300');
			return false;
		}
		
		function twitter_get(tmp_returnfunction,tmp_data,tmp_getdata){
			var tmp_query = '';
			if(typeof tmp_data != 'undefined' && tmp_data != ''){
				tmp_query = serialize(tmp_data);
			}
			if(typeof tmp_getdata == 'undefined' || tmp_getdata == ''){
				tmp_getdata = 'account/verify_credentials';
			}
			
			var win = window.open(self.vars.siteurl+'/?uh_social_data=true&uh_fn=twitter_get&return_function='+tmp_returnfunction+'&get_data='+tmp_getdata+'&'+tmp_query,'twitter','width=500,height=300');
			return false;
		}
		
		function twitter_publish(tmp_data,tmp_returnfunction){
			var tmp_query = '';
			if(typeof tmp_data != 'undefined' && tmp_data != ''){
				tmp_query = serialize(tmp_data);
			}

			
			var win = window.open(self.vars.siteurl+'/?uh_social_data=true&uh_fn=twitter_publish&return_function='+tmp_returnfunction+'&'+tmp_query,'twitter','width=500,height=300');
			return false;
		}

		function twitter_logout(){
		 jQuery.post(self.vars.siteurl+'/?uh_social_data=true&uh_fn=twitter_destry_session=true');	
			return false;
		}
		
		
		
		// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		// google plus
		// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			
	      function googleplus_init() {
	        //gapi.client.setApiKey(self.vars.googleplus_apiKey);
	        googleplus_checkAuth();
	      }
	

	      function googleplus_checkAuth(checkup) {
	        if(checkup === '' || checkup == undefined || checkup == 'undefined'){
		        checkup = true;
	        }
	        
	        if(uh_tmp_alwayslogin){
		        checkup = false;
	        }
	        
	        gapi.auth.authorize({client_id: self.vars.googleplus_clientId, scope: 'https://www.googleapis.com/auth/plus.me', immediate: checkup}, googleplus_handle);
	      }
	
	
	      function googleplus_handle(authResult) {

	        if (authResult && !authResult.error) {
				if(uh_tmp_direction == 'googleplus_return'){
					googleplus_return();
				} else if(uh_tmp_direction == 'googleplus_publish_action'){
					googleplus_publish_action();
				}

	        } else {
	          googleplus_checkAuth(false);
	        }
	      }
	
	      // Load the API and make an API call.  Display the results on the screen.
	      function googleplus_return() {
			gapi.client.load('plus', 'v1', function(temps) {
				if(typeof(temps) !== 'undefined' && typeof(temps.error) !== 'undefined'){
					if(typeof(temps.error.errors[0].message) !== 'undefined'){
						alert('Error : '+temps.error.errors[0].message + '\nReason : '+temps.error.errors[0].reason);
					} else {
						alert('Error problem under connections');
					}
					return false;
				}
				
				if(typeof tmp_getdata == 'undefined' || tmp_getdata == ''){
					tmp_getdata = {'userId': 'me'};
				}
	
				var request = gapi.client.plus.people.get(tmp_getdata);
				request.execute(function(response) {
					var fn = window[uh_tmp_function];
					if(typeof fn === 'function') {
						fn(response,uh_tmp_extra,'googleplus');
						uh_tmp_data = '';
						uh_tmp_function = '';
						uh_tmp_extra = '';
						uh_tmp_direction = '';
						uh_tmp_alwayslogin = '';
					}
				});
			});
	      }
	      
	      
	    function googleplus_publish_action(){
		    gapi.post.go(uh_tmp_data);
			uh_tmp_data = '';
			uh_tmp_function = '';
			uh_tmp_extra = '';
			uh_tmp_direction = '';
			uh_tmp_alwayslogin = '';
	    }

		function googleplus_get(tmp_function,tmp_data,tmp_extra){	
			uh_tmp_data = tmp_data;
			uh_tmp_function = tmp_function;
			uh_tmp_extra = tmp_extra;
			uh_tmp_direction = 'googleplus_return';
			uh_tmp_alwayslogin = false;
		
			googleplus_init();
		}
		
		function googleplus_publish(tmp_function,tmp_data,tmp_extra){	
			uh_tmp_data = tmp_data;
			uh_tmp_function = tmp_function;
			uh_tmp_extra = tmp_extra;
			uh_tmp_direction = 'googleplus_publish_action';
			uh_tmp_alwayslogin = false;
			googleplus_init();
		}

		function googleplus_logout(){	
			gapi.auth.signOut();

		}	

		
		// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		// linkedin
		// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		
		function linkedin_logout() {
			IN.User.logout();
		}
		

		function linkedin_get(tmp_function,tmp_data,tmp_extra) {
			uh_tmp_data = tmp_data;
			uh_tmp_function = tmp_function;
			uh_tmp_extra = tmp_extra;
			IN.User.authorize(linkedin_connect_get_data);
		}


		function linkedin_publish(tmp_data,tmp_extra) {
			uh_tmp_data = tmp_data;
			uh_tmp_extra = tmp_extra;
			IN.User.authorize(linkedin_publish_action).place();
		}

		function linkedin_publish_action() {
			IN.UI.Share().params({
			  url: uh_tmp_data
			}).place()

			uh_tmp_data = '';
			uh_tmp_extra = '';
		}


		function linkedin_connect_get_data() {
			if(typeof uh_tmp_data == 'undefined' || uh_tmp_data == ''){
			  IN.API.Profile("me")
			    .fields(["id","firstName","lastName","pictureUrl","location", "publicProfileUrl"])
			    .result( function(me) {
					var fn = window[uh_tmp_function];
					if(typeof fn === 'function') {
						fn(me.values[0],uh_tmp_extra,'linkedin');
						uh_tmp_data = '';
						uh_tmp_function = '';
						uh_tmp_extra = '';
					}
			    });
			} else {
			  IN.API.Profile("me")
			  	.fields(uh_tmp_data)
			    .result( function(me) {
					var fn = window[uh_tmp_function];
					if(typeof fn === 'function') {
						fn(me.values[0],uh_tmp_extra,'linkedin');
						uh_tmp_data = '';
						uh_tmp_function = '';
						uh_tmp_extra = '';
					}
			    });
			}
		}
		
		// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		// other functions 
		// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		function serialize(obj) {
			var str = [];
			for(var p in obj){
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			}
			return str.join("&");
		}
	}
}