<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Dinah!' );
}

class RHC_RSVP_Backend extends tools_rsvp {
	function __construct() {
		if ( is_admin() ) {
			add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ) );
		}
	}

	function plugins_loaded() {
		global $wpdb;

		parent::__construct();

		require_once( RHCRSVP_PATH . 'inc/class-rhc-rsvp-assets.php' );
		require_once( RHCRSVP_PATH . 'inc/class-rhc-rsvp-metabox.php' );
		
		if ( isset( $_REQUEST['rsvp_export'] ) ) {
			$this->show_list();
		}

		if ( ! empty( $this->panel_settings['overwritealleventpost'] ) ) {
			$get_list_date = $wpdb->get_results( "SELECT ID FROM {$wpdb->prefix}posts WHERE post_type='events'" );

			if ( ! empty( $get_list_date ) ) {
				$tmpss = true;
				
				if ( 'no' == $this->panel_settings['overwritealleventpost'] ) {
					$tmpss = false;
				} else if ( 'yes' == $this->panel_settings['overwritealleventpost'] ) {
					$tmpss = true;
				}

				foreach ( $get_list_date as $tmp_run ) {
					update_post_meta( $tmp_run->ID, 'enable_rsvb_box', $tmpss );
				}
			}

			$this->panel_settings['overwritealleventpost'] = '';
			update_option( 'rsvp_options', $this->panel_settings );
		}
		
		if ( ! empty( $this->panel_settings['convertolddata'] ) ) {
			$wpdb->get_results( "UPDATE {$wpdb->prefix}rhc_rsvp SET answer = 'attending_event_yes_i_will_attend' WHERE answer = '1';" );
			$wpdb->get_results( "UPDATE {$wpdb->prefix}rhc_rsvp SET answer = 'maybe_not_sure_i_will_attend' WHERE answer = '0';" );
			$wpdb->get_results( "UPDATE {$wpdb->prefix}rhc_rsvp SET answer = 'not_attending_no_i_will_not_attend' WHERE answer = '-1';" );

			$this->panel_settings['convertolddata'] = false;
			update_option( 'rsvp_options', $this->panel_settings );
		}

		add_action( 'admin_menu', array( $this, 'register_my_custom_menu_page' ) );
		add_action( 'admin_menu', array( $this, 'my_remove_menu_pages' ) );
		add_action( 'admin_head',array( $this, 'admin_init' ) );
		add_action( 'save_post', array( $this, 'save_post' ) );
		add_action( 'admin_print_scripts-post-new.php', array( $this, 'admin_script' ) );
		add_action( 'admin_print_scripts-post.php', array( $this, 'admin_script' ) );
		add_action( 'quick_edit_custom_box',  array( $this, 'add_quick_edit' ), 999, 2 );
		add_filter( 'post_row_actions', array( $this, 'remove_row_actions' ), 10, 2 );
		add_filter( 'page_row_actions', array( $this, 'remove_row_actions' ), 10, 2 );
		add_action( 'admin_footer', array( $this, 'quick_edit_javascript' ) );
			
		if ( ! empty( $this->panel_settings['rsvp_show_dashbord_widget'] ) || ! isset( $this->panel_settings['rsvp_show_dashbord_widget'] ) ) {
			add_action( 'wp_dashboard_setup', array( $this, 'dashboard_widgets_setup' ) );
		}
		
		do_action( 'rh-php-commons' );
		
		$settings = array(
			'id'                     => 'rsvp',
			'plugin_id'              => 'rhc',
			'capability'             => 'rhc_options',
			'options_varname'        => 'rsvp_options',
			'menu_id'                => 'rsvp-options',
			'page_title'             => __( 'RSVP Events', 'rhcrsvp' ),
			'menu_text'              => __( 'RSVP Events', 'rhcrsvp' ),
			'option_menu_parent'     => 'edit.php?post_type=' . RHC_EVENTS,
			'notification'           => (object) array(
				'plugin_version' => RHCRSVP_VERSION,
				'plugin_code'    => 'RSVP',
				'message'        => __( 'Panel plugin update %s is available! <a href="%s">Please update now</a>', 'rhcrsvp' )
			),
			'theme'                  => false,
			'extracss'               => 'rhcrsvp-backend',
			'fileuploader'           => true,
			'stylesheet'             => 'rsvp-options',
			'option_show_in_metabox' => true,
			'pluginurl'              => RHCRSVP_URL,
			'pluginslug'             => RHCRSVP_SLUG,
			'api_url'                => 'http://plugins.righthere.com',
			'autoupdate'             => false,
			'path'                   => RHC_PATH . 'options-panel/',
			'url'                    => RHC_URL . 'options-panel/',
		);
		
		if ( class_exists( 'PluginOptionsPanelModule' ) ) {
			new PluginOptionsPanelModule( $settings );
		}
	}
	
	function show_list() {
		global $wpdb;
		
		parent::__construct();
	
		$return_data_content = '';

		if ( ! empty( $_REQUEST['rsvpid'] ) && is_numeric( $_REQUEST['rsvpid'] ) && isset( $_REQUEST['changeto'] ) && $_REQUEST['changeto'] ) {
			$wpdb->update(
				"{$wpdb->prefix}rhc_rsvp",
				array( 'answer' => $_REQUEST['changeto'] ),
				array( 'id' => $_REQUEST['rsvpid'] ),
				array( '%s' ),
				array( '%d' )
			);
		}
		
		$pagetype = 'post_type=events&';

		if ( ! current_user_can( 'manage_options' ) && current_user_can( 'rsvp_read_list' ) ) {
			$pagetype = '';
		}

		if ( ! empty( $_REQUEST['rsvpid'] ) && is_numeric( $_REQUEST['rsvpid'] ) && isset( $_REQUEST['delete'] ) && $_REQUEST['delete'] == 'true' ) {
			$wpdb->delete( "{$wpdb->prefix}rhc_rsvp", array( 'id' => $_REQUEST['rsvpid'] ), array( '%d' ) );
		}
		
		$post_id = absint( $_REQUEST['postID'] );

		if ( $post_id ) {
			$current_dato_minusweeks = date( 'Ymdhis', strtotime( '-4 week' ) );
			
			$get_date_type = $wpdb->get_results( $wpdb->prepare(
				"SELECT event_date FROM {$wpdb->prefix}rhc_rsvp WHERE postID = '%d' group by event_date order by event_date ",
				$post_id
			) );
			$show_arrows = false;
			$show_arrows_forward = false;	
			$tmp_content = '';
			$count = 0;

			if ( ! empty( $get_date_type ) ) {
				foreach($get_date_type as $tmp_key => $tmpsql ) {
					$tmp = explode( ',', $tmpsql->event_date );

					if ( ! $this->event_date_exists( $post_id, $tmp[0], $tmp[1] ) ) {
						continue;
					}

					$tmp_show = false;
					$active = false;
					
					if ( ! empty( $_REQUEST['event_rdate'] ) && $tmpsql->event_date == $_REQUEST['event_rdate'] ) {
						$active = true;
						$tmp_show = true;
					}
										
					if ( $tmp[0] < $current_dato_minusweeks && $tmp_show != true && count( $get_date_type ) > 5 ) {
						$tmp_show = false;
						$show_arrows = true;
					} else {
						$tmp_show = true;
						
						if ( empty( $_REQUEST['event_rdate'] ) ) {
							$active = true;
							$tmp_show = true;
							$_REQUEST['event_rdate'] = $tmpsql->event_date;
						}
					}
					
					if ( $tmp_show ) {
						$count++;
					}
					
					if ( $count > 5 && ! $active ) {
						$show_arrows_forward = true;
						$tmp_show = false;
					}

					$tmp_content .= '<a class="item button button-large ' . ( $active == true ? 'button-primary' : 'action' ) . '" style="margin-right:10px;margin-top:10px;' . ( $tmp_show == true ? '' : 'display:none;' ) . '" href="?' . $pagetype . 'page=rsvp-list&postID=' . $_REQUEST['postID'] . '&event_rdate=' . $tmpsql->event_date . '">' . date( 'F j, Y', strtotime( $tmp[0] ) ) .'</a>';
				}
			}
			
			$extra_text = '';

			if ( isset( $_REQUEST['attending_mail_list'] ) ) {
				$extra_text = ' : only attending ';
			}

			$tmp_array = array();
			$tmp_array_list = array();

			$return_data_content .= '<div class="wrap"><div style="margin-left:3px;" class="icon32" id="icon-options-general"><br></div><h2>' . __( 'RSVP list for event', 'rhcrsvp' ) . ' "' . get_the_title( $_REQUEST['postID'] ) . '"';
			
			$return_data_content .= sprintf( ' (%1$s : <a href="%2$s?p=%1$s%3$s">%4$s</a>)',
				$post_id,
				get_site_url(),
				( isset( $_REQUEST['event_rdate'] ) ) ? '&event_rdate=' . $_REQUEST['event_rdate'] : '',
				__( 'View page', 'rhcrsvp' )
			);
			
			$return_data_content .= $extra_text;
			$return_data_content .='</h2>';
			
			if ( ! empty( $_REQUEST['event_rdate'] ) ) {
				$total_joint = $wpdb->get_results( $wpdb->prepare(
					"
					SELECT answer, count(id) AS answer_count
					FROM {$wpdb->prefix}rhc_rsvp
					WHERE postID = '%d'
					GROUP BY answer
					ORDER BY answer
					",
					$post_id,
					$_REQUEST['event_rdate']
				) );
				
				$return_data_content .= '<div style="margin-left: 7px; margin-top: 10px;">';
				
				for ( $i = 0; $i < $this->get_layout_choose_fields_count( $post_id ); $i++ ) {
					$get_title = $this->get_layout_choose_fields_title( $i, $post_id );
					$get_subtitle = $this->get_layout_choose_fields_subtitle( $i, $post_id );
					$get_limit = $this->get_layout_choose_fields_limit( $i, $post_id );
					$get_color = $this->get_layout_choose_fields_color( $i, $post_id );
					$get_title_slug = $this->get_layout_choose_fields_slug( $i, $post_id );
					
					$tmp_array[ $get_title_slug ] = $get_title . ' (' . $get_subtitle . ')';
					
					if ( $this->get_layout_choose_fields_allow( $i, $post_id ) ) {
						array_push( $tmp_array_list, $get_title_slug );
					}
					
					$total_count = 0;
					
					foreach ( $total_joint as $tmp_data ) {
						if ( $get_title_slug == $tmp_data->answer ) {
							$total_count = $tmp_data->answer_count;
							break;
						}
					}
					
					$total_count = '<div class="total_count" style="color:' . $get_color[2] . ';background-color:' . $get_color[0] . ';">' . $total_count . '</div>';
					
					if ( ! empty( $get_limit ) ) {
						$total_count .= '<div class="limit_count" style="color:#ffffff;background-color:#000000;">' . $get_limit . '</div>';
					} else {
						$total_count .= '<div class="limit_count" style="color:#ffffff;background-color:#000000;">&infin;</div>';
					}
					
					$return_data_content .= '<div class="rsvp_count_object"><div class="rsvp_number_count">' . $total_count . '</div><div class="rsvp_title">' . $get_title . '<br><small>' . $get_subtitle . '</small></div></div>';
				}
				
				$return_data_content .= '</div>';
			}

			$return_data_content .= '<div style="clear:both;"></div>';
			
			$return_data_content .= '<div style="margin-top:10px;margin-left: 5px;" id="kalenderdatebuttom">';
			
			if ( $show_arrows ) {
				$return_data_content .= '<a class="button button-large action" href="#" style="margin-right:10px;margin-top:10px;" onclick="rsvp_show_back(this);return false;"><<</a>';
			}

			$return_data_content .= $tmp_content;
			
			if ( $show_arrows_forward ) {
				$return_data_content .= '<a class="button button-large action" href="#" style="margin-right:10px;margin-top:10px;" onclick="rsvp_show_forword(this);return false;">>></a>';
			}

			$return_data_content .= '</div>';
				
			if ( empty( $_REQUEST['event_rdate'] ) ) {
				$_REQUEST['event_rdate'] = 0;
			}
			
			$extra_qury = '';

			if ( isset( $_REQUEST['attending_mail_list'] ) ) {
				$tmp_check_string = '';

				for ( $i = 0; $i< $this->get_layout_choose_fields_count( $post_id ); $i++ ) {
					if ( $this->get_layout_choose_fields_allow( $i, $post_id ) ) {
						$get_title_slug = $this->get_layout_choose_fields_slug( $i, $post_id );

						if ( ! empty( $tmp_check_string ) ) {
							$tmp_check_string .= ',';
						}

						$tmp_check_string .= "'" . $get_title_slug . "'";
					}
				}
				
				if ( ! empty( $tmp_check_string ) ) {
					$extra_qury = ' and answer in (' . $tmp_check_string . ') ';
				}
			}

		 	if ( isset( $_REQUEST['rsvp_export'] ) ) {
		 		$this->panel_settings = get_option( 'rsvp_options' );
		 	
		 		$get_list_date = $wpdb->get_results("SELECT id,firstName,lastName,email,comment,answer,cDate,checkedin,multidata FROM {$wpdb->prefix}rhc_rsvp WHERE postID=".$_REQUEST['postID']."".$extra_qury." order by answer DESC,firstName ASC",ARRAY_N);
		 		$get_title = explode(',',$_REQUEST['event_rdate']);
		 		
		 		$fieldnames2 = array('ID','First Name','Last Name','Email','Comment','Answer','Reg Date','Checked In','Attending');
		 		
		 		foreach($get_list_date as $tmp_k => $tmp_d){

		 			

			 		if($tmp_d[7] == '0000-00-00 00:00:00'){
			 			$get_list_date[$tmp_k][7] = '';
			 		}
			 		
			 		if(!empty($tmp_d[8])){
				 		$multidata = unserialize($tmp_d[8]);

				 		foreach($multidata as $multi_tmp_key => $multi_tmp_data){
					 		array_push($get_list_date[$tmp_k],$multi_tmp_data);
					 		if($tmp_k == 0){
						 		array_push($fieldnames2,$multi_tmp_key);
					 		}
					 		
				 		}
			 		}
			 		
			 		$get_list_date[$tmp_k][8] = in_array($tmp_d[5], $tmp_array_list) == true ? 'True' : 'False';
			 		$tmp_d[5] = $tmp_array[$tmp_d[5]];
			 		foreach($get_list_date[$tmp_k] as $tmp_d_tmp_tmp_key => $tmp_d_tmp_tmp_data){
			 			$get_list_date[$tmp_k][$tmp_d_tmp_tmp_key] = $this->str_to_not_utf8($tmp_d_tmp_tmp_data);
			 		}
			 		
			 		
			 		
			 		
			 		
		 		}
		 		
		 		
		 		$delimiter = ';';
		 		if(!empty($this->panel_settings['rsvp_csv_symbole'])){
			 		$delimiter = ',';
		 		}

		 		$fieldnames1 = array(get_the_title($_REQUEST['postID']).' ('.date('F j Y',strtotime($get_title[0])).')');

		 			
		 		array_unshift($get_list_date, $fieldnames1,$fieldnames2);
		 		$this->array_to_csv_download($get_list_date, "rsvp event ".date('F j Y',strtotime($get_title[0])).".csv",$delimiter);
		 		die();
		 	
		 	} else {
		 		$get_list_date = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}rhc_rsvp WHERE postID=".$_REQUEST['postID']);
		 		
		 		
				$return_data_content .= '<div style="left: 5px; bottom: 0px; margin-right: 20px; overflow: auto; position: relative; margin-top: 15px;"><table class="wp-list-table widefat fixed posts" cellspacing="0"><thead><tr>';
				$return_data_content .= '<th class="manage-column column-author column-comments" style="padding-left: 10px;">'.__('Count','rhcrsvp').'</th>';
				$return_data_content .= '<th class="manage-column column-author column-comments" scope="col">'.__('Pic','rhcrsvp').'</th>';
				$return_data_content .= '<th class="manage-column column-title" scope="col">'.__('Name','rhcrsvp').'</th>';
				$return_data_content .= '<th class="manage-column column-title" scope="col">'.__('Email','rhcrsvp').'</th>';
				$return_data_content .= '<th class="manage-column column-title" scope="col">'.__('Login Type','rhcrsvp').'</th>';
				$return_data_content .= '<th class="manage-column column-title" scope="col">'.__('Attending','rhcrsvp').'</th>';
				$return_data_content .= '<th class="manage-column column-title" scope="col">'.__('Time','rhcrsvp').'</th>';
				$return_data_content .= '<th class="manage-column column-title" scope="col">'.__('Custom Fields','rhcrsvp').'</th>';
				$return_data_content .= '<th class="manage-column column-title" scope="col">'.__('Comment','rhcrsvp').'</th>';
				$return_data_content .= '<th class="manage-column column-author column-comments" scope="col">'.__('Tools','rhcrsvp').'</th>';
				$return_data_content .= '</tr></thead><tbody id="the-list">';
		
				if(!empty($get_list_date)){
					$tmp_skift = true;
				
					foreach($get_list_date as $tmp_key => $tmp_data){
						
						
						if(current_user_can('manage_options') || current_user_can('rsvp_change_status') ){
							$tmp_scrole = (in_array($tmp_data->answer, $tmp_array_list) == true ? 'Yes' : 'No'). ' : <select style="max-width: 200px;width: 100%;" onchange="rsvp_chnageattending(this,'.$_REQUEST['postID'].','.$tmp_data->id.')">';
							if(!empty($tmp_array)){
								foreach($tmp_array as $tmp_v2_key => $tmp_v2_data){
									$tmp_scrole .= '<option value="'.$tmp_v2_key.'" '.( $tmp_data->answer == $tmp_v2_key ? ' selected="selected" ':'').'>'.$tmp_v2_data.'</option>';	
								}			
							}
							$tmp_scrole .= '</select>'; 
						} else {
							$tmp_scrole = $tmp_array[$tmp_data->answer];
						}
									
									
									
										
						$Loginbywp = __('Manual','rhcrsvp');
						
						if(!empty($tmp_data->extern)){
							if($tmp_data->extern == 'FACEBOOK'){
								$Loginbywp = '<a target="_black" href="'.$tmp_data->externUrl.'">'.__('Facebook link','rhcrsvp').'</a>';
							} else if($tmp_data->extern == 'TWITTER'){
								$Loginbywp = '<a target="_black" href="'.$tmp_data->externUrl.'">'.__('Twitter link','rhcrsvp').'</a>';
							}
						}
						
						$get_avatar = get_avatar( $tmp_data->email, 40,RHCRSVP_URL.'images/default_avatar.jpg',$tmp_data->firstName.' '.$tmp_data->lastName);
								
						if(!empty($tmp_data->externUrlPhoto)){
							$get_avatar = '<img width="40" height="40" class="avatar avatar-40 photo" src="'.$tmp_data->externUrlPhoto.'" alt="'.$tmp_data->firstName.' '.$tmp_data->lastName.'">';
						}
						
						$return_data_content .= '<tr class="'.($tmp_skift == true ? 'alternate' : '').'">';		
						$return_data_content .= '<td class="comments column-comments" style="padding-left: 10px;">'.($tmp_key+1).'</td>';
						$return_data_content .= '<td class="comments column-comments"><div style="border-radius: 40px 40px 40px 40px; width: 40px; height: 40px; display: block; overflow: hidden;">'.$get_avatar.'</div></td>';
						$return_data_content .= '<td class="categories column-categories">'.$tmp_data->firstName.' '.$tmp_data->lastName.'</td>';
						$return_data_content .= '<td class="categories column-categories"><a href="mailto:'.$tmp_data->email.'">'.$tmp_data->email.'</a></td>';
						$return_data_content .= '<td class="categories column-categories">'.$Loginbywp.'</td>';
						$return_data_content .= '<td class="categories column-categories">'.$tmp_scrole.'</td>';
						$return_data_content .= '<td class="categories column-categories">'.date('F j, Y',strtotime($tmp_data->cDate)).'</td>';
						
						$return_data_content .= '<td class="categories column-categories">';
						
						
						if(!empty($tmp_data->multidata)){
					 		$multidata = unserialize($tmp_data->multidata);
	
					 		foreach($multidata as $multi_tmp_key => $multi_tmp_data){
					 			$return_data_content .= '<small><lable style="display: inline-block; min-width: 60px; text-transform: capitalize;">'.$multi_tmp_key.': </lable> <span style="display: inline-block; min-width: 60px; text-transform: capitalize;">'.$multi_tmp_data.'</span></small><br>';
					 		}
				 		}
						
						$return_data_content .= '</td>';
						
						
						$return_data_content .= '<td class="categories column-categories">'.$tmp_data->comment.'</td>';
						$return_data_content .= '<td class="comments column-comments">';
						if(current_user_can('manage_options') || current_user_can('rsvp_delete') ){
							// $return_data_content .= '<a href="#" onclick="rsvp_delete('.$_REQUEST['postID'].','.$tmp_data->id.');return false;">'.__('Delete','rhcrsvp').'</a>';
							$return_data_content .= '<a href="/wp-json/federmanager/v1/deletesubscription?id=' . $tmp_data->id . '&post_id=' . $_REQUEST['postID'] . '">'.__('Delete','rhcrsvp').'</a>';

						}
						
						$return_data_content .= '</td>';
						$return_data_content .= '</tr>';	
						
						$tmp_skift = !$tmp_skift;
					}
				}
				$return_data_content .= '</tbody></table></div></div>';
						 	
		 	}
			
			if(isset($_REQUEST['attending_mail_list'])){
				$return_data_content .= '<a class="item button button-large" style="margin-right:10px;margin-top:10px;" href="?'.$pagetype.'page=rsvp-list&postID='.$_REQUEST['postID'].'&event_rdate='.$_REQUEST['event_rdate'].'">'.__('Back','rhcrsvp').'</a>';
				$return_data_content .= '<a class="item button button-large" style="margin-right:10px;margin-top:10px;" href="?'.$pagetype.'page=rsvp-list&postID='.$_REQUEST['postID'].'&event_rdate='.$_REQUEST['event_rdate'].'&attending_mail_list=true&rsvp_export=csv">'.__('Download Checked-in list','rhcrsvp').'</a>';	
			} else {
				$return_data_content .= '<a class="item button button-large" style="margin-right:10px;margin-top:10px;" href="?'.$pagetype.'page=rsvp-list&postID='.$_REQUEST['postID'].'&event_rdate='.$_REQUEST['event_rdate'].'&attending_mail_list=true">'.__('Show only Attendees','rhcrsvp').'</a>';
				$return_data_content .= '<a class="item button button-large" style="margin-right:10px;margin-top:10px;" href="?'.$pagetype.'page=rsvp-list&postID='.$_REQUEST['postID'].'&event_rdate='.$_REQUEST['event_rdate'].'&rsvp_export=csv">'.__('Download CSV list','rhcrsvp').'</a>';	
				$return_data_content .= '<a class="item button button-large" style="margin-right:10px;margin-top:10px;" href="'.site_url().'/wp-json/federmanager/v1/getreport?id='.$_REQUEST['postID'].'&amp;fileName='.get_the_title($_REQUEST['postID']).'">'.__('Scarica la lista XLS semplificata').'</a>';	
			}	
		}
		
		?>
		<script>
			function rsvp_chnageattending(tmp_this,tmp_postID,tmp_id){
				if (window.confirm('<?php echo __('Are you sure you want to change status','rhcrsvp'); ?>')){
					window.location.href = '?<?php echo $pagetype; ?>page=rsvp-list&event_rdate=<?php echo $_REQUEST['event_rdate']; ?>&postID='+tmp_postID+'&changeto='+jQuery(tmp_this).val()+'&rsvpid='+tmp_id; 
				}
			}
			
			function rsvp_delete(tmp_postID,tmp_id){
				if (window.confirm('<?php echo __('Are you sure you want to remove this entry','rhcrsvp'); ?>')){
					window.location.href = '?<?php echo $pagetype; ?>page=rsvp-list&event_rdate=<?php echo $_REQUEST['event_rdate']; ?>&postID='+tmp_postID+'&delete=true&rsvpid='+tmp_id; 
				}
			}	
			
			function rsvp_show_forword(tmp_this){
				var tmp = jQuery('#kalenderdatebuttom a.item').filter(':visible:first').index();
				var tmplength = jQuery('#kalenderdatebuttom a').length;
				for(var i=tmp; i<tmplength;i++){
					jQuery('#kalenderdatebuttom a:eq('+i+')').show();
				}
				jQuery(tmp_this).hide();
			}
			
			function rsvp_show_back(tmp_this){
			
				var tmp = jQuery('#kalenderdatebuttom a.item').filter(':visible:first').index();
				
				for(var i=0; i<tmp;i++){
					jQuery('#kalenderdatebuttom a:eq('+i+')').show();
				}
				jQuery(tmp_this).hide();
			}	
		</script>
		<?php
		
		
		echo $return_data_content;
	}
	

	function remove_row_actions( $actions,$post )
	{
	    if( get_post_type() === 'events' ){
		    $tmp = get_post_meta($post->ID,'enable_rsvb_box',true);
		
			if(empty($tmp)){
				$tmp = false;
			}
			if($tmp){
				$actions['eventpage'] = '<input type="hidden" value="'.$tmp.'" class="rhc_rsvp_box_data">';
				$actions['eventpage'] .= '<a href="edit.php?post_type=events&page=rsvp-list&postID='.$post->ID.'">'.__('RSVP List','rhcrsvp').'</a>';
			}
	        
		}
	    return $actions;
	}

	function admin_script() {
	    if($this->check_post_type()){
	    	wp_enqueue_script( 'rsvp_admin', RHCRSVP_URL . 'js/rsvp_admin.js' );  
		}
	}		
			
	function admin_init(){
		echo "<link rel='stylesheet' type='text/css' href='".RHCRSVP_URL."css/dashbordwidget.css'>";
	
	    if($this->check_post_type()){
			global $post;
			$get_value = 0;
			
			if(!empty($post) && isset($_REQUEST['post']) && isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit'){
				$get_value = get_post_meta($post->ID,'enable_rsvb_box',true);
			} else if(!empty($post)){
				
				if(!empty($this->panel_settings['rsvp_newpost_on'])){
					$get_value = true;
				} 
			}
			
			
			echo '<script>if(typeof cit_rsvp_createbutton == \'function\'){cit_rsvp_createbutton(\''.__('RSVP Box','rhcrsvp').'\',\''.$get_value.'\');}</script>';	
		}

	}
	
	function save_post(){
		global $post;
		if($this->check_post_type() and !empty($post->ID) and isset( $_POST['action'] ) and $_POST['action'] != 'inline-save'){
			$value = isset($_REQUEST['enable_rsvb_box'])?$_REQUEST['enable_rsvb_box']:'';
			update_post_meta($post->ID,'enable_rsvb_box',$value);
		}
		
		
		if($this->check_post_type() and !empty($_POST['post_ID']) and isset( $_POST['action'] ) and $_POST['action'] == 'inline-save'){
			$value = isset($_POST['rhc_rsvp_box'])?$_POST['rhc_rsvp_box']:'';
			update_post_meta($_POST['post_ID'],'enable_rsvb_box',$value);
		}
	}
	
function dashboard_widget_new_feauter() {
		global $wpdb;
		
		parent::__construct();
		
		$tmp_array = array(1 => __('1 Month', 'rhcrsvp' ), 3 => __('3 Months', 'rhcrsvp' ), 6 => __('6 Months', 'rhcrsvp' ),12 => __('12 Months', 'rhcrsvp' ),'all' => __('Show All', 'rhcrsvp' ));
		$tmp_order = array('date' => __('Date', 'rhcrsvp' ),'title' => __('Title', 'rhcrsvp' ),'attending' => __('Attending', 'rhcrsvp' ));
		
		$tmp_get = array();
		if(!empty($_GET)){
			$tmp_get_org = $_GET;
		}
		
		if(!empty($_GET['rewrittenadmin'])){
			unset($tmp_get_org['rewrittenadmin']);
		}
		
		
		
		if(empty($tmp_get_org['rsvp_dir'])){
			$tmp_get_org['rsvp_dir'] = 'DESC';			
		}
		
		if(empty($tmp_get_org['rsvp_sort'])){
			$tmp_get_org['rsvp_sort'] = 'date';			
		}
		
		if(empty($tmp_get_org['rsvp_show_month'])){
			$tmp_get_org['rsvp_show_month'] = '1';			
		}
		
		
		echo '<div class="rsvp_dashbord_small">';
			$count = 0;
			foreach($tmp_order as $k_tmp => $d_tmp){
				$tmp_get_copy = $tmp_get_org;
				$tmp_get_copy['rsvp_sort'] = $k_tmp;
				$tmp_get_copy['rsvp_page'] = 1;
				if($count > 0){
					echo ' | ';
				}
				
				if($tmp_get_org['rsvp_sort'] == $k_tmp){
					if($tmp_get_org['rsvp_dir'] == 'DESC'){
						$tmp_get_copy['rsvp_dir'] = 'ASC';
					} else {
						$tmp_get_copy['rsvp_dir'] = 'DESC';	
					}	
					echo '<b>';
				} else {
					$tmp_get_copy['rsvp_dir'] = 'DESC';
				}
				echo '<a href="?'.http_build_query($tmp_get_copy).'">'.$d_tmp.'</a>';
				
				if($tmp_get_org['rsvp_sort'] == $k_tmp){
					echo '</b>';
				}
				$count++;
			}
		echo '</div>';
		echo '<div class="rsvp_dashbord_small">';
			$count = 0;
			foreach($tmp_array as $k_tmp => $d_tmp){
				$tmp_get_copy = $tmp_get_org;
				$tmp_get_copy['rsvp_show_month'] = $k_tmp;
				$tmp_get_copy['rsvp_page'] = 1;
				
				if($count > 0){
					echo ' | ';
				}
				
				if($tmp_get_org['rsvp_show_month'] == $k_tmp){
					echo '<b>'.$d_tmp.'</b>';
				} else {
					echo '<a href="?'.http_build_query($tmp_get_copy).'">'.$d_tmp.'</a>';
				}
				$count++;
			}
		echo '</div>';
		
		$get_month_forward = '';
		$get_month_back = strtotime("-1 months");
		$get_month_back = "rsvp1.event_date > '".date('Ymdhis',$get_month_back)."'";
		if($tmp_get_org['rsvp_show_month'] != 'all'){
			$get_month_forward = strtotime("+".$tmp_get_org['rsvp_show_month']." months");
			$get_month_forward = "and rsvp1.event_date <= '".date('Ymdhis',$get_month_forward)."'";
		}
		
		
		$tmp_check_string = '';
		for($i=0;$i< $this->get_layout_choose_fields_count(0);$i++){
			if($this->get_layout_choose_fields_allow($i,0)){
				$get_title_slug = $this->get_layout_choose_fields_slug($i,0);

				if(!empty($tmp_check_string)){
					$tmp_check_string .= ',';
				}
				$tmp_check_string .= "'".$get_title_slug."'";
			}
		}
		

		
		
		$get_list_date = $wpdb->get_results("SELECT  sum(IF(rsvp1.answer in (".$tmp_check_string."), 1, 0)) AS attending,sum(IF(rsvp1.answer not in (".$tmp_check_string.") , 1, 0)) AS notattending, rsvp1.postID,rsvp1.event_date as date, (SELECT post_title FROM {$wpdb->prefix}posts as p1 WHERE p1.ID = rsvp1.postID limit 1) as title, rsvp1.postID,rsvp1.event_date FROM {$wpdb->prefix}rhc_rsvp as rsvp1 WHERE ".$get_month_back." ".$get_month_forward." group by rsvp1.postID,rsvp1.event_date order by ".$tmp_get_org['rsvp_sort']." ".$tmp_get_org['rsvp_dir']." ,rsvp1.postID ");

		echo '<div class="rsvp_dashbord_content"><table cellpadding="0" >';
			if ( ! empty( $get_list_date ) ) {
				foreach ( $get_list_date as $k_tmp => $d_tmp ) {
					$time_devider = explode( ',', $d_tmp->event_date );
				
					if ( ! $this->event_date_exists( $d_tmp->postID, $time_devider[0], $time_devider[1] ) ) {
						continue;
					}

					echo '<tr><td>';
						echo '<div class="rsvp_title_main"><div class="rsvp_title_sub"><a target="_blank" href="' . get_permalink( $d_tmp->postID ) . '?event_rdate=' . $d_tmp->event_date . '">' . $d_tmp->title . '</a></div></div>';
					echo '</td><td>';
						echo '<span class="rsvp_date">' . date( 'M d, Y',strtotime( $time_devider[0] ) ) . '</span>';
					echo '</td><td>';
						echo '<div class="rsvp_attending_box">
								<div class="rsvp_attending_small">' . $d_tmp->attending . '</div>
								<div class="rsvp_attending_sub"><div class="rsvp_attending_sub2"><div class="rsvp_notattending_small">' . $d_tmp->notattending . '</div></div></div>
							   </div>';
					echo '</td><td>';
						echo '<a href="edit.php?post_type=events&page=rsvp-list&postID=' . $d_tmp->postID . '&event_rdate=' . $d_tmp->event_date . '&attending_mail_list=true&rsvp_export=csv"><div class="rsvp_link_to_list rsvplist-uniE130"></div></a>';
					echo '</td><td>';
						echo '<a href="edit.php?post_type=events&page=rsvp-list&postID=' . $d_tmp->postID . '&event_rdate=' . $d_tmp->event_date . '"><div class="rsvp_email_liste rsvplist-uniE15D"></div></a>';
					echo '</td></tr>';
				}
			}
		echo '</table></div>';
	}
	
	function dashboard_widgets_setup() {
		if(current_user_can('manage_options') || current_user_can('rsvp_read_list') ){
			wp_add_dashboard_widget( 'dashboard_widget_new_feauter', __( 'RSVP Attendee Status', 'rhcrsvp' ), array( &$this, 'dashboard_widget_new_feauter') );
		}
	}

	function add_quick_edit($column_name, $post_type) {
		if(!$this->check_post_type()) return false;
		
		if($column_name != 'fc_start') return false;
	    ?>
	    <fieldset class="inline-edit-col-right">
			<div class="inline-edit-group">
				<label class="alignleft">
						<input type="checkbox" value="true" id="rhc_rsvp_box" class="rhc_rsvp_box" name="rhc_rsvp_box">
						<span class="checkbox-title"><?php echo __('R.S.V.P Box','rhcrsvp'); ?></span>
				</label>
			</div>
		</fieldset>
	    <?php
	}
	
	function quick_edit_javascript() {
	    ?>
	    <script type="text/javascript">
	    
	    jQuery( document ).on( "mouseout", "a.editinline", rhc_rsvp_inline_widget_set ); // jQuery 1.7+
	    function rhc_rsvp_inline_widget_set() {
	    	tmp = jQuery(this).parent().parent().parent().parent().attr('id');
	    	tmp=tmp.replace("post-","");
			setTimeout(function() {
	    	if(jQuery('.post-'+tmp+' .rhc_rsvp_box_data').val()!= 'undefined' && (jQuery('.post-'+tmp+' .rhc_rsvp_box_data').val()== 'true' || jQuery('.post-'+tmp+' .rhc_rsvp_box_data').val()== true || jQuery('.post-'+tmp+' .rhc_rsvp_box_data').val() == 1)){
		    	jQuery('#edit-'+tmp+' .rhc_rsvp_box').attr('checked', true);
	    	} else {
		    	jQuery('#edit-'+tmp+' .rhc_rsvp_box').removeAttr('checked');
	    	}
	    	},120);
	    }
	    </script>
	    <?php
	}

	function register_my_custom_menu_page(){
		if(current_user_can('manage_options') ){
			add_submenu_page(('edit.php?post_type=events'),__('RSVP List','rhcrsvp'), __('RSVP List','rhcrsvp'), 'manage_options',('rsvp-list'), array(&$this,'show_list') );  
		} else {
			add_submenu_page(('edit.php?post_type=events'),__('RSVP List','rhcrsvp'), __('RSVP List','rhcrsvp'), 'rsvp_read_list',('rsvp-list'), array(&$this,'show_list') );  
		}
	}

	function my_remove_menu_pages() {
	    global $submenu;
	    
		$pagetype = '?post_type=events';
		if(!current_user_can('manage_options') && current_user_can('rsvp_read_list') ){
			$pagetype = '';
		} 	    
	    
	    if(!empty($submenu['edit.php'.$pagetype])){
		    foreach($submenu['edit.php'.$pagetype] as $tmp_key => $tmp_tag){
			    if($tmp_tag[0] == 'RSVP List'){
				    unset($submenu['edit.php'.$pagetype][$tmp_key]);
				    break;
			    }
		    }
	    }
	}

	function event_date_exists( $event_id, $start_date, $end_date ) {
		global $wpdb;

		return $wpdb->get_col( $wpdb->prepare( "SELECT post_id FROM {$wpdb->prefix}rhc_events WHERE post_id = '%d' AND event_start = '%s' OR event_end = '%s'",
			$event_id,
			date( 'Y-m-d H:i:s', strtotime( $start_date ) ),
			date( 'Y-m-d H:i:s', strtotime( $end_date ) )
		) );
	}
}

return new RHC_RSVP_Backend();