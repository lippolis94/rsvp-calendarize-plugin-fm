<?php 

/**
 * @author Angelo L.
 * Classe per fare un'esportazione solo dei custom fields dell'RSVP in un file xls
 * @param int $post_id
 * @param array $columns
 */

class RSVP_Delete_Subscription_FM
{
    public function __construct()
    {
        add_action( 'rest_api_init', [ 'RSVP_Delete_Subscription_FM', 'add_custom_routes' ] );
    }

    /**
     * Metodo che elimina dal DB una partecipazione dato l'id, da usare con la funzione getResponseID
     * @param int $rsvp_id
     * @return mixed | int if true, false 
     */
    protected static function deletePartecipation( $rsvp_id )
    {
        global $wpdb;
        return $wpdb->delete( "{$wpdb->prefix}rhc_rsvp", array( 'id' => $rsvp_id ));
    }

    /**
	  * Endpoint per cancellare una data partecipazione
	  * @param int | $rsvp_id
	  */
    public function handle_delete_member_subscription( WP_REST_Request $req )
    {
        $rsvp_id = $req['id'];
        $post_id = $req['post_id'];

        //Configuro risposta di errore 
        $permalink = site_url('/wp-admin/edit.php?post_type=events&page=rsvp-list&postID=' . $post_id);
        $success = new WP_REST_Response();
        $success->set_status('302');
        $success->header('Location', $permalink);

        if(self::deletePartecipation($rsvp_id)) :
            return $success;
        endif;

        wp_send_json_error();
    }

    public function add_custom_routes()
    {
        register_rest_route( 'federmanager/v1', '/deletesubscription', array(
			'methods' => 'GET',
			'callback' => ['RSVP_Delete_Subscription_FM', 'handle_delete_member_subscription']
		));
    }
}

new RSVP_Delete_Subscription_FM();