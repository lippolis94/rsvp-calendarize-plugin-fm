<?php

class tools_rsvp {
	public $tmp_pack;
	public $dep_level;

	public $get_meta_data = array(
		'event_start_date' => array(
			'meta_key'   => 'fc_start',
			'replacetag' => '[STARTDATE]'
		),
		'event_start_time' => array(
			'meta_key'   => 'fc_start_time',
			'replacetag' => '[STARTTIME]'
		),
		'event_end_date' => array(
			'meta_key'   => 'fc_end',
			'replacetag' => '[ENDDATE]'
		),
		'event_end_time' => array(
			'meta_key' => 'fc_end_time',
			'replacetag'=>'[ENDTIME]'
		)
	);
										 
	public $get_organizer_data = array(
		'organizer_name' => array(
			'meta_key'   => 'name',
			'replacetag' => '[ORGANIZER_NAME]'
		),
		'organizer_email' => array(
			'meta_key'   => 'email',
			'replacetag' => '[ORGANIZER_EMAIL]'
		),
		'organizer_phone' => array(
			'meta_key'   => 'phone',
			'replacetag' => '[ORGANIZER_PHONE]'
		),
		'organizer_website' => array(
			'meta_key'   => 'website',
			'replacetag' =>'[ORGANIZER_WEBSITE]'
		),
		'organizer_content' => array(
			'meta_key'   => 'content',
			'replacetag' => '[ORGANIZER_CONTENT]'
		)
	);

	public $get_venue_data = array(
		'venue_name' => array(
			'meta_key'   => 'name',
			'replacetag' => '[VENUE_NAME]'
		),
		'venue_content' => array(
			'meta_key'   => 'content',
			'replacetag' => '[VENUE_CONTENT]'
		),
		'venue_address' => array(
			'meta_key'   => 'address',
			'replacetag' => '[VENUE_ADDRESS]'
		),
		'venue_email' => array(
			'meta_key'   => 'email',
			'replacetag' => '[VENUE_EMAIL]'
		),
		'venue_city'  => array(
			'meta_key'   => 'city',
			'replacetag' => '[VENUE_CITY]'
		),
		'venue_zip' => array(
			'meta_key'   => 'zip',
			'replacetag' => '[VENUE_ZIP]'
		),
		'venue_state' => array(
			'meta_key'   => 'state',
			'replacetag' => '[VENUE_STATE]'
		),
		'venue_country' => array(
			'meta_key'   => 'country',
			'replacetag' => '[VENUE_COUNTRY]'
		),
		'venue_phone' => array(
			'meta_key'   => 'phone',
			'replacetag' => '[VENUE_PHONE]'
		),
		'venue_website' => array(
			'meta_key'   => 'website',
			'replacetag' => '[VENUE_WEBSITE]'
		),
		'venue_gaddress' => array(
			'meta_key'   => 'gaddress',
			'replacetag' => '[VENUE_GADDRESS]'
		)
	);

	function __construct() {
		if ( ! empty( $this->panel_settings ) ) {
			return $this->panel_settings;
		}

		$receive_email = sprintf(
			"<h1>%s</h1>\n" .
			"<p>%s</p>\n\n" .
			"<h2>%s</h2>\n" .
			"<p>[TITLE]<br>\n" .
			"<strong>%s</strong>: [LINK]<br>\n" .
			"<strong>%s</strong>: [ICAL_FEED]</p>\n\n" .
			"<h2>%s</h2>\n" .
			"<p><strong>%s</strong>: [STARTDATE]<br>\n" .
			"<strong>%s</strong>: [STARTTIME]</p>\n" .
			"<p><strong>%s</strong>: [ENDDATE]<br>\n" .
			"<strong>%s</strong>: [ENDTIME]</p>\n\n" .
			"<h2>%s</h2>\n" .
			"<p>[VENUE_NAME]<br>\n" .
			"[VENUE_ADDRESS]<br>\n" .
			"[VENUE_CITY], [VENUE_ZIP] [VENUE_STATE]<br>\n" .
			"[VENUE_COUNTRY] </p>\n\n" .
			"<h2>%s</h2>\n" .
			"<p><strong>%s</strong>: [ORGANIZER_NAME]<br>\n" .
			"<strong>%s</strong>:  [ORGANIZER_EMAIL]<br>\n" .
			"<strong>%s</strong>: [ORGANIZER_PHONE] </p>\n\n" .
			"<p>%s</p>\n" .
			"<p>%s</p>\n",
			__( 'Thank you for your RSVP!', 'rhcrsvp' ),
			__( 'We want to let you know that you have RSVP\'d to the following event.', 'rhcrsvp' ),
			__( 'WHAT?', 'rhcrsvp' ),
			__( 'Click for details', 'rhcrsvp' ),
			__( 'Subscribe', 'rhcrsvp' ),
			__( 'WHEN?', 'rhcrsvp' ),
			__( 'Start Date', 'rhcrsvp' ),
			__( 'Start Time', 'rhcrsvp' ),
			__( 'End Date', 'rhcrsvp' ),
			__( 'End Time', 'rhcrsvp' ),
			__( 'WHERE?', 'rhcrsvp' ),
			__( 'WHO?', 'rhcrsvp' ),
			__( 'Organizer', 'rhcrsvp' ),
			__( 'Email', 'rhcrsvp' ),
			__( 'Phone', 'rhcrsvp' ),
			__( 'Thank you!', 'rhcrsvp' ),
			__( 'The Calendarize it! Team', 'rhcrsvp' )
		);

		$admin_receive_email = sprintf(
			"<p>%s</p>\n\n" .
			"<p><strong>%s:</strong> [USER_FIRSTNAME] [USER_LASTNAME]<br>\n" .
			"<strong>%s:</strong> [USER_EMAIL]<br>\n" .
			"<strong>%s:</strong> [ANSWER]</p>\n\n" .
			"<p><strong>%s:</strong> [TITLE]<br>\n" .
			"<strong>%s:</strong> [STARTDATE]<br>\n" .
			"<strong>%s:</strong> [STARTTIME]</p>\n\n" .
			"<p><strong>%s:</strong> [LIMIT]<br>\n" .
			"<strong>%s:</strong> [CURRENT_ATTENDING]</p>",
			__( 'The following user has RSVP\'d to an event where you are the Organizer.', 'rhcrsvp' ),
			__( 'Name', 'rhcrsvp' ),
			__( 'Email', 'rhcrsvp' ),
			__( 'Status', 'rhcrsvp' ),
			__( 'Event', 'rhcrsvp' ),
			__( 'Event Date', 'rhcrsvp' ),
			__( 'Event Time', 'rhcrsvp' ),
			__( 'Maximum for Event', 'rhcrsvp' ),
			__( 'Currently attending', 'rhcrsvp' )
		);

		$organizer_email = sprintf(
			"<p>%s</p>\n\n" .
			"<p><strong>%s:</strong> [USER_FIRSTNAME] [USER_LASTNAME]<br>\n" .
			"<strong>%s:</strong> [USER_EMAIL]<br>\n" .
			"<strong>%s:</strong> [ANSWER]</p>\n\n" .
			"<p><strong>%s:</strong> [TITLE]<br>\n" .
			"<strong>%s:</strong> [STARTDATE]<br>\n" .
			"<strong>%s:</strong> [STARTTIME]</p>\n\n" .
			"<p><strong>%s:</strong> [LIMIT]<br>\n" .
			"<strong>%s:</strong> [CURRENT_ATTENDING]</p>",
			__( 'The following user has RSVP\'d to an event where you are the Organizer.', 'rhcrsvp' ),
			__( 'Name', 'rhcrsvp' ),
			__( 'Email', 'rhcrsvp' ),
			__( 'Status', 'rhcrsvp' ),
			__( 'Event', 'rhcrsvp' ),
			__( 'Event Date', 'rhcrsvp' ),
			__( 'Event Time', 'rhcrsvp' ),
			__( 'Maximum for Event', 'rhcrsvp' ),
			__( 'Currently attending', 'rhcrsvp' )
		);

		$default = array(
			'text_title'                       => __( 'RSVP', 'rhcrsvp' ),
			'text_list_title'                  => __( 'Check-In Attendees', 'rhcrsvp' ),
			'text_info_title'                  => __( 'Please enter your information',  'rhcrsvp' ),
			'text_social_facebook'             => __( 'Connect with Facebook', 'rhcrsvp' ),
			'text_social_googleplus'           => __( 'Connect with Google+', 'rhcrsvp' ),
			'text_social_linkedin'             => __( 'Connect with LinkedIn', 'rhcrsvp' ),
			'text_social_twitter'              => __( 'Connect with Twitter', 'rhcrsvp' ),
			'text_social_wp'                   => __( 'Connect with Wordpress', 'rhcrsvp' ),
			'text_email_receive'               => __( 'Receive notification by email about the event.', 'rhcrsvp' ),
			'text_button_manual_join'          => __( 'Join and RSVP', 'rhcrsvp' ),
			'text_sold_out'                    => __( '(Sold out)', 'rhcrsvp' ),
			'element_show_input_comment'       => true,
			'element_show_text_title'          => true,
			'element_show_text_list_title'     => true,
			'element_show_text_info_title'     => true,
			'element_show_text_sold_out'       => true,
			'element_show_text_email_receive'  => true,
			'rsvp_facebook_show'               => true,
			'rsvp_googleplus_show'             => true,
			'rsvp_linkedin_show'               => true,
			'rsvp_twitter_show'                => true,
			'rsvp_wp_show'                     => true,
			'rsvp_manualjoin_show'             => true,
			'convertolddata'                   => false,
			'layout_choose_fields'             => '3',
			'layout_user_input_fields'         => '3',
			'layout_choose_text_0'             => __( 'Attending Event', 'rhcrsvp' ),
			'layout_choose_text_1'             => __( 'Maybe', 'rhcrsvp' ),
			'layout_choose_text_2'             => __( 'Not Attending', 'rhcrsvp' ),
			'layout_choose_subtext_0'          => __( 'Yes, I will attend', 'rhcrsvp' ),
			'layout_choose_subtext_1'          => __( 'Not sure I will attend', 'rhcrsvp' ),
			'layout_choose_subtext_2'          => __( 'No, I will not attend', 'rhcrsvp' ),
			'layout_choose_slug_0'             => 'attending_event_yes_i_will_attend',
			'layout_choose_slug_1'             => 'maybe_not_sure_i_will_attend',
			'layout_choose_slug_2'             => 'not_attending_no_i_will_not_attend',
			'layout_choose_limit_0'            => '0',
			'layout_choose_limit_1'            => '0',
			'layout_choose_limit_2'            => '0',
			'layout_normal_color_0'            => '#1abc9c',
			'layout_normal_color_1'            => '#f1c40f',
			'layout_normal_color_2'            => '#e74c3c',
			'layout_hover_color_0'             => '#16a085',
			'layout_hover_color_1'             => '#f39c12',
			'layout_hover_color_2'             => '#c0392b',
			'layout_text_color_0'              => '#ffffff',
			'layout_text_color_1'              => '#ffffff',
			'layout_text_color_2'              => '#ffffff',
			'layout_choose_allow_0'            => true,
			'layout_choose_allow_1'            => false,
			'layout_choose_allow_2'            => false,
			'layout_count_color_body'          => '#ffffff',
			'layout_count_color_boble'         => '#3498db',
			'layout_count_color_text'          => '#ffffff',
			'layout_user_input_main_name'      => '0',
			'layout_user_input_main_email'     => '1',
			'layout_user_input_label_comment'  => __( 'Add comment...', 'rhcrsvp' ),
			'layout_user_input_label_0'        => __( 'Name', 'rhcrsvp' ),
			'layout_user_input_label_1'        => __( 'Email', 'rhcrsvp' ),
			'layout_user_input_label_2'        => __( 'Phone', 'rhcrsvp' ),
			'layout_user_input_slug_0'         => 'name',
			'layout_user_input_slug_1'         => 'email',
			'layout_user_input_slug_2'         => 'phone',
			'layout_user_input_type_0'         => 'text',
			'layout_user_input_type_1'         => 'email',
			'layout_user_input_type_2'         => 'tlf',
			'layout_user_input_check_0'        => true,
			'layout_user_input_check_1'        => false,
			'layout_user_input_check_2'        => false,
			'field_for_the_email'              => '1',
			'field_for_the_name'               => '0',
			'rsvp_attendee_email'              => '1',
			'rsvp_attend_show'                 => '0',
			'rsvp_attend_show_name'            => '0',
			'rsvp_receive_emailfrom'           => '',
			'rsvp_receive_emailsub'            => sprintf( __( 'RSVP for %s', 'rhcrsvp' ), '[TITLE]' ),
			'rsvp_receive_email'               => $receive_email,
			'rsvp_admin_email'                 => '1',
			'rsvp_admin_receive_emailfrom'     => '',
			'rsvp_admin_receive_emailsub'      => sprintf( __( 'RSVP: %1$s to %2$s', 'rhcrsvp' ), '[ATTENDEE_NAME]', '[TITLE]' ),
			'rsvp_admin_receive_subchange'     => sprintf( __( 'RSVP: %1$s change answer on "%2$s"', 'rhcrsvp' ), '[ATTENDEE_NAME]', '[TITLE]' ), 
			'rsvp_admin_receive_email'         => $admin_receive_email,
			'rsvp_organizer_email'             => '0',
			'rsvp_organizer_receive_emailsub'  => sprintf( __( 'RSVP: %1$s to %2$s', 'rhcrsvp' ), '[ATTENDEE_NAME]', '[TITLE]' ),
			'rsvp_organizer_receive_subchange' => sprintf( __( 'RSVP: %1$s change answer on "%2$s"', 'rhcrsvp' ), '[ATTENDEE_NAME]', '[TITLE]' ), 
			'rsvp_organizer_receive_emailfrom' => '',
			'rsvp_organizer_receive_email'     => $organizer_email
		);

		$this->panel_settings = get_option( 'rsvp_options' );

		foreach ( $default as $tmp_key => $tmp_data ) {
			if ( ! isset( $this->panel_settings[ $tmp_key ] ) ) {
				$this->panel_settings[ $tmp_key ] = $tmp_data;
			}
		}

		return $this->panel_settings;
	}

	function setup_post_fields( $post_id ) {
		if ( ! $post_id ) {
			return;
		}

		$post_fields = $this->get_fields( $post_id );

		if ( is_array( $post_fields ) && $post_fields ) {
			$this->panel_settings['layout_choose_fields'] = count( $post_fields );
			$i = 0;

			foreach ( $post_fields as $field ) {
				foreach ( $field as $field_key => $field_value ) {
					$this->panel_settings["layout_{$field_key}_{$i}"] = $field_value;
				}

				$i++;
			}
		}
	}

	function get_fields( $post_id, $reset_indexes = false ) {
		$fields = get_post_meta( $post_id, '_rhc_rsvp_fields', true );
		
		if ( $reset_indexes && is_array( $fields ) && $fields ) {
			$fields = array_values( $fields );
		}

		return $fields;
	}

	function update_post_fields( $post_id, $fields ) {
		return update_post_meta( $post_id, '_rhc_rsvp_fields', $fields );
	}

	function get_layout_choose_fields_count() {
		$output = 3;

		if ( ! empty( $this->panel_settings['layout_choose_fields'] ) ) {
			$output = $this->panel_settings['layout_choose_fields'];
		}

		return $output;
	}
	
	function get_layout_choose_fields_title( $tmp_field_number = 0 ) {
		$output = '';
		
		if ( ! empty( $this->panel_settings["layout_choose_text_{$tmp_field_number}"] ) ) {
			$output = $this->panel_settings["layout_choose_text_{$tmp_field_number}"];
		}

		return $output;
	}

	function get_layout_choose_fields_subtitle( $tmp_field_number = 0 ) {
		$output = '';

		if ( ! empty($this->panel_settings["layout_choose_subtext_{$tmp_field_number}"] ) ) {
			$output = $this->panel_settings["layout_choose_subtext_{$tmp_field_number}"];
		}

		return $output;
	}
	
	function get_layout_choose_fields_slug( $tmp_field_number = 0 ) {
		$output = '';

		if ( ! empty( $this->panel_settings["layout_choose_slug_{$tmp_field_number}"] ) ) {
			$output = $this->panel_settings["layout_choose_slug_{$tmp_field_number}"];
		}
		
		return $output;
	}
	
	function get_layout_choose_fields_allow( $tmp_field_number = 0 ) {
		$output = false;

		if ( ! empty( $this->panel_settings["layout_choose_allow_{$tmp_field_number}"] ) ) {
			$output = $this->panel_settings["layout_choose_allow_{$tmp_field_number}"];
		}

		return $output;
	}

	function get_layout_choose_fields_color( $tmp_field_number = 0 ) {
		$output = array( '#ffffff', '#ffffff', '#000000' );

		if ( ! empty( $this->panel_settings["layout_normal_color_{$tmp_field_number}"] ) ) {
			$output[0] = $this->panel_settings["layout_normal_color_{$tmp_field_number}"];
		}

		if ( ! empty( $this->panel_settings["layout_hover_color_{$tmp_field_number}"] ) ) {
			$output[1] = $this->panel_settings["layout_hover_color_{$tmp_field_number}"];
		}

		if ( ! empty( $this->panel_settings["layout_text_color_{$tmp_field_number}"] ) ) {
			$output[2] = $this->panel_settings["layout_text_color_{$tmp_field_number}"];
		}
		
		return $output;
	}
	
	function get_layout_choose_fields_count_color( $tmp_field_number = 0 ) {		
		$return_array = array( '#ffffff', '#000000', '#ffffff' );

		if ( ! empty( $this->panel_settings['layout_count_color_body'] ) ) {
			$return_array[0] = $this->panel_settings['layout_count_color_body'];
		}

		if ( ! empty( $this->panel_settings['layout_count_color_boble'] ) ) {
			$return_array[1] = $this->panel_settings['layout_count_color_boble'];
		}

		if ( ! empty( $this->panel_settings['layout_count_color_text'] ) ) {
			$return_array[2] = $this->panel_settings['layout_count_color_text'];
		}
		
		return $return_array;
	}

	function get_layout_choose_fields_limit( $tmp_field_number = 0 ) {
		$output = 0;

		if ( ! empty( $this->panel_settings["layout_choose_limit_{$tmp_field_number}"] ) ) {
			$output = $this->panel_settings["layout_choose_limit_{$tmp_field_number}"];
		}

		return $output;
	}
	
	function get_layout_user_input_fields_count($tmp_post_id = 0){
		if(!empty($this->panel_settings['layout_user_input_fields'])){
			return $this->panel_settings['layout_user_input_fields'];
		}
		return 3;
	}
	
	function get_layout_user_input_fields_title( $tmp_field_number = 0 ) {
		$output = '';

		if ( ! empty( $this->panel_settings["layout_user_input_label_{$tmp_field_number}"] ) ) {
			$output = $this->panel_settings["layout_user_input_label_{$tmp_field_number}"];
		}

		return $output;
	}
	
	function get_layout_user_input_fields_type($tmp_field_number = 0,$tmp_post_id = 0){
		if(!empty($this->panel_settings['layout_user_input_type_'.$tmp_field_number])){
			return $this->panel_settings['layout_user_input_type_'.$tmp_field_number];
		}
		return 'text';
	}
	
	function get_layout_user_input_fields_must($tmp_field_number = 0,$tmp_post_id = 0){
		if(!empty($this->panel_settings['layout_user_input_check_'.$tmp_field_number])){
			return $this->panel_settings['layout_user_input_check_'.$tmp_field_number];
		}
		return false;
	}	
	
	function get_layout_user_input_fields_slug( $tmp_field_number = 0 ) {
		$output = false;

		if ( ! empty( $this->panel_settings["layout_user_input_slug_{$tmp_field_number}"] ) ) {
			$output = $this->panel_settings["layout_user_input_slug_{$tmp_field_number}"];
		}

		return $output;
	}
	
	function get_rsvp_front_title() {
		$output = '';

		if ( $this->panel_settings['element_show_text_title'] ) {
			$output = $this->panel_settings['text_title'];
		}

		return $output;
	}
	
	function get_rsvp_front_info_title() {
		$output = '';

		if ( $this->panel_settings['element_show_text_info_title'] ) {
			$output = $this->panel_settings['text_info_title'];
		}

		return $output;
	}
	
	function get_rsvp_front_sold_out() {
		$output = '';

		if ( $this->panel_settings['element_show_text_sold_out'] ) {
			$output = $this->panel_settings['text_sold_out'];
		}

		return $output;
	}

	function get_rsvp_front_list_title() {
		$output = '';

		if ( $this->panel_settings['element_show_text_list_title'] ) {
			$output = $this->panel_settings['text_list_title'];
		}

		return $output;
	}

	function get_rsvp_front_email_resiv($tmp_post_id = 0){
		$output = '';

		if ( $this->panel_settings['element_show_text_email_receive'] ) {
			$output = sprintf( '<input type="checkbox" checked="checked" value="1" id="rsvp_sendemail" data-toggle="checkbox"> %s', $this->panel_settings['text_email_receive'] );
		}

		return $output;
	}
	
	function get_rsvp_front_comment() {
		$output = '';

		if ( $this->panel_settings['element_show_input_comment'] ) {
			$output = sprintf( '<textarea class="form-control large" rows="4" id="rsvp_comment" placeholder="%s"></textarea>',
				esc_attr( $this->panel_settings['layout_user_input_label_comment'] )
			);
		}

		return $output;
	}
	
	function get_rsvp_front_manual_join() {
		$output = '';

		if ( $this->panel_settings['rsvp_manualjoin_show'] ) {
			$output = sprintf( '<a href="#JoinandRSVP" class="btn btn-embossed btn-primary btn-social-twitter next" id="rsvp_next" onclick="return rsvp_next(%s);">%s</a>',
				esc_attr( $this->tmp_pack['post_id'] ),
				esc_html( $this->panel_settings['text_button_manual_join'] )
			);
		}

		return $output;
	}

	function get_title_slug_name( $tmp_data ) {
		$tmp_data = strtolower( $tmp_data );
		
		$str1 = array( ' ', 'Æ', 'Ø', 'Å', 'æ', 'ø', 'å' );
		$str2 = array( '_', 'AE', 'OE', 'AA', 'ae', 'oe', 'aa' );
		
		$tmp_data = str_replace( $str1, $str2, $tmp_data );
		$tmp_data = preg_replace( '/[^ \w]+/', '', $tmp_data );

		return $tmp_data;
	}

	function get_the_excerpt( $post_id ) {
		global $post;  
		
		$output = '';
		$save_post = $post;
		$post = get_post( $post_id );

		if ( ! empty( $post ) ) {
			if ( ! empty( $post->post_excerpt ) ) {
				$output = $post->post_excerpt;
			} else {
				$output = $this->get_the_excerpt_from_text( $post->post_content );
			}
		}

		$post = $save_post;
		
		return $output;
	}	
	
	function get_the_excerpt_from_text($text='',$numb=140,$etc='...') {
		if (strlen($text) > $numb) { 
		  $text = substr($text, 0, $numb); 
		  $text = substr($text,0,strrpos($text," ")); 
		  $text = $text.$etc; 
		}
		
		$text = strip_tags($text, '<div><img><small><h1><h2><h3><h4><h5><h6><h7><h8><a>');
		return $text; 
	}	
	
	function check_post_type() {
		global $post, $typenow, $current_screen;
		
		wp_reset_postdata();
		$return = null;
			
		if ( $post && $post->post_type )
			$return =  $post->post_type;
		elseif( $typenow )
			$return = $typenow;
		elseif( $current_screen && $current_screen->post_type )
			$return =  $current_screen->post_type;
		elseif( isset( $_REQUEST['post_type'] ) )
			$return = sanitize_key( $_REQUEST['post_type'] );
		
		return ($return == 'events');
	}
	
	function send_error_die($msg){
		die(json_encode(array('R'=>'ERR','MSG'=>$msg)));
	}	
	
	function send_success_die($tmp_data){
		die(json_encode($tmp_data));
	}
	
	function rsvp_mysql_real_escape_string($tmp_value){
		if(!empty($tmp_value)){
			$tmp_value_array = explode(',', $tmp_value);


			if(!empty($tmp_value_array) and count($tmp_value_array) == 1 and is_numeric($tmp_value_array[0])){
				return $tmp_value;
			} else if(!empty($tmp_value_array) and count($tmp_value_array) == 2 and is_numeric($tmp_value_array[0]) and is_numeric($tmp_value_array[1]) ){
				return $tmp_value;
			} else {
				return '';
			}
		} else {
			return '';
		}
		
	}
	
	function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
		$f = fopen('php://memory', 'w'); 
		foreach ($array as $line) { 
			fputcsv($f, $line, $delimiter); 
		}
		fseek($f, 0);
		header('Content-Type: application/csv');
		header('Content-Disposition: attachement; filename="'.$filename.'";');
		fpassthru($f);
	}	
	
	function generate_avatar( $id = 0, $email = '', $firstName = '', $lastName = '', $externUrlPhoto = '', $externUrl = '',$noname = false ) {
		$return_data_content = '';
		$get_avatar = get_avatar( $email, 75,RHCRSVP_URL . 'images/default_avatar.jpg', $firstName . ' ' . $lastName );
						
		if(!empty($externUrlPhoto)){
			$get_avatar = '<img width="75" height="75" class="avatar avatar-75 photo" src="'.$externUrlPhoto.'" alt="'.$firstName.' '.$lastName.'">';
		}

		$return_data_content .= '<div class="rvsp_tooltip_start avatarid'.$id.'">';
		
		if(!empty($this->panel_settings['rsvp_attend_show_link']) && !empty($externUrl)){
			$return_data_content .= '<a target="_blank" href="'.$externUrl.'">';
		}

		if ( $this->panel_settings['rsvp_attend_show_name'] && ! $noname ) {
			$return_data_content .= sprintf( '<div class="rvsp_tooltip"><div class="rvsp_tooltip_sub">%s</div></div>',
				$lastName ? $firstName . ' ' . $lastName : $firstName
			);
		}

		$return_data_content .= $get_avatar;
		
		if(!empty($this->panel_settings['rsvp_attend_show_link']) && !empty($externUrl)){
			$return_data_content .= '</a>';
		}

		$return_data_content .= '</div>';
		
		return $return_data_content;
	}
	
	function get_gravatar($email) {
		// Craft a potential url and test its headers
		$hash = md5($email);
		$uri = 'http://www.gravatar.com/avatar/' . $hash . '?d=404';
		$headers = @get_headers($uri);
		if (!preg_match("|200|", $headers[0])) {
			return '';
		} else {
			return $uri;
		}
		return '';
	}	
	
	function generate_data_pack( $dep_level = false ) {
		global $post, $wpdb;
		
		self::__construct();

		if ( ! empty( $this->tmp_pack ) && $dep_level = $this->dep_level ) {
			return $this->tmp_pack;
		}

		$this->dep_level = $dep_level;
		$this->tmp_pack = array();
		
		$save_data = array(
			'system'         => 'rsvp_extern',
			'first_name'     => 'rsvp_firstname',
			'last_name'      => 'rsvp_lastname',
			'field'          => 'rsvp_fields',
			'post_id'        => 'rsvp_postID',
			'event_rdate'    => 'event_rdate',
			'answer'         => 'rsvp_answer',
			'to_email'       => 'rsvp_email',
			'comment'        => 'comment',
			'user_photo_url' => 'rsvp_externUrlPhoto',
			'venue'          => 'venue',
			'organizer'      => 'organizer',
			'want_mail'      => 'rsvp_sendemail',
			'user_url'       => 'rsvp_externUrl',
		);
		
		foreach ( $save_data as $key => $tmp ) {
			if ( ! empty( $_REQUEST[ $tmp ] ) ) {
				if ( $key == 'event_rdate'|| $key == 'post_id' ) {
					$this->tmp_pack[ $key ] = $this->rsvp_mysql_real_escape_string( $_REQUEST[ $tmp ] );
				} else {
					$this->tmp_pack[ $key ] = $_REQUEST[ $tmp ];
				}
			} else {
				$this->tmp_pack[ $key ] = '';
			}
		}
		
		if ( empty( $this->tmp_pack['post_id'] ) && ! empty( $post->ID ) ) {
			$this->tmp_pack['post_id'] = $post->ID;
		}
		
		if ( ! empty( $this->tmp_pack['field'] ) ) {
			foreach ( $this->tmp_pack['field'] as $tmp_key => $tmp_data ) {
				$this->tmp_pack['field'][ $tmp_key ] = $this->str_to_utf8( $tmp_data );
			}
		}
		
		if ( 'WP' == $this->tmp_pack['system'] ) {
			$this->tmp_pack['last_name'] = $this->str_to_not_utf8( $this->tmp_pack['last_name'] );
			$this->tmp_pack['first_name'] = $this->str_to_not_utf8( $this->tmp_pack['first_name'] );
		}
		
		if ( empty( $this->tmp_pack['to_email'] ) && ! empty( $this->tmp_pack['field'] ) ) {
			for ( $i = 0; $i < $this->get_layout_user_input_fields_count(); $i++ ) {
				if ( ! empty( $this->panel_settings["layout_user_input_main_email_{$i}"] ) ) {
					$tmp_get_title_slug = $this->get_layout_user_input_fields_slug( $i );

					if ( ! empty( $this->tmp_pack['field'][ $tmp_get_title_slug ] ) ) {
						$this->tmp_pack['to_email'] = $this->tmp_pack['field'][ $tmp_get_title_slug ];
					}
				}
			}
		}
		
		if ( empty( $this->tmp_pack['first_name'] ) && empty( $this->tmp_pack['last_name'] ) && ! empty( $this->tmp_pack['field'] ) ) {
			if ( ! empty( $this->panel_settings['layout_user_input_main_name'] ) ) {
				$tmp_get_title_slug = $this->get_layout_user_input_fields_slug( $this->panel_settings['layout_user_input_main_name'] );

				if ( ! empty( $this->tmp_pack['field'][ $tmp_get_title_slug ] ) ) {
					$tmp_name = $this->tmp_pack['field'][ $tmp_get_title_slug ];

					$last_space = strrpos( $tmp_name, ' ' );
					$last_word = substr( $tmp_name, $last_space );
					$first_chunk = substr( $tmp_name, 0, $last_space );
					
					if ( empty( $first_chunk ) ) {
						$this->tmp_pack['first_name'] = ucfirst( trim( $last_word ) );
						$this->tmp_pack['last_name'] = '';
					} else {
						$this->tmp_pack['last_name'] = ucfirst( trim( $last_word ) );
						$this->tmp_pack['first_name'] = ucfirst( trim( $first_chunk ) );
					}
				}
			}
		}

		if ( ! empty( $this->tmp_pack['post_id'] ) && empty( $this->tmp_pack['event_rdate'] ) ) {
			$this->tmp_pack['event_rdate'] = get_post_meta( $this->tmp_pack['post_id'], 'fc_start_datetime', true );
			$this->tmp_pack['event_rdate'] .= ',' . get_post_meta( $this->tmp_pack['post_id'], 'fc_end_datetime', true );
			$this->tmp_pack['event_rdate'] = str_replace( array( '-', ' ', ':' ), array( '', '', '' ), $this->tmp_pack['event_rdate'] );
		}

		$this->set_data_pack_terms(
			wp_get_post_terms( $this->tmp_pack['post_id'], 'venue', array( 'fields' => 'all' ) ),
			'venue',
			array(
				'name',
				'content',
				'address',
				'email',
				'city',
				'zip',
				'state',
				'country',
				'phone',
				'website',
				'gaddress'
			)
		);

		$this->set_data_pack_terms(
			wp_get_post_terms( $this->tmp_pack['post_id'], 'organizer', array( 'fields' => 'all' ) ),
			'organizer',
			array(
				'name',
				'email',
				'phone',
				'website',
				'content'
			)
		);
		
		$image_scr = wp_get_attachment_image_src( get_post_thumbnail_id( $this->tmp_pack['post_id'] ) );
		$this->tmp_pack['thumbnail'] = $image_scr ? $image_scr : '';
		
		$attachment_id = get_post_meta( $this->tmp_pack['post_id'], 'rhc_tooltip_image', true );

		if ( ! empty( $attachment_id ) && is_numeric( $attachment_id ) ) {
			$this->tmp_pack['rhc_tooltip_image'] = wp_get_attachment_image_src( $attachment_id, array( 64, 64 ) );
			$this->tmp_pack['thumbnail'] = $this->tmp_pack['rhc_tooltip_image'];

			if ( ! empty( $this->tmp_pack['rhc_tooltip_image'] ) ) {
				$this->tmp_pack['rhc_tooltip_image'] = $this->tmp_pack['rhc_tooltip_image'][0];
			}
		} else {
			$this->tmp_pack['rhc_tooltip_image'] = '';
		}
		
		$attachment_id = get_post_meta( $this->tmp_pack['post_id'], 'rhc_top_image', true );

		if ( ! empty( $attachment_id ) && is_numeric( $attachment_id ) ) {
			$this->tmp_pack['rhc_top_image'] = wp_get_attachment_image_src( $attachment_id, array( 64, 64 ) );

			if ( ! empty( $this->tmp_pack['rhc_top_image'] ) ) {
				$this->tmp_pack['rhc_top_image'] = $this->tmp_pack['rhc_top_image'][0];
			}
		} else {
			$this->tmp_pack['rhc_top_image'] = '';
		}

		$attachment_id = get_post_meta( $this->tmp_pack['post_id'], 'rhc_dbox_image', true );
		
		if ( ! empty( $attachment_id ) && is_numeric( $attachment_id ) ) {
			$this->tmp_pack['rhc_dbox_image'] = wp_get_attachment_image_src( $attachment_id, array( 64,64 ) );

			if ( ! empty( $this->tmp_pack['rhc_dbox_image'] ) ) {
				$this->tmp_pack['rhc_dbox_image'] = $this->tmp_pack['rhc_dbox_image'][0];
			}
		} else {
			$this->tmp_pack['rhc_dbox_image'] = '';
		}
		
		if ( ! empty( $this->tmp_pack['thumbnail'] ) ) {
			$this->tmp_pack['thumbnail'] = $this->tmp_pack['thumbnail'][0];
		}

		$this->tmp_pack['user_ip'] = getenv( 'REMOTE_ADDR' );
		$this->tmp_pack['ical_feed'] = site_url() . '/?rhc_action=get_icalendar_events&ID=' . $this->tmp_pack['post_id'];
		$this->tmp_pack['post_permalink'] = get_permalink( $this->tmp_pack['post_id'] );
		$this->tmp_pack['event_permalink'] = add_query_arg( 'event_rdate', $this->tmp_pack['event_rdate'], $this->tmp_pack['post_permalink'] );
		$this->tmp_pack['post_title'] = get_the_title( $this->tmp_pack['post_id'] );
		$this->tmp_pack['excerpt'] = $this->get_the_excerpt( $this->tmp_pack['post_id'] );

		foreach ( $this->get_meta_data as $tmp_key => $tmp_data ) {
			$meta = get_post_meta( $this->tmp_pack['post_id'], $tmp_data['meta_key'], true );

			if ( in_array( $tmp_data['meta_key'], array( 'fc_start', 'fc_end' ) ) ) {
				$meta = $this->convert_date( $meta );
			} elseif ( in_array( $tmp_data['meta_key'], array( 'fc_start_time', 'fc_end_time' ) ) ) {
				$meta = $this->convert_date( $meta, 'time' );
			}

			$this->tmp_pack[ $tmp_key ] = $meta;
		}

		$this->tmp_pack['front_time_type'] = 'M d, Y @ G:i';
		$this->tmp_pack['front_time'] = date( $this->tmp_pack['front_time_type'] );
		
		if ( ! empty( $this->tmp_pack['event_rdate'] ) ) {
			$fc_start = explode( ',', $this->tmp_pack['event_rdate'] );
			$fc_end = $this->convert_date( $fc_start[1] );

			$this->tmp_pack['front_time'] = $fc_start[0];
		
			if ( date( 'G:i', strtotime( $this->tmp_pack['front_time'] ) ) == '0:00' ) {
				$this->tmp_pack['front_time_type'] = 'M d, Y';
			}

			$fc_start = $this->convert_date( $fc_start[0] );;
			
			$this->tmp_pack['event_start_date'] = $fc_start;
			$this->tmp_pack['event_end_date'] = $fc_end;
		}
		
		if ( ! empty( $_REQUEST['event_rdate'] ) ) {
			$datetime = explode( ',', $_REQUEST['event_rdate'] );
		}
	
		if ( empty( $this->tmp_pack['event_start_time'] ) ) {
			$this->tmp_pack['event_start_time'] = '00:00:00';
		}

		if ( empty( $this->tmp_pack['event_end_time'] ) ) {
			$this->tmp_pack['event_end_time'] = '00:00:00';
		}

		if ( isset( $this->panel_settings['rsvp_facebook_publish'] ) ) {
			$this->tmp_pack['fb_pub'] = $this->panel_settings['rsvp_facebook_publish'];
		} else {
			$this->tmp_pack['fb_pub'] = true;
		}

		if ( ! empty( $this->panel_settings['rsvp_public_facebook_text'] ) ) {
			$this->tmp_pack['media_text'] = $this->panel_settings['rsvp_public_facebook_text'];
		} else {
			$this->tmp_pack['media_text'] = '[EXCERPT]';
		}
		
		if ( ! empty( $this->panel_settings['rsvp_public_facebook_title'] ) ) {
			$this->tmp_pack['media_title'] = $this->panel_settings['rsvp_public_facebook_title'];
		} else {
			$this->tmp_pack['media_title'] = 'I am going to [TITLE]. Join me!';
		}	
		
		if ( ! empty( $this->panel_settings['rsvp_public_facebook_subtitle'] ) ) {
			$this->tmp_pack['media_subtitle'] = $this->panel_settings['rsvp_public_facebook_subtitle'];
		} else {
			$this->tmp_pack['media_subtitle'] = '[STARTDATE] @ [STARTTIME]';
		}

		if ( ! empty( $this->panel_settings['rsvp_public_twitter_text'] ) ) {
			$this->tmp_pack['media_twitter_text'] = $this->panel_settings['rsvp_public_twitter_text'];
		} else {
			$this->tmp_pack['media_twitter_text'] = 'I am going to [TITLE]. Join me! [LINK]';
		}			
		
		if ( isset( $this->panel_settings['rsvp_twitter_publish'] ) ) {
			$this->tmp_pack['twitter_pub'] = $this->panel_settings['rsvp_twitter_publish'];
		} else {
			$this->tmp_pack['twitter_pub'] = true;
		}
		
		if ( empty( $this->tmp_pack['user_photo_url'] ) && ! empty( $this->tmp_pack['to_email'] ) ) {
			$avatar_url = $this->get_gravatar( $this->tmp_pack['to_email'] );

			if ( ! empty( $avatar_url ) ) {
				$this->tmp_pack['user_photo_url'] = $avatar_url;
			} else {
				$avatar_url = RHCRSVP_URL . 'images/default_avatar.jpg';
			}
		}

		$this->tmp_pack['media_title'] = $this->replace_tags( $this->tmp_pack['media_title'], $this->tmp_pack, false );
		$this->tmp_pack['media_subtitle'] = $this->replace_tags( $this->tmp_pack['media_subtitle'], $this->tmp_pack, false );
		$this->tmp_pack['media_text'] = $this->replace_tags( $this->tmp_pack['media_text'], $this->tmp_pack, false );
		$this->tmp_pack['media_twitter_text'] = $this->replace_tags( $this->tmp_pack['media_twitter_text'], $this->tmp_pack, false );

		if ( ! empty( $this->tmp_pack['answer'] ) ) {
			$tmp_sql = "SELECT count(id) FROM  {$wpdb->prefix}rhc_rsvp WHERE answer='" . $this->tmp_pack['answer'] . "' and postID=" . $this->tmp_pack['post_id'] . " and event_date='" . $this->tmp_pack['event_rdate'] . "'";

			$this->tmp_pack['current_attending'] = $wpdb->get_var( $tmp_sql );
			$this->tmp_pack['answer_title'] = '';
			$this->tmp_pack['answer_allow'] = false;
			$this->tmp_pack['current_limit'] = 0;

			$this->setup_post_fields( $this->tmp_pack['post_id'] );

			for ( $i = 0; $i < $this->get_layout_choose_fields_count(); $i++ ) {
				if ( $this->get_layout_choose_fields_slug( $i ) == $this->tmp_pack['answer'] ) {
					$this->tmp_pack['current_limit'] = $this->get_layout_choose_fields_limit( $i );
					$this->tmp_pack['answer_title'] = $this->get_layout_choose_fields_title( $i );
					$this->tmp_pack['answer_subtitle'] = $this->get_layout_choose_fields_subtitle( $i );
					
					if ( $this->get_layout_choose_fields_allow( $i ) ) {
						$this->tmp_pack['answer_allow'] = true;
					}

					break;
				}
			}

			$this->tmp_pack['current_limit'] = ( '0' == $this->tmp_pack['current_limit'] ) ? __( 'Unlimited', 'rhcrsvp' ) : $this->tmp_pack['current_limit'];
		}

		return $this->tmp_pack;
	}

	function set_data_pack_terms( $terms, $type, $fields ) {
		if ( ! is_array( $terms ) || ! $terms ) {
			return;
		}

		$prefix = $type . '_';

		foreach ( $terms as $term ) {
			foreach ( $fields as $field ) {
				$key = $prefix . $field;

				if ( in_array( $field, array( 'name', 'description' ) ) ) {
					$value = $term->$field;
				} else {
					$value = get_term_meta( $term->term_id, $field, true );
				}

				if ( ! isset( $this->tmp_pack[ $key ] ) ) {
					$this->tmp_pack[ $key ] = '';
				}

				if ( ! empty( $this->tmp_pack[ $key ] ) ) {
					$this->tmp_pack[ $key ] .= ', ';
				}

				$this->tmp_pack[ $key ] .= $value;
			}
		}
	}

	function get_data_from_db($id_array,$data_array){
		global $wpdb;
		foreach($id_array as $tmp_id){
			foreach($data_array as $tmp_key => $tmp_data){
				$this->tmp_pack[$tmp_key] = '';
				$tmp = '';
				if($tmp_data['meta_key'] == 'name'){
					$tmp = $wpdb->get_var( "SELECT ".$tmp_data['meta_key']." FROM {$wpdb->prefix}terms WHERE term_id=".$tmp_id." limit 1");
				} else {
					$tmp = $wpdb->get_var( "SELECT meta_value FROM {$wpdb->prefix}taxonomymeta WHERE taxonomy_id=".$tmp_id." and meta_key = '".$tmp_data['meta_key']."' limit 1");	
				}
				
				if(!empty($tmp)){
					$this->tmp_pack[$tmp_key] = $tmp;
				}
			}
		}
	}

	function replace_tags( $string, $datapacke, $small = true ) {
		$tmp_array_replace = array(
			'[SYSTEM]',
			'[POST_ID]',
			'[USER_FIRSTNAME]',
			'[USER_LASTNAME]',
			'[ANSWER]',
			'[USER_EMAIL]',
			'[USER_PHOTO_URL]',
			'[USER_PHOTO_IMG]',
			'[TOOLTIP_IMAGE_URL]',
			'[TOOLTIP_IMAGE_IMG]',
			'[DBOX_IMAGE_URL]',
			'[DBOX_IMAGE_IMG]',
			'[TOP_IMAGE_URL]',
			'[TOP_IMAGE_IMG]',
			'[THUMBNAIL_URL]',
			'[THUMBNAIL_IMG]',
			'[USER_URL]',
			'[LINK]',
			'[TITLE]',
			'[LIMIT]',
			'[CURRENT_ATTENDING]',
			'[EXCERPT]',
			'[ATTENDEE_NAME]',
			'[ATTENDEE_EMAIL]',
			'[ATTENDEE_STATUS]',
			'[ICAL_FEED]'
		);

		$tmp_array_replace_to = array(
			$datapacke['system'],
			( isset( $datapacke['post_id'] ) ) ? $datapacke['post_id'] : 0,
			( isset( $datapacke['first_name'] ) ) ? $datapacke['first_name'] : '',
			( isset( $datapacke['last_name'] ) ) ? $datapacke['last_name'] : '',
			( isset( $datapacke['answer_title'] ) ) ? $datapacke['answer_title'] : '',
			( isset( $datapacke['to_email'] ) ) ? $datapacke['to_email'] : '',
			( isset( $datapacke['user_photo_url'] ) ) ? $datapacke['user_photo_url'] : '',
			( isset( $datapacke['user_photo_url'] ) ) ? $this->generate_img( $datapacke['user_photo_url'] ) : '',
			( isset( $datapacke['rhc_tooltip_image'] ) ) ? $datapacke['rhc_tooltip_image'] : '',
			( isset( $datapacke['rhc_tooltip_image'] ) ) ? $this->generate_img( $datapacke['rhc_tooltip_image'] ) : '',
			( isset( $datapacke['rhc_dbox_image'] ) ) ? $datapacke['rhc_dbox_image']:'',
			( isset( $datapacke['rhc_dbox_image'] ) ) ? $this->generate_img( $datapacke['rhc_dbox_image'] ) : '',
			( isset( $datapacke['rhc_top_image'] ) ) ? $datapacke['rhc_top_image'] : '',
			( isset( $datapacke['rhc_top_image'] ) ) ? $this->generate_img( $datapacke['rhc_top_image'] ) : '',
			( isset( $datapacke['thumbnail'] ) ) ? $datapacke['thumbnail'] : '',
			( isset( $datapacke['thumbnail']  ) ) ? $this->generate_img( $datapacke['thumbnail'] ) : '',
			( isset( $datapacke['user_url'] ) ) ? $datapacke['user_url'] : '',
			( isset( $datapacke['event_permalink'] ) ) ? $datapacke['event_permalink'] : '',
			( isset( $datapacke['post_title'] ) ) ? $datapacke['post_title'] : '',
			( isset( $datapacke['current_limit'] ) ) ? $datapacke['current_limit'] : '',
			( isset( $datapacke['current_attending'] ) ) == true ?$datapacke['current_attending']:'',
			( isset( $datapacke['excerpt'] ) ) ? $datapacke['excerpt'] : '',
			( isset( $datapacke['first_name'] ) ) ? $datapacke['first_name'] : '',
			( isset( $datapacke['to_email'] ) ) ? $datapacke['to_email'] : '',
			( isset( $datapacke['answer'] ) ) ? $datapacke['answer'] : '',
			( isset( $datapacke['ical_feed'] ) ) ? $datapacke['ical_feed'] : ''
		);

		for ( $i=0; $i < $this->get_layout_user_input_fields_count(); $i++ ) {
			$tmp_title_slug = $this->get_layout_user_input_fields_slug( $i );

			if ( ! empty( $tmp_title_slug ) ) {
				$tmp_array_replace[] = '[FIELD_' . strtoupper( $tmp_title_slug ) . ']';
				
				if ( ! empty( $datapacke['field'][ $tmp_title_slug ] ) ) {
					$tmp_array_replace_to[] = $datapacke['field'][ $tmp_title_slug ];
				} else {
					$tmp_array_replace_to[] = '';
				}
			}
		}

		foreach ( $this->get_meta_data as $tmp_key => $tmp_data ) {
			$tmp = '';

			if ( ! empty( $datapacke[ $tmp_key ] ) ) {
				$tmp = $datapacke[ $tmp_key ];
			}

			array_push( $tmp_array_replace, $tmp_data['replacetag'] );
			array_push( $tmp_array_replace_to, $tmp );
		}

		if ( $small ) {
			foreach ( $this->get_organizer_data as $tmp_key => $tmp_data ) {
				$tmp = '';

				if ( ! empty( $datapacke[ $tmp_key ] ) ) {
					$tmp = $datapacke[ $tmp_key ];
				}
				
				array_push( $tmp_array_replace, $tmp_data['replacetag'] );
				array_push( $tmp_array_replace_to, $tmp);
			}

			foreach ( $this->get_venue_data as $tmp_key => $tmp_data ) {
				$tmp = '';

				if ( ! empty( $datapacke[ $tmp_key ] ) ) {
					$tmp = $datapacke[ $tmp_key ];
				}

				array_push( $tmp_array_replace, $tmp_data['replacetag'] );
				array_push( $tmp_array_replace_to, $tmp );
			}
		}

		return str_replace( $tmp_array_replace, $tmp_array_replace_to, $string );
	}
	
	function send_email( &$data_pack, $to = '', $from = '', $email_title = '', $email_content = '' ) {
		$email_title = $this->replace_tags( $email_title, $data_pack );
		$email_content = $this->replace_tags( $email_content, $data_pack );

		$headers  = sprintf( "From: %s\r\n", $from ? $from : 'no-reply@no-reply.com' );
		$headers .= "Content-type: text/html\r\n";

		if ( empty( $to ) ) {
			$to = $data_pack['to_email'];
		}

		mail( $to, '=?utf-8?B?' . base64_encode( $email_title ) . '?=', $email_content, $headers );
	}

	function send_email_to_user( &$data_pack ) {
		$this->send_email(
			$data_pack,
			'',
			$this->panel_settings['rsvp_receive_emailfrom'],
			$this->panel_settings['rsvp_receive_emailsub'],
			$this->panel_settings['rsvp_receive_email']
		);
	}
	
	function send_email_to_admin( &$data_pack, $subchange = false ) {
		$post_author_id = get_post_field( 'post_author', $data_pack['post_id'] );
		$to = get_the_author_meta( 'user_email', $post_author_id );

		if ( ! $to ) {
			$to = get_option( 'admin_email', '' ); 
		}

		if ( $to ) {
			$this->send_email(
				$data_pack,
				$to,
				$this->panel_settings['rsvp_admin_receive_emailfrom'],
				$subchange ? $this->panel_settings['rsvp_admin_receive_subchange'] : $this->panel_settings['rsvp_admin_receive_emailsub'],
				$this->panel_settings['rsvp_admin_receive_email'],
				true
			);
		}
	}

	function send_email_to_organizer( &$data_pack, $subchange = false ) {
		$to = array();

		$post_terms = wp_get_post_terms( $data_pack['post_id'], 'organizer' );

		if ( $post_terms ) {
			foreach ( $post_terms as $post_term ) {
				$to[] = get_term_meta( $post_term->term_id, 'email', true ); 
			}
		}

		if ( $to ) {
			$this->send_email(
				$data_pack,
				implode( ',', $to ),
				$this->panel_settings['rsvp_organizer_receive_emailfrom'],
				$subchange ? $this->panel_settings['rsvp_organizer_receive_subchange'] : $this->panel_settings['rsvp_organizer_receive_emailsub'],
				$this->panel_settings['rsvp_organizer_receive_email']
			);
		}
	}
	
	function str_to_utf8( $str ) {
		if ( false === mb_detect_encoding( $str, 'UTF-8', true ) ) {
			$str = utf8_encode( $str );
		}

		return $str;
	}
	
	function str_to_not_utf8( $str ) {
		$str = utf8_decode( $str );

		return $str;
	}
	
	function generate_img( $url, $width = 150 ) {
		if ( empty( $url ) ) {
			return '';
		}

		return '<img src="' . $url . '" width="' . $width . '" style="width:' . $width . 'px;height:auto;">';
	}

	function convert_date( $date, $type = 'date' ) {
		if ( ! in_array( $type, array( 'date', 'time' ) ) ) {
			return;
		}

		return date( get_option( $type . '_format' ), strtotime( $date ) );
	}
}
