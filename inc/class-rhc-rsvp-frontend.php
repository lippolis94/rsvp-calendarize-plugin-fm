<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Dinah!' );
}

class RHC_RSVP_Frontend extends tools_rsvp {
	public $return_data_content = '';

	function __construct() {
		parent::__construct();

		if ( ! is_admin() ) {
			add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ) );
		}
	}
	
	function plugins_loaded() {
		if ( empty( $this->panel_settings['rsvp_allow_on_wp_users'] ) || ( ! empty( $this->panel_settings['rsvp_allow_on_wp_users'] ) && is_user_logged_in() ) ) {

			add_shortcode( 'rhc_rsvp', array( $this,'handle_rsvp_shortcode' ) );
			add_filter( 'rhc_single_template_content_before', array( $this, 'the_content' ), 10, 1 );

			if ( current_user_can( 'rsvp_read_list' ) || current_user_can( 'manage_options' ) ) {
				add_action( 'admin_bar_menu', array( $this, 'toolbar_link_to_mypage' ), 999 );
			}
		}

		add_action( 'wp_enqueue_scripts', array( $this, 'script' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'stylesheet' ) );
	}

	function script() {
		if ( $this->check_post_type() ) {
			wp_register_script( 'rsvp_front', RHCRSVP_URL . 'js/rsvp_front.js', array( 'jquery' ), '1.0.1' );

			$tmp = $this->generate_data_pack();

			wp_localize_script( 'rsvp_front', 'rsvp_vars', array(
				'fb_appID'            => @$this->panel_settings['rsvp_facebook_app_id'],
				'twitter_key'         => @$this->panel_settings['rsvp_twitter_key'],
				'twitter_secret'      => @$this->panel_settings['rsvp_twitter_secret'],
				'siteurl'             => get_site_url(),
				'organizer'           => $tmp['organizer'],
				'venue'               => $tmp['venue'],
				'event_rdate'         => $tmp['event_rdate'],
				'pleaseAttend'        => __( 'Please select type of attendance', 'rhcrsvp' ),
				'pleaseValidateText'  => __( 'Please fill in marked fields', 'rhcrsvp' ),
				'pleaseValidateEmail' => __( 'Please fill in a valid email', 'rhcrsvp' ),
				'pleaseValidateTlf'   => __( 'Please fill in a validate phone number', 'rhcrsvp' ),
				'limitReached'        => __( 'Sorry The limit has been reached', 'rhcrsvp' ),
				'ajaxurl'            => admin_url( 'admin-ajax.php' ),
			));
			wp_enqueue_script( 'rsvp_front' );

			wp_register_script( 'flatui-checkbox', RHCRSVP_URL . 'css/flat-ui/js/flatui-checkbox.js', array( 'jquery' ), '3.0.0' );
			wp_enqueue_script( 'flatui-checkbox' );
		}
	}

	function stylesheet() {
		if($this->check_post_type()){
		
			wp_register_style( 'bootstrap-rsvp', RHCRSVP_URL.'css/bootstrap/css/bootstrap.namespaced.css','','3.0.0' );
			wp_enqueue_style( 'bootstrap-rsvp' );	    
			
			wp_register_style( 'flat-ui-rsvp', RHCRSVP_URL.'css/flat-ui/css/flat-ui.namespaced.css','','1.2.1' );
			wp_enqueue_style( 'flat-ui-rsvp' );	        
			
			wp_register_style( 'frontendrsvp_box', RHCRSVP_URL.'css/frontend.css' );
			wp_enqueue_style( 'frontendrsvp_box' );
		}
	}

	function handle_rsvp_shortcode( $atts, $content = '' ) {
		global $post;

		extract( shortcode_atts( array(
			'post_id' => ''
		), $atts ) );

		$post_id = $post_id ? $post_id : $post->ID;

		if ( $post_id ) {
			$this->tmp_pack['post_id'] = $post_id;

			if ( '1' == get_post_meta( $post_id, '_enable_rsvp', true ) ) {
				$this->setup_post_fields( $post_id );
			}
		}

		$this->attening_list();
		$this->create_col();
		$this->create_box();

		return $this->return_data();
	}
	
	function the_content( $content ) {
		global $post;

		if ( $this->check_post_type() and ! empty( $post->ID ) ) {
			if ( '1' == get_post_meta( $post->ID, 'enable_rsvb_box', true ) ) {
				$content .= '[rhc_rsvp class="rhc-rsvp-auto-box"]';
			}
		}

		return $content;
	}
	
	function toolbar_link_to_mypage( $wp_admin_bar ) {
		global $post;
		if($this->check_post_type()){
		
			$pagetype = 'post_type=events&';
			if(!current_user_can('manage_options') && current_user_can('rsvp_read_list') ){
				$pagetype = '';
			} 	
		
			if(!empty($_GET['event_rdate'])){
				$_REQUEST['event_rdate'] = $_GET['event_rdate'];
			} else if(!empty($_POST['event_rdate'])){
				$_REQUEST['event_rdate'] = $_POST['event_rdate'];
			} else if(!empty($post->ID)){
				$retrive_event_rdate = get_post_meta($post->ID,'fc_start_datetime',true);
				$retrive_event_rdate .= ','.get_post_meta($post->ID,'fc_end_datetime',true); 
				$_REQUEST['event_rdate'] = str_replace(array('-',' ',':'), array('','',''), $retrive_event_rdate);
			}		    
		

			$wp_admin_bar->add_menu( array(
				'id'    => 'rsvp_list',
				'title' => 'RSVP',
				'meta'  => array( 'class' => 'my-toolbar-page' )
			));
			
			$wp_admin_bar->add_menu( array(
				'parent' => 'rsvp_list',
				'id' => 'rsvp_list_backend',
				'title' => __('RSVP List','rhcrsvp'),
				'href' => admin_url( 'edit.php?'.$pagetype.'page=rsvp-list&postID='.$post->ID.'&event_rdate='.$_REQUEST['event_rdate']),
				'meta' => array( 'class' => 'my-toolbar-page' )
			) );
			  
			$wp_admin_bar->add_menu( array(
				'parent' => 'rsvp_list',
				'id' => 'rsvp_list_checkin',
				'title' => __('Check-In Attendees','rhcrsvp'),
				'href' => '#',
				'meta' => array( 'class' => 'my-toolbar-page','onclick' => 'rsvp_show_attending('.$post->ID.',"'.$_REQUEST['event_rdate'].'"); return false;' )
			) );
			
			$wp_admin_bar->add_menu( array(
				'parent' => 'rsvp_list',
				'id' => 'rsvp_list_download_checkin',
				'title' => __('Download Checked-In list','rhcrsvp'),
				'href' => admin_url( 'edit.php?'.$pagetype.'page=rsvp-list&postID='.$post->ID.'&event_rdate='.$_REQUEST['event_rdate'].'&attending_mail_list=true&rsvp_export=csv'),
				'meta' => array( 'class' => 'my-toolbar-page')
			) );	
		}
	}

	function attening_list() {
		$this->return_data_content = '';

		$this->return_data_content .= '<div class="rsvp_attending_list_bg">';
		$this->return_data_content .= '<div class="rsvp_attending_list_inner">';
		$this->return_data_content .= '<div class="rsvp_attending_list_inner2">';
		$this->return_data_content .= '<div class="rsvp_attending_list_main">';
		$this->return_data_content .= '<div class="rsvp_attending_title">' . __( 'Check-In Attendees', 'rhcrsvp' ) . '</div>';
		$this->return_data_content .= '<div class="rsvp_attending_close rsvplist-uniE2C2" onClick="rsvp_hide_attending();"></div>';
		$this->return_data_content .= '<div class="rsvp_attending_main_title_container">';
		$this->return_data_content .= '<div class="rsvp_attending_posttile span9"><div class="rsvp_title_main"><div class="rsvp_title_sub">' . get_the_title( $this->tmp_pack['post_id'] ) . '</div></div></div>';
		$this->return_data_content .= '<div class="rsvp_attending_postdate span3">' . date( $this->tmp_pack['front_time_type'], strtotime( $this->tmp_pack['front_time'] ) ) . '</div>';
		$this->return_data_content .= '</div>';
		$this->return_data_content .= '<div class="rsvp_attending_main_data_container"></div>';
		$this->return_data_content .= '</div>';
		$this->return_data_content .= '</div>';
		$this->return_data_content .= '</div>';
		$this->return_data_content .= '</div>';
	}

	function create_col() {
		global $wpdb;

		$total_joint = array();
		$get_social_connection = get_option( 'rhsco_options' );

		if ( ! empty( $this->tmp_pack['post_id'] ) ) {
			$total_joint = $wpdb->get_results( $wpdb->prepare(
				"
				SELECT    answer, count(id) as answer_count
				FROM      {$wpdb->prefix}rhc_rsvp
				WHERE     postID = %d
				GROUP BY  answer
				ORDER BY  answer
				",
				$this->tmp_pack['post_id'],
				$this->tmp_pack['event_rdate']
			) );
		}

		// 1/3
		$this->return_data_content .= '<div class="rhc-info-cell columbox span12 first">';
		$this->return_data_content .= '<div class="rhc-info-cell fe-cell-label rch_h1"><label class="fe-extrainfo-label">';
		
		$this->return_data_content .= $this->get_rsvp_front_title();

		$this->return_data_content .= '</label></div>';
		$this->return_data_content .= '<div class="span4 first" id="rhc_box_rsvp1">';
		$this->return_data_content .= '<div class="cit_rsvpselect">';
		$this->return_data_content .= '<ul class="nav nav-list">';

		$get_count_color = $this->get_layout_choose_fields_count_color( 0 );
		
		for ( $i = 0; $i < $this->get_layout_choose_fields_count(); $i++ ) {
			$get_title = $this->get_layout_choose_fields_title( $i );
			$get_subtitle = $this->get_layout_choose_fields_subtitle( $i );
			$get_limit = $this->get_layout_choose_fields_limit( $i );
			$get_color = $this->get_layout_choose_fields_color( $i );
			$get_title_slug = $this->get_layout_choose_fields_slug( $i );

			$get_current_count = 0;
			
			foreach ( $total_joint as $tmp_key => $tmp_data ) {
				if ( $get_title_slug == $tmp_data->answer ) {
					$get_current_count = $tmp_data->answer_count;
					break;
				}
			}				
			
			$class = '';
			$alt = 'enabled';

			if ( ! empty( $get_limit ) && $get_limit > 0 && $get_limit <= $get_current_count ) {
				$class = 'disabled';
				$alt = 'disabled';
				$get_title .= ' <span class="rsvp_solgout">' . __( $this->get_rsvp_front_sold_out( $this->tmp_pack['post_id'] ), 'rhcrsvp' ) . '</span>';
			}

			$this->return_data_content .= '<li style="background-color:'.$get_color[0].';" class="'.$class.'" unselectable="on" onselectstart="return false;" onmousedown="return false;" data-normal-color="'.$get_color[0].'" data-hover-color="'.$get_color[1].'" data-slug="'.$get_title_slug.'" data-limit="'.$get_limit.'" data-id="'.$i.'" data-status="'.$alt.'"><i class="fui-user rsvp_object" style="color:'.$get_count_color[0].'"><span class="iconbar-unread rsvp_number" style="background-color:'.$get_count_color[1].';color:'.$get_count_color[2].';">'.$get_current_count.'</span></i><span class="rsvp_namespace" style="color:'.$get_color[2].';">'.$get_title.'<br><span class="rsvp_smallnamespace" style="color:'.$get_color[2].';">' . $get_subtitle . '</span></span><div class="rsvp_dropbox fui-check"></div></li>';
		}
		
		$this->return_data_content .= '</ul></div></div>';

			// 2/3	
			$this->return_data_content .= '<div class="span8" id="rhc_box_rsvp2">';
				$this->return_data_content .= '<div class="rhc-info-cell fe-cell-taxonomymeta rch_h2"><label class="fe-extrainfo-label">';
					$this->return_data_content .= $this->get_rsvp_front_info_title($this->tmp_pack['post_id']);	
				$this->return_data_content .= '</label></div>';
				
				
				
				$this->return_data_content .= '<div class="span12 rsvp_stage1hide rsvp_infobox first bottomspace">';
				
				if(empty($this->panel_settings['rsvp_remove_manaul_input'])){
					$this->return_data_content .= '<div class="rsvp_manual">';	

						for($i=0;$i<$this->get_layout_user_input_fields_count($this->tmp_pack['post_id']);$i++){
							$get_title = $this->get_layout_user_input_fields_title($i,$this->tmp_pack['post_id']);
							$get_type = $this->get_layout_user_input_fields_type($i,$this->tmp_pack['post_id']);
							$get_must = $this->get_layout_user_input_fields_must($i,$this->tmp_pack['post_id']);
							$class = ($i==0?' notopadd':'');
							
							$get_title_slug = $this->get_layout_user_input_fields_slug($i,$this->tmp_pack['post_id']);

							
							$this->return_data_content .= '<input id="rsvp_user_input_'.$i.'" data-slug="'.$get_title_slug.'" data-type-nr="'.$i.'" data-must="'.$get_must.'" data-type="'.$get_type.'" type="text" class="rsvp_info_input form-control large'.$class.'" placeholder="'.__($get_title,'rhcrsvp').' '.(!empty($get_must) == true ? __('*','rhcrsvp') : '').'">';
							
						}
						$this->return_data_content .= '<input id="rsvp_extern" type="hidden" value=""><input id="rsvp_externUrl" type="hidden" value="">';
					$this->return_data_content .= '</div>';
				}
				$this->return_data_content .= '<div class="comment_specialbox" style="min-height:0px;">';
					if(!empty($this->panel_settings['element_show_input_comment'])){
						$this->return_data_content .= $this->get_rsvp_front_comment($this->tmp_pack['post_id']);
					}
				
					$this->return_data_content .= '<span><label class="checkbox" for="checkbox1">';
						$this->return_data_content .= $this->get_rsvp_front_email_resiv($this->tmp_pack['post_id']);
					$this->return_data_content .= '</label></span>';
				$this->return_data_content .= '</div>';
				
				$this->return_data_content .= '</div>';
				
				
				
			$this->return_data_content .= '</div>';
			
			$this->return_data_content .= '<div class="span12 first comment_specialbox_extra" style="min-height:0px;">';
			$this->return_data_content .= '</div>';
			
			// 3/3	
		
			// Error Alert 
			$this->return_data_content .= '<div class="rsvp_alert_dialog span12 first" style="display:none;">';
				$this->return_data_content .= '<div class="alert alert-error">';
					$this->return_data_content .= '<button data-dismiss="alert" class="close fui-cross" type="button"></button>';
					$this->return_data_content .= '<div class="text"></div>';
				$this->return_data_content .= '</div>';
			$this->return_data_content .= '</div>';	
			
			
			$this->return_data_content .= '<div class="rsvp_success_dialog span12 first" style="display:none;">';
				$this->return_data_content .= '<div class="alert alert-success">';
					$this->return_data_content .= '<button data-dismiss="alert" class="close fui-cross" type="button"></button>';
					$this->return_data_content .= '<div class="text"></div>';
				$this->return_data_content .= '</div>';
			$this->return_data_content .= '</div>';	
	
		// add loader
			$this->return_data_content .= '<div class="fc-view-loading loading-events" style="display:none;"><div class="fc-view-loading-1 ajax-loader loading-events"><div class="fc-view-loading-2x xspinner icon-xspinner-3"></div></div></div>';

			$this->return_data_content .= '<div class="span12 first buttoncontrole">';
				if(empty($this->panel_settings['rsvp_remove_manaul_input'])){
					$this->return_data_content .= $this->get_rsvp_front_manual_join($this->tmp_pack['post_id']);	
				}
				
				
				$generate_social_button_array = array(
								  'wp'	=> array(array('rsvp_wp_show'),array()),
								  'twitter' => array(array('rsvp_twitter_show'),array('twitter_key','twitter_secret')),
								  'linkedin' => array(array('rsvp_linkedin_show'),array('linkedin_app_key')),
								  'facebook' => array(array('rsvp_facebook_show'),array('facebook_app_id')),
								  'googleplus' => array(array('rsvp_googleplus_show'),array('googleplus_apikey','googleplus_clientid'))
								  );
				
				foreach($generate_social_button_array as $tmp_key => $tmp_main){
					$social_show = true;
					
					foreach($tmp_main[0] as $tmp_sub){
						if(isset($this->panel_settings[$tmp_sub]) && empty($this->panel_settings[$tmp_sub]))
							$social_show = false;
					}
					
					foreach($tmp_main[1] as $tmp_sub){
						if(isset($get_social_connection[$tmp_sub]) && empty($get_social_connection[$tmp_sub]))
							$social_show = false;
					}		
					
					if($social_show){
						$this->return_data_content .= '<a href="#'.$tmp_key.'" class="btn btn-sm btn-social-'.$tmp_key.' '.$tmp_key.' " id="rsvp_'.$tmp_key.'" onclick="return rsvp_social_connection('.$this->tmp_pack['post_id'].',\''.$tmp_key.'\');"><i class="fui-'.$tmp_key.'"></i> '.(!empty($this->panel_settings['text_social_'.$tmp_key])== true ?__($this->panel_settings['text_social_'.$tmp_key],'rhcrsvp'):'').'</a>';//Connect with Twitter
					}				
				}

			$this->return_data_content .= '<div class="clear"></div></div>';
	
		// show all new line
	
		if(!empty($this->panel_settings['rsvp_attend_show'])){
			
			$tmp_check_string = '';
			
			for($i=0;$i< $this->get_layout_choose_fields_count($this->tmp_pack['post_id']);$i++){
				if($this->get_layout_choose_fields_allow($i,$this->tmp_pack['post_id'])){
					$get_title_slug = $this->get_layout_choose_fields_slug($i,$this->tmp_pack['post_id']);

					if(!empty($tmp_check_string)){
						$tmp_check_string .= ',';
					}
					$tmp_check_string .= "'".$get_title_slug."'";
				}
				
			}

			$get_list_date = '';

			if ( ! empty( $tmp_check_string ) ) {
				$get_list_date = $wpdb->get_results("SELECT id,email,firstName,lastName,externUrlPhoto,externUrl, multidata FROM {$wpdb->prefix}rhc_rsvp WHERE postID=".$this->tmp_pack['post_id']." and answer in (".$tmp_check_string.") and event_date='".$this->tmp_pack['event_rdate']."'");
			}
			
			$this->return_data_content .= '<div id="rsvp_scroll_attending" class="span12 first" style="display:'.(!empty($get_list_date)=='true'?'block':'none').';">';
			$this->return_data_content .= '<div class="rhc-info-cell fe-cell-label rch_h1"><label class="fe-extrainfo-label">';
			$this->return_data_content .= $this->get_rsvp_front_list_title();
			$this->return_data_content .= '</label></div>';
			$this->return_data_content .= '<div class="rsvp_scroll">';

			if ( ! empty( $get_list_date ) ) {
				foreach ( $get_list_date as $tmp_key => $tmp_data ) {
					if ( ! $tmp_data->firstName && ! $tmp_data->firstName ) {
						$tmp_data->multidata = unserialize( $tmp_data->multidata );
						$tmp_data->firstName = empty( $tmp_data->multidata['name'] ) ? '' : $tmp_data->multidata['name'];
					}
				
					$this->return_data_content .= $this->generate_avatar(
						$tmp_data->id,
						$tmp_data->email,
						$tmp_data->firstName,
						$tmp_data->lastName,
						$tmp_data->externUrlPhoto,
						$tmp_data->externUrl
					);
				}
			}

			$this->return_data_content .= '</div></div>';
		}

		$this->return_data_content .= '</div>';
	}
	
	function create_box(){
		$tmp = '<div class="rhc fe-extrainfo-container se-rsvpbox"><div class="fe-extrainfo-container2 row-fluid cit_rsvp"><div class="fe-extrainfo-holder">';
			$tmp .= $this->return_data_content;
		$tmp .= '</div></div></div>';
		
		$this->return_data_content = $tmp;
	}	
	
	function return_data() {
		return $this->return_data_content;
	}
}

return new RHC_RSVP_Frontend();