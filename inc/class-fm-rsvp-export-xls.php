<?php 

/**
 * @author Angelo L.
 * Classe per fare un'esportazione solo dei custom fields dell'RSVP in un file xls
 * @param int $post_id
 * @param array $columns
 */

class RSVP_Export_XLS_FM
{
    public $result;
    public $post_id;

    public function __construct($post_id = 0)
    {
        $this->post_id = $post_id;
        $this->columns = $columns;
        add_action( 'rest_api_init', [ 'RSVP_Export_XLS_FM', 'add_custom_routes' ] );
    }
    public function getContent()
    {
        global $wpdb;
        return $wpdb->get_results("SELECT id, multidata FROM {$wpdb->prefix}rhc_rsvp WHERE postID=" . strval($this->post_id) . " order by answer DESC,firstName ASC",'ARRAY_N');
    }
    public function setContent()
    {
        $data = $this->getContent();
        $result = [];
        $result[] = array(
                'ID' => 'ID',
                'Cognome' => 'Cognome',
                'Nome' => 'Nome',
                'Email' => 'Email',
                'Telefono' => 'Telefono',
                'Iscritto' => 'Iscritto'
        );
        foreach($data as $content)
        {
            $unserialized_content = unserialize($content[1]);
            $result[] = array(
                'ID' => $content[0],
                'Cognome' => $unserialized_content['cognome'],
                'Nome' => $unserialized_content['nome'],
                'Email' => $unserialized_content['email'],
                'Telefono' => $unserialized_content['telefono'],
                'Iscritto' => $unserialized_content['iscritto'] == 'iscritto' ? 'Si' : 'No'
            );
        }
        return $result;       
    }
    public function header()
    {
        $this->result .= '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
        <head>
            <!--[if gte mso 9]>
            <xml>
                <x:ExcelWorkbook>
                    <x:ExcelWorksheets>
                        <x:ExcelWorksheet>
                            <x:Name>Sheet 1</x:Name>
                            <x:WorksheetOptions>
                                <x:Print>
                                    <x:ValidPrinterInfo/>
                                </x:Print>
                            </x:WorksheetOptions>
                        </x:ExcelWorksheet>
                    </x:ExcelWorksheets>
                </x:ExcelWorkbook>
            </xml>
            <![endif]-->
        </head>
        
        <body>
           <table>';
    }
    public function footer()
    {
        $this->result .= '    </table>
        </body></html>';
    }
    public function tableData()
    {
        $contents = $this->setContent();
        $columns = ['ID', 'Cognome', 'Nome', 'Email', 'Telefono', 'Iscritto'];

        foreach($contents as $content)
        {
            $this->result .= '<tr>';
            foreach($columns as $column)
            {
                $this->result .= '<td>' . $content[$column] .'</td>';
            }
            $this->result .= '</tr>';
        }
    }
    public function save()
    {
        $this->header();
        $this->tableData();
        $this->footer();
        return $this->result;
    }

    /**
	 * Endpoint per scaricare il report semplificato degli iscritti ad un evento
	 * @param int $id,
	 * @param string $fileName
	 */
	public function handle_download_rsvp_simplified_report( WP_REST_Request $req )
	{
		$id = $req['id'];
		$fileName = $req['fileName'] . '.xls';

		$export = new RSVP_Export_XLS_FM($id);
		$file = wp_upload_bits($fileName, null, $export->save());
		
		wp_redirect($file['url']);
		wp_send_json_success();
    }
    
    public function add_custom_routes()
    {
        register_rest_route( 'federmanager/v1', '/getreport', array(
			'methods' => 'GET',
			'callback' => ['RSVP_Export_XLS_FM', 'handle_download_rsvp_simplified_report']
		));
    }
}

new RSVP_Export_XLS_FM();

?>