<?php
/*
Plugin Name: RSVP Federmanager - Custom Plugin Calendarize
Description: Plugin per la gestione delle iscrizioni agli eventi.
Version: 1.0.0
*/

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Dinah!' );
}

if ( defined( 'RHCRSVP_PATH' ) ) {
	throw new Exception ( __( 'A duplicate of this add-on/plugin is already active.', 'rhc' ) );
}

define( 'RHCRSVP_VERSION', '4.0.7' );
define( 'RHCRSVP_SLUG', plugin_basename( __FILE__ ) );

if(defined('RHC_ADDON_PATH')){
	define('RHCRSVP_PATH', trailingslashit(RHC_ADDON_PATH . dirname($addon)) ); 
	define("RHCRSVP_URL", trailingslashit(RHC_ADDON_URL . dirname($addon)) );

	if( !function_exists('rhc_getRelativePath') ):
	function rhc_getRelativePath($from, $to){
		$from = is_dir($from) ? rtrim($from, '\/') . '/' : $from;
		$to   = is_dir($to)   ? rtrim($to, '\/') . '/'   : $to;
		$from = str_replace('\\', '/', $from);
		$to   = str_replace('\\', '/', $to);
	
		$from     = explode('/', $from);
		$to       = explode('/', $to);
		$relPath  = $to;
	
		foreach($from as $depth => $dir) {
			if($dir === $to[$depth]) {
				array_shift($relPath);
			} else {
				$remaining = count($from) - $depth;
				if($remaining > 1) {
					$padLength = (count($relPath) + $remaining - 1) * -1;
					$relPath = array_pad($relPath, $padLength, '..');
					break;
				} else {
					$relPath[0] = './' . $relPath[0];
				}
			}
		}
		return implode('/', $relPath);
	}
	endif;
	
	$language_path = rhc_getRelativePath( WP_PLUGIN_DIR, RHCRSVP_PATH ) . 'languages/';
	define('RHC_RSVP_LANGUAGE', $language_path);	
}else{
	define('RHCRSVP_PATH', plugin_dir_path(__FILE__) ); 
	define("RHCRSVP_URL", plugin_dir_url(__FILE__) );
	define('RHC_RSVP_LANGUAGE', RHCRSVP_PATH.'languages/');
}
 
require_once( RHCRSVP_PATH . 'inc/uh_social/social_loader.php' ); 
require_once( RHCRSVP_PATH . 'inc/tools.php' );
require_once( RHCRSVP_PATH . 'inc/class-rhc-rsvp-frontend.php' );
require_once( RHCRSVP_PATH . 'inc/class-rhc-rsvp-backend.php' );
require_once( RHCRSVP_PATH . 'inc/class-rhc-rsvp-options.php' );
require_once( RHCRSVP_PATH . 'inc/class-rhc-rsvp-visual-composer.php' );

// FEDERMANAGER EXPORT XLS Customized
require_once( RHCRSVP_PATH . 'inc/class-fm-rsvp-export-xls.php');
// FEDERMANAGER DELETE SUBSCRIPTION ENDPOINT
require_once( RHCRSVP_PATH . 'inc/class-fm-rsvp-delete-subscription.php');
// FEDERMANAGER LIMITS ALERT MAIL ON MAXIMUM SUBSCRIPTION REACHED RSVP
require_once( RHCRSVP_PATH . 'inc/class-fm-rsvp-limits.php');
// FEDERMANAGER RSVP CUSTOMIZATION PAGE (options page WP)
require_once( RHCRSVP_PATH . 'rsvp-options.php');

class Plugin_RHC_RSVP extends tools_rsvp {
	function __construct() {
		rh_register_php( 'social-connection', RHCRSVP_PATH . 'social_connection/class.SocialConnection.php', '1.0.2' );

		add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ) );
		add_action( 'after_setup_theme', array( $this, 'after_setup_theme'), 20 );
	}
	
	function after_setup_theme() {
		global $rhc_plugin;

		load_plugin_textdomain( 'rhcrsvp', null, RHC_RSVP_LANGUAGE );
		
		do_action('rh-php-commons');

		$this->handle_sco_back_compat();

		$settings = array(
			'options_capability' => $rhc_plugin->options_capability,
			'option_menu_parent' => 'edit.php?post_type=' . RHC_EVENTS,
			'stylesheet'         => 'rhc-options',
			'path'               => RHCRSVP_PATH . 'social_connection/',
			'url'                => RHCRSVP_URL . 'social_connection/',
			'pop_path'           => RHC_PATH . 'options-panel/',
			'pop_url'            => RHC_URL . 'options-panel/',
			'networks'           => array(),
			'layout'             => 'horizontal',
		);

		new SocialConnection( $settings );
	}

	function plugins_loaded() {
		global $uh_social_loader;

		$this->install();
		
		$uh_social_loader->__construct(
			array(
				'post_type' => 'events',
				'URL'       => RHCRSVP_URL . 'inc/uh_social/',
				'PATH'      => RHCRSVP_PATH . 'inc/uh_social/'
			), 
			RHCRSVP_PATH . 'inc/uh_social/',
			1.1
		);
		
		add_action( 'wp_ajax_retrive_attendinglist', array( $this, 'retrive_attendinglist' ) );
		add_action( 'wp_ajax_rsvp_change_attending', array( $this, 'rsvp_change_attending' ) );
		add_action( 'wp_ajax_rsvp_saveanswer', array( $this, 'saveanswer' ) );
		add_action( 'wp_ajax_nopriv_rsvp_saveanswer', array( $this, 'saveanswer' ) );
	}

	function handle_sco_back_compat() {
		// Social network api keys where stored in the rhc_options option, it will now have its own var for other plugins to use it.
		if ( '' == get_option( 'rhsco_options', '' ) ) {
			global $rhc_plugin;
			
			$copy_fields = array(
				'sco_login_behavior',
				'login_behavior_group',
				'login_redirect_to',
				'dummy_email_domain',
				'enable_wp_login',
				'network_order',
				'googleplus_enable',
				'googleplus_clientid',
				'googleplus_apikey',
				'facebook_enable',
				'facebook_app_id',
				'facebook_app_secret',
				'linkedin_enable',
				'linkedin_app_key',
				'linkedin_app_secret',
				'microsoft_enable',
				'microsoft_client_id',
				'microsoft_client_secret',
				'twitter_enable',
				'twitter_key',
				'twitter_secret'
			);		
			
			$options = array();

			foreach ( $copy_fields as $field ) {
				$options[ $field ] = $rhc_plugin->get_option( $field, '', true );
			}
			
			update_option( 'rhsco_options', $options );
		}
	}
	
	function rsvp_change_attending() {
		global $wpdb;
		
		$tmp_time = '0000-00-00 00:00:00';
		
		if(!empty($_REQUEST['rsvp_status'])){
			$tmp_time = date('Y-m-d H:i:s');
		}
		
		if(empty($_REQUEST['rsvp_ID']) or !is_numeric($_REQUEST['rsvp_ID'])){
			$this->send_success_die(array('R'=>'ERROR','MSG'=>'contact support'));
		}
		
		$wpdb->update( 
			$wpdb->prefix.'rhc_rsvp', 
			array( 
				'checkedin' => $tmp_time,	// string
			), 
			array( 'id' => $_REQUEST['rsvp_ID'] ), 
			array( 
				'%s',
			), 
			array( '%d' ) 
		);
		
		if(!empty($_REQUEST['rsvp_status'])){
			$tmp_time = date('M d, Y @ g:i a',strtotime($tmp_time));
		} else {
			$tmp_time = '';
		}
		
		$this->send_success_die(array('R'=>'DONE','DATESTR'=>$tmp_time,'THIS'=>$_REQUEST['tmp_this']));
		
	}
	
	function retrive_attendinglist() {
		global $wpdb;
		
		$tmp = $this->generate_data_pack();
		
		$tmp_check_string = '';
		for($i=0;$i< $this->get_layout_choose_fields_count(0);$i++){
			if($this->get_layout_choose_fields_allow($i,0)){
				$get_title_slug = $this->get_layout_choose_fields_slug($i,0);

				if(!empty($tmp_check_string)){
					$tmp_check_string .= ',';
				}
				$tmp_check_string .= "'".$get_title_slug."'";
			}
		}
		
		if(!empty($tmp_check_string)){
			$get_list_date = $wpdb->get_results("SELECT id,email,firstName,lastName,externUrlPhoto,checkedin,multidata FROM {$wpdb->prefix}rhc_rsvp WHERE postID=".$tmp['post_id']." and event_date='".$tmp['event_rdate']."' and answer in (".$tmp_check_string.") order by firstName ASC");
		}

		$return_data = '<table>';
		$count = 1;
		if(!empty($get_list_date)){
			foreach($get_list_date as $k_tmp => $d_tmp){	
				$return_data .= '<tr>';
					$return_data .= '<td>';
						$return_data .= '<span class="rsvp_attending_count">#'.$count.'</span>';	
					$return_data .= '</td>';
					$return_data .= '<td>';
						$return_data .= $this->generate_avatar($d_tmp->id,$d_tmp->email,$d_tmp->firstName,$d_tmp->lastName,$d_tmp->externUrlPhoto,'',true);
					$return_data .= '</td>';
					$return_data .= '<td>';
						$return_data .= '<div class="rsvp_attending_name"><div class="rsvp_title_main"><div class="rsvp_title_sub">'.$d_tmp->firstName.' '. $d_tmp->lastName. '</div><div onclick="rsvp_list_show_more(this);return true;" class="show_more" data-textA="showmore" data-textB="hide">showmore</div>';
						
						
						$return_data .='<div class="show_more_object">';
						
						if(!empty($d_tmp->multidata)){
							$multidata = unserialize($d_tmp->multidata);
	
							foreach($multidata as $multi_tmp_key => $multi_tmp_data){
								$return_data .= '<small><lable style="display: inline-block; min-width: 60px; text-transform: capitalize;">'.$multi_tmp_key.': </lable> <span style="display: inline-block; min-width: 60px; text-transform: capitalize;">'.$multi_tmp_data.'</span></small>';
							}
						}
						
						
						$return_data .='</div></div></div>';
					$return_data .= '</td>';
					$return_data .= '<td>';
						$return_data .= '<div class="rsvp_title_main">';
							$return_data .= '<div class="btn-toolbar rsvp_attending_button_group">';
								$return_data .= '<div class="btn-group">';
									$return_data .= '<a href="#" onclick="rsvp_change_attending(this,'.$d_tmp->id.',1);return false;" class="btn btn-primary '.($d_tmp->checkedin != '0000-00-00 00:00:00' ? 'active' : '').'">'.__('Checked-In','rhcrsvp').'</a>';
									$return_data .= '<a href="#" onclick="rsvp_change_attending(this,'.$d_tmp->id.',0);return false;" class="btn btn-primary '.($d_tmp->checkedin == '0000-00-00 00:00:00' ? 'active' : '').'">'.__('Not Checked','rhcrsvp').'</a>';
								$return_data .= '</div>';
							$return_data .= '</div>';
						$return_data .= '</div>';
					$return_data .= '</td>';
					$return_data .= '<td>';
						$return_data .= '<span class="rsvp_attending_checkin_date">'.($d_tmp->checkedin != '0000-00-00 00:00:00' ? date('M d, Y @ g:i a',strtotime($d_tmp->checkedin)) : '').'</span>';	
					$return_data .= '</td>';
				$return_data .= '</tr>';
				$count++;
			}
		} else {
			$return_data .= '<tr><td>'.__('sorry no attending','rhcrsvp').'</td></tr>';
		}
		$return_data .= '</table>';
		
		$this->send_success_die( array(
			'R'           => 'DONE',
			'MSG'         => 'ok',
			'RETURNVALUE' => $return_data,
		) );
	}
	
	function saveanswer() {
		global $wpdb,$post;

		$tmp_data_pack = $this->generate_data_pack();

		// Check Limits FM Alert
		$fm_rsvp_options = get_option( 'FM_RSVP_settings' );
		if($fm_rsvp_options['fm_limits_checkbox'] == 1)
		{
			RSVP_Limits_FM::checkLimits($tmp_data_pack['post_id']);
		}
			
		if ( empty( $tmp_data_pack['post_id'] ) || ! is_numeric( $tmp_data_pack['post_id'] ) ) {
			$this->send_error_die( __( 'No post id.', 'rhcrsvp' ) );
		}
		
		if ( empty( $tmp_data_pack['answer'] ) ) {
			$this->send_error_die( __( 'No answer found.', 'rhcrsvp' ) );
		}

		if ( empty( $tmp_data_pack['to_email'] ) ) {
			$this->send_error_die( __( 'In order to RSVP you must provide valid email.', 'rhcrsvp' ) );
		}

		if ( ! empty( $tmp_data_pack['current_limit'] ) && $tmp_data_pack['current_attending'] >= $tmp_data_pack['current_limit'] ) {
			$this->send_error_die( __( 'Sorry The limit has been reached.', 'rhcrsvp' ) );
		}
 
		$check = $wpdb->get_results("SELECT id,answer FROM {$wpdb->prefix}rhc_rsvp WHERE postID=".$tmp_data_pack['post_id']." and email like '%".$tmp_data_pack['to_email']."%' and event_date='".$tmp_data_pack['event_rdate']."' limit 1" );	    
	   
		if(!empty($check)){
			$check = $check[0];
			$out_off_time = true;
			$tmp_time_check = array();
			$tmp_time_check[] = date('Y-m-d H:i:s');
			
			$time_diffenrence = '6';
			
			if(!empty($this->panel_settings['rsvp_attendee_time'])){
				$time_diffenrence = $this->panel_settings['rsvp_attendee_time'];
			}
			
			if(!empty($this->tmp_pack['event_rdate'])){
				$tmp_time_check = explode(',',$this->tmp_pack['event_rdate']);
				$tmp_time_check = date('YmdHis', strtotime($tmp_time_check[0]));
			}
			if($time_diffenrence != '-'){
				$tmp_current_date = date('YmdHis',strtotime("+".$time_diffenrence." hours"));
			
				if($tmp_time_check < $tmp_current_date){
					$out_off_time = false;
				}
			}
			
			if($tmp_data_pack['answer'] == $check->answer){
					$this->send_error_die(__('Your status is already registered.','rhcrsvp'));
			}

			if ( $out_off_time ) {
				$wpdb->update( 
					$wpdb->prefix.'rhc_rsvp', 
					array( 'answer' => $tmp_data_pack['answer'],), 
					array( 'id' => $check->id ), 
					array( '%s',), 
					array( '%d' )
				);
			
				$get_avatar ='';
				if(!empty($this->panel_settings['rsvp_attend_show']) && $tmp_data_pack['answer_allow']){
					$get_avatar = $this->generate_avatar($check->id,$tmp_data_pack['to_email'],$tmp_data_pack['first_name'],$tmp_data_pack['last_name'],$tmp_data_pack['user_photo_url'],$tmp_data_pack['user_url']);
				}		    
			
				if ( $this->panel_settings['rsvp_admin_email'] ) {
					$this->send_email_to_admin( $tmp_data_pack, true );
				}

				if ( $this->panel_settings['rsvp_organizer_email'] ) {
					$this->send_email_to_organizer( $tmp_data_pack, true );
				}
				
				$this->send_success_die( array(
					'R'            => 'DONE',
					'CHANGE'       => 'true',
					'AnswerAllow'  => $tmp_data_pack['answer_allow'],
					'CHANGEANSWER' => $check->answer,
					'REMOVEID'     => $check->id,
					'MSG'          => sprintf( __( 'You have updated your attendee status to "%s"', 'rhcrsvp' ), $tmp_data_pack['answer_title'] ),
					'AVATAR'       => $get_avatar
				) );
			} else {
				$this->send_error_die( __( 'Sorry, but you can not change your status at this time.', 'rhcrsvp' ) );
			}
		}

		$sqlevent = $wpdb->insert( 
			$wpdb->prefix.'rhc_rsvp', 
			array( 
				'postID' => $tmp_data_pack['post_id'], 
				'firstName' => $tmp_data_pack['first_name'],
				'lastName' => $tmp_data_pack['last_name'],
				'comment' => $tmp_data_pack['comment'],
				'email' => $tmp_data_pack['to_email'],
				'answer' => $tmp_data_pack['answer'],
				'extern' => $tmp_data_pack['system'],
				'externUrl' => $tmp_data_pack['user_url'],
				'externUrlPhoto' =>$tmp_data_pack['user_photo_url'],
				'event_date' =>$tmp_data_pack['event_rdate'],
				'loginip' => $tmp_data_pack['user_ip'],
				'multidata' => serialize($tmp_data_pack['field'])
			), 
			array( 
			'%d', 
			'%s',
			'%s', 
			'%s', 
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s'
			)
		);
		
		$this->tmp_pack['current_attending']++;
		
		$curent_avatar_id = $wpdb->insert_id;

		if ( $tmp_data_pack['want_mail'] && $tmp_data_pack['answer_allow'] && $tmp_data_pack['to_email'] ) {
			$this->send_email_to_user( $tmp_data_pack );
		}

		if ( $this->panel_settings['rsvp_admin_email'] ) {
			$this->send_email_to_admin( $tmp_data_pack );
		}

		if ( $this->panel_settings['rsvp_organizer_email'] ) {
			$this->send_email_to_organizer( $tmp_data_pack );
		}

		$return_string = __( 'Thank you for your registration.', 'rhcrsvp' );
		$get_avatar = '';

		if(!empty($this->panel_settings['rsvp_attend_show']) && $tmp_data_pack['answer_allow']){
			$get_avatar = $this->generate_avatar($curent_avatar_id,$tmp_data_pack['to_email'],$tmp_data_pack['first_name'],$tmp_data_pack['last_name'],$tmp_data_pack['user_photo_url'],$tmp_data_pack['user_url']);
		}

		$this->send_success_die(array('R'=>'DONE','LINK'=>$tmp_data_pack['event_permalink'],'THUMBNAIL'=>$tmp_data_pack['thumbnail'],'FBPUB'=>$tmp_data_pack['fb_pub'],
		'FBTEXT'=>$tmp_data_pack['media_text'],'FBSUBTITLE'=>$tmp_data_pack['media_subtitle'],'FBTITLE'=>$tmp_data_pack['media_title'],'AnswerAllow'=>$tmp_data_pack['answer_allow'],'MSG'=>$return_string,'AVATAR'=>$get_avatar));
	}
	
	function install(){
		global $wpdb;
		
		$installed = get_option('RHCRSVP_INSTALL','');	    
		
		if(empty($installed) or version_compare($installed, '1.0.0','<')) {
			$queries[] =  "CREATE TABLE IF NOT EXISTS `#__rhc_rsvp` (
			`id` int(11) NOT NULL auto_increment,
			`postID` int(11) NOT NULL,
			`firstName` varchar(255) NOT NULL,
			`lastName` varchar(255) NOT NULL,
			`comment` text COLLATE utf8_unicode_ci NOT NULL,
			`email` varchar(255) NOT NULL,
			`answer` int(1) NOT NULL,
			`extern` varchar(10) NOT NULL,
			`externUrl` varchar(255) NOT NULL,
			`externUrlPhoto` varchar(255) NOT NULL,
			`event_date` varchar(255) NOT NULL,
			`loginip` varchar(255) NOT NULL,
			`cDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY  (`id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
			
		}
		
		if(empty($installed) or version_compare($installed, '1.1.5','<')) {
			$queries[] =  "ALTER TABLE `#__rhc_rsvp` ADD `checkedin` timestamp NOT NULL ;";
		}
		
		if(empty($installed) or version_compare($installed, '2.1.0','<')) {
			$queries[] =  "ALTER TABLE `#__rhc_rsvp` ADD `multidata` text COLLATE utf8_unicode_ci NOT NULL;";
			$queries[] =  "ALTER TABLE `#__rhc_rsvp` CHANGE `answer` `answer` VARCHAR( 255 ) NOT NULL;";
		}		
		
		if(!empty($queries)){
			foreach($queries as $query){
				$query = str_replace('#__', $wpdb->prefix, $query);
				$wpdb->query($query);
			}	
		}

		if(empty($installed) or version_compare($installed, RHCRSVP_VERSION,'<')) {
			update_option('RHCRSVP_INSTALL',RHCRSVP_VERSION);
		}	
	}	
}

global $rsvp_plugin;

if ( ! $rhc_rsvp_plugin ) {
	$rhc_rsvp_plugin = new Plugin_RHC_RSVP();
}