'use strict';

(function( $ ) {
	var template = null;
	var methods = {
		init: function( options ) {
			return this.each(function() {
				if ( 'undefined' == typeof $( this ).data( 'rhcrsvp' ) && $( this ).find( '#rhc-rsvp-fields-table' ).length > 0 ) {
					if ( ! $( '#rsvp-templates').length ) {
						$( 'BODY' ).append( '<table id="rsvp-templates"></table>' );
					}

					var _this = this;
					var data = {
						template: {}
					};

					data.template.container = $( this ).find( '.rsvp-field-container' ).first().hide().appendTo( '#rsvp-templates' );
					data.template.detail = $( this ).find( '.rsvp-field-detail' ).first().hide().appendTo( '#rsvp-templates' );
					data.template.settings = $( this ).find( '.rsvp-field-settings' ).first().hide().appendTo( '#rsvp-templates' );

					$( this ).data( 'rhcrsvp', data );

					$( this ).find( '.rsvp-add' ).on( 'click', function( e ) {
						return cbRsvpAdd( e, this, _this );
					});

					renderFields( this );
				}
			});
		}
	};

	function toggleButtons() {	
		var haveFields = $( '.rsvp-field-container .rsvp-field-detail' ).length;

		if ( haveFields ) {
			$( '.rsvp-btn-group-top').hide();
			$( '#rhc-rsvp-fields-table' ).show();
		} else {		
			$( '.rsvp-btn-group-top' ).show();	
			$( '#rhc-rsvp-fields-table' ).hide();
		}
	}

	function initSortable() {
		Sortable.create( $( '#rhc-rsvp-fields-table' )[0], {
			handle: '.rsvp-btn-sortable',
			draggable: '.rsvp-field-container',
			animation: 250,			
			onUpdate: function( event ) {
				sortFields( $( event.to ).find( 'input[name="rsvp_choose_id[]"]' ).map( function(){
					return this.value;
				}).get() );
			},
			onChoose: function( event ) {
				$( event.item ).find( '.tooltipster' ).trigger( 'mouseleave' );
			}
		});
	}

	function setProcessing( loading ) {
		if ( loading ) {
			$( '#rhc-rsvp-fields-table').addClass( 'rsvp-loading' );
		} else {
			$( '#rhc-rsvp-fields-table' ).removeClass( 'rsvp-loading' );
		}
	}

	function renderFields( table ) {
		var args = {
			action: 'rhc_rsvp_get_fields',
			post_id: $( '#post_ID' ).val()
		};

		setProcessing( true );

		$.post( ajaxurl, args, function( response ) {
			if ( 'OK' === response.R ) {
				if ( response.DATA ) {
					$.each( response.DATA, function( i, field ) {
						initFieldForm( table, field );
					});
				}

				setProcessing( false );
			}
		}, 'json');

		toggleButtons();
		initSortable();
	}

	function cbRsvpAdd( e, btn, table ) {
		var args = {
			action: 'rhc_rsvp_add_field',
			post_id: $( '#post_ID' ).val(),
			field_type: $( btn ).data( 'type' )
		};

		setProcessing( true );

		$.post( ajaxurl, args, function( response ) {
			if ( 'OK' === response.R ) {
				initFieldForm( table, response.DATA );
				setProcessing( false );
			}
		}, 'json');
	}

	function initFieldForm( table, field, data ) {
		var data = $( table ).data( 'rhcrsvp' );
		var $table = $( table );
		var $container = data.template.container.clone().uniqueId().insertBefore( $table.find( '#rhc-rsvp-fields-table > tfoot' ) );
		
		data.template.detail.clone().appendTo( $container );
		data.template.settings.clone().appendTo( $container );

		$container.find( '.rsvp-btn-settings' ).on( 'click', function() {
			return cbBtnFieldSettings( getClosestContainerElement( this, 'settings' ) );
		});

		$container.find( '.rsvp-btn-delete' ).on( 'click', function() {
			return cbBtnFieldDelete( getClosestContainer( this ), field );
		});

		$container.find( '.tooltipster' ).tooltipster();

		setFieldValues( $container, field );

		$container.find( '.rsvp-input' ).on( 'change', function() {
			return updateField( getClosestContainer( this ) );
		});

		$container.find( '.rsvp-input-color').wpColorPicker({
			change: function( event, ui ) {
				$( this ).val( ui.color.toString() ).trigger( 'change' );
			}
		});

		$container.fadeIn( 1500 );
		toggleButtons();
	}

	function getClosestContainer( element ) {
		return $( element ).closest( 'tbody' );
	}

	function getClosestContainerElement( element, type ) {
		return $( element ).closest( 'tbody' ).find( getContainerElementClassName( type ) );
	}

	function getContainerElementClassName( type ) {
		var className;

		switch ( type ) {
			case 'detail' :
				className = '.rsvp-field-detail';
				break;
			case 'type' : 
				className = '.rsvp-field-type';
				break;
			case 'settings' : 
				className = '.rsvp-field-settings';
				break;
			default :
				return;
		}

		return className;
	}

	function showFieldSettings( $settings ) {
		$settings.hide();
		$settings.slideDown( 'fast', 'swing' );
	}

	function hideFieldSettings( $settings ) {
		$settings.slideUp( 'fast', 'swing' );
	}

	function cbBtnFieldSettings( $settings ) {
		if ( $settings.is( ':visible' ) ) {
			hideFieldSettings( $settings );
		} else {
			showFieldSettings( $settings );
		}
	}

	function cbBtnFieldDelete( $container, field ) {
		setProcessing( true );

		var args = {
			action: 'rhc_rsvp_delete_field',
			post_id:  $( '#post_ID' ).val(),
			field_id: field.choose_id
		};

		$.post( ajaxurl, args, function( response ) {
			if ( 'OK' === response.R ) {
				removeContainer( $container );
				setProcessing( false );
			}
		}, 'json');	
	}

	function removeContainer( $container ) {
		$container.hide();

		$container.fadeOut( 'fast', function() {
			$( this ).remove();
			toggleButtons();
		});
	}

	function setFieldValues( $container, field ) {
		$container.find( 'input[name="rsvp_choose_id[]"]' ).val( field.choose_id );
		$container.find( 'input[name="rsvp_choose_type[]"]' ).val( field.choose_type );

		$container.find( 'input[name="rsvp_choose_text[]"]' ).val( field.choose_text );
		$container.find( 'input[name="rsvp_choose_subtext[]"]' ).val( field.choose_subtext );
		$container.find( 'input[name="rsvp_choose_slug[]"]' ).val( field.choose_slug );
		$container.find( 'input[name="rsvp_choose_limit[]"]' ).val( field.choose_limit );
		
		$container.find( 'input[name="rsvp_normal_color[]"]' ).val( field.normal_color );
		$container.find( 'input[name="rsvp_hover_color[]"]' ).val( field.hover_color );
		$container.find( 'input[name="rsvp_text_color[]"]' ).val( field.text_color );

		if ( '1' == field.choose_allow ) {
			$container.find( 'input[name="rsvp_choose_allow[]"]' ).prop( 'checked', true );
		}
	}

	function updateField( $container ) {
		setProcessing( true );

		var field = getFieldData( $container );
		
		var args = {
			action: 'rhc_rsvp_update_field',
			post_id:  $( '#post_ID' ).val(),
			field_id: field.choose_id,
			field: field
		};

		$.post( ajaxurl, args, function( response ) {
			if ( 'OK' === response.R ) {
				setProcessing( false );
			}
		}, 'json');
	}

	function getFieldData( $container ) {
		var field = {};

		$container.find( '.rsvp-input' ).each( function( i, inp ) {
			var $inp = $( inp );
			var name = $( inp ).attr( 'name' );
			
			name = name.substring( 'rsvp_'.length, name.length - 2 );

			if ( $inp.is( ':radio' ) ) {
				field[ name ] = $inp.is( ':checked' ) ? $inp.val() : ( field[ name ] || 0 );
			} else if( $inp.is( ':checkbox' ) ) {
				field[ name ] = $inp.is( ':checked' ) ? '1' : '';
			} else {
				field[ name ] = $inp.val();
			}
		});

		return field;
	}

	function sortFields( order ) {
		setProcessing( true );

		var args = {
			action: 'rhc_rsvp_sort_fields',
			post_id:  $( '#post_ID' ).val(),
			order: order
		};

		$.post( ajaxurl, args, function( response ) {
			if ( 'OK' === response.R ) {
				setProcessing( false );
			}
		}, 'json');
	}

	$.fn.rhcRsvpMetabox = function( method ) {
		if ( methods[ method ] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ) );
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.rhcRsvpMetabox' );
		}    
	};
})( jQuery );

jQuery( document ).ready(function( $ ) {
	$( '#rhc-rsvp' ).rhcRsvpMetabox();
});