 jQuery(document).ready(function($){
	 
	jQuery( "#rhc-rsvp-custom-fields .choose_colobox, #rhc-rsvp-default-fields .choose_colobox" ).append('<div class="dragpoint"></div>'); 
	 
	 
 	jQuery( "#rhc-rsvp-custom-fields" ).sortable(
 	{
	 	items:'.choose_fields',
	 	handle:'.dragpoint',
	 	stop:function(event,ui){
	 		jQuery( "#rhc-rsvp-custom-fields .choose_fields" ).each(function(tmp_i){
		 		jQuery(this).find('h3').html('Field '+(tmp_i+1));	
		 		
		 		jQuery(this).find('.layout_user_input_label').attr('name','layout_user_input_label_'+tmp_i);
		 		jQuery(this).find('.layout_user_input_slug').attr('name','layout_user_input_slug_'+tmp_i);	
		 		jQuery(this).find('.layout_user_input_type').attr('name','layout_user_input_type_'+tmp_i); 		
		 		
		 		jQuery(this).find('.layout_user_input_check').attr('name','layout_user_input_check_'+tmp_i);
		 		jQuery(this).find('.layout_user_input_main_name').val(tmp_i);	
		 		jQuery(this).find('.layout_user_input_main_email').val(tmp_i);
	 		})
	    }
 	});
 	
 	jQuery( "#rhc-rsvp-default-fields" ).sortable(
 	{
	 	items:'.choose_fields',
	 	handle:'.dragpoint',
	 	stop:function(event,ui){
	 		jQuery( "#rhc-rsvp-default-fields .choose_fields" ).each(function(tmp_i){
		 		jQuery(this).find('h3').html('Button '+(tmp_i+1));	
		 		
		 		jQuery(this).find('.layout_choose_text').attr('name','layout_choose_text_'+tmp_i);
		 		jQuery(this).find('.layout_choose_subtext').attr('name','layout_choose_subtext_'+tmp_i);	
		 		jQuery(this).find('.layout_choose_slug').attr('name','layout_choose_slug_'+tmp_i); 		
		 		
		 		jQuery(this).find('.layout_choose_limit').attr('name','layout_choose_limit_'+tmp_i);
		 		jQuery(this).find('.layout_normal_color').attr('name','layout_normal_color_'+tmp_i);
		 		jQuery(this).find('.layout_hover_color').attr('name','layout_hover_color_'+tmp_i);
		 		jQuery(this).find('.layout_text_color').attr('name','layout_text_color_'+tmp_i);
		 		
		 		jQuery(this).find('.layout_choose_allow').attr('name','layout_choose_allow_'+tmp_i);
	 		})
	    }
 	});
 });