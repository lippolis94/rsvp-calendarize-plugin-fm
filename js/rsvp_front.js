var rsvp_lockdown = false;
var connections_type_rsvp = new uh_social_connection();


setTimeout(function(){
	jQuery(window).resize(function(){ rhc_rsvp_init(); });
},10);

jQuery(document).ready(function($){
	
	jQuery('.cit_rsvp .cit_rsvpselect li').mouseout(function(){
		if(!jQuery(this).hasClass('disabled') && !jQuery(this).hasClass('active')){
			jQuery(this).css('background-color',jQuery(this).attr('data-normal-color'));
		}	
	});
	
	jQuery('.cit_rsvp .cit_rsvpselect li').mouseover(function(){
		if(!jQuery(this).hasClass('disabled')){
			jQuery(this).css('background-color',jQuery(this).attr('data-hover-color'));
		}	
	});	
	
	jQuery('.cit_rsvp .cit_rsvpselect li').click(function(){
		if(rsvp_lockdown == false && !jQuery(this).hasClass('disabled')){
			jQuery('.cit_rsvp .cit_rsvpselect li.active').css('background-color',jQuery('.cit_rsvp .cit_rsvpselect li.active').attr('data-normal-color')).removeClass('active');
			jQuery('.cit_rsvp .cit_rsvpselect .rsvp_dropbox').removeClass('fui-check');
			
			jQuery(this).find('.rsvp_dropbox').addClass('fui-check');
			
			jQuery(this).addClass('active').css('background-color',jQuery(this).attr('data-hover-color'));
		}
	})
	
	
	setTimeout(rhc_rsvp_update_commentbox, 100);

	

	rhc_rsvp_init();
	
	jQuery('[data-toggle="checkbox"]').each(function () {
	  jQuery(this).checkbox();
	});
	
	
	jQuery('[data-toggle="tooltip"]').each(function () {
	  jQuery(this).tooltip({ show: 500, hide: 999000000 });
	});
	
	
	jQuery('.cit_rsvp .close').click(function () {
	  	jQuery(this).parent().parent().fadeTo( "slow" , 0.0, function(){
			  jQuery(this).hide();
	  	});
	});
});

function rhc_rsvp_update_commentbox(){
		tmp_count_choose = jQuery('#rhc_box_rsvp1 .cit_rsvpselect .nav.nav-list').innerHeight();
		tmp_count_info = jQuery('#rhc_box_rsvp2 .rsvp_infobox .rsvp_manual').outerHeight()+jQuery('#rhc_box_rsvp2 .rch_h2').outerHeight();
	
		if(parseInt(tmp_count_choose-tmp_count_info) < 50){
			if(jQuery('#rhc_box_rsvp2 .rsvp_infobox .comment_specialbox').html()!=''){
				jQuery('.rhc-info-cell .comment_specialbox_extra').html(jQuery('#rhc_box_rsvp2 .rsvp_infobox .comment_specialbox').html());
				jQuery('#rhc_box_rsvp2 .rsvp_infobox .comment_specialbox').html('');
			}
			
		} else {
			jQuery('#rhc_box_rsvp2 .rsvp_infobox .comment_specialbox textarea').css('height',(tmp_count_choose-tmp_count_info-45)+'px');
			if(jQuery('.rhc-info-cell .comment_specialbox_extra').html()!=''){
				jQuery('#rhc_box_rsvp2 .rsvp_infobox .comment_specialbox').html(jQuery('.rhc-info-cell .comment_specialbox_extra').html());
				jQuery('.rhc-info-cell .comment_specialbox_extra').html('');
			}
		}
}

var rhc_rsvp_changescreen = 0;
function rhc_rsvp_init(){
	
	rhc_rsvp_update_commentbox();
	
	var tmp = jQuery('.cit_rsvp').width();
	if(rhc_rsvp_changescreen != 1 && tmp <= 900){
		rhc_rsvp_changescreen = 1;
		jQuery('.cit_rsvp #rhc_box_rsvp1').removeClass('span4').addClass('span6');
		jQuery('.cit_rsvp #rhc_box_rsvp2').removeClass('span8').addClass('span6');
	} else if(rhc_rsvp_changescreen != 2  && tmp >= 900){
		rhc_rsvp_changescreen = 2;
		jQuery('.cit_rsvp #rhc_box_rsvp1').removeClass('span6').addClass('span4');
		jQuery('.cit_rsvp #rhc_box_rsvp2').removeClass('span6').addClass('span8');
		
	}

}


function rsvp_list_show_more(tmp_this){
	
	if(jQuery(tmp_this).parent().find('.show_more_object').is(':visible')){
		jQuery(tmp_this).parent().find('.show_more_object').hide();
		jQuery(tmp_this).html(jQuery(tmp_this).attr('data-texta'));
	} else {
		jQuery(tmp_this).parent().find('.show_more_object').show();
		jQuery(tmp_this).html(jQuery(tmp_this).attr('data-textb'));
	}
	
};

function rsvp_error_alert(tmp_msg){
	jQuery('.cit_rsvp .rsvp_alert_dialog').html('<div class="alert alert-error"><button type="button" class="close fui-cross" data-dismiss="alert"></button><div class="text">'+tmp_msg+'</div></div>')
	jQuery('.cit_rsvp .rsvp_alert_dialog').css({ opacity: 0.0 }).show().fadeTo( "slow" , 1.0);
}

function rsvp_checkText(tmpdata,objeck){
	if((typeof(tmpdata) === 'undefined') || tmpdata == undefined || tmpdata == 'undefined' || tmpdata == '' || tmpdata.length < 2){
		rsvp_error_alert(rsvp_vars.pleaseValidateText );
		jQuery(objeck).focus();
		return false;
	}
	return true
}




function rsvp_checkEmail(tmpdata,objeck){
	var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
	
	if(typeof tmpdata == 'undefined' || tmpdata == undefined || tmpdata == 'undefined' || tmpdata == '' || tmpdata.length < 2 || !pattern.test(tmpdata) ){
		rsvp_error_alert(rsvp_vars.pleaseValidateEmail);
		jQuery(objeck).focus();
		return false;
	}	
	
	return true;
	
}

function rsvp_checkTlf(tmpdata,objeck){
	var pattern = new RegExp(/^[0-9 \+]+$/i);
	
	if(typeof tmpdata == 'undefined' || tmpdata == undefined || tmpdata == 'undefined' || tmpdata == '' || tmpdata.length < 2 || !pattern.test(tmpdata) ){
		rsvp_error_alert(rsvp_vars.pleaseValidateTlf);
		jQuery(objeck).focus();
		return false;
	}	
	return true;
}

function rsvp_get_info_objects(do_not_check_value){
	var object = new Object();	

	jQuery('.cit_rsvp .rsvp_infobox input.rsvp_info_input').each(function(){
		if(jQuery(this).attr('data-type') == 'text' && jQuery(this).attr('data-must') == true && do_not_check_value == false){
			if(!rsvp_checkText(jQuery(this).val(),this)){
				rsvp_lockdown = false;
				return false;
			}
		}
		if(jQuery(this).attr('data-type') == 'email' && jQuery(this).attr('data-must') == true && do_not_check_value == false){
			if(!rsvp_checkEmail(jQuery(this).val(),this)){
				rsvp_lockdown = false;
				return false;
			}
		}		
		if(jQuery(this).attr('data-type') == 'tlf' && jQuery(this).attr('data-must') == true && do_not_check_value == false){
			if(!rsvp_checkTlf(jQuery(this).val(),this)){
				rsvp_lockdown = false;
				return false;
			}
		}	
			
		tmpslug = jQuery(this).attr('data-slug');
		object[tmpslug] = jQuery(this).val();			
	});	
	
	return object;
}

function rsvp_next( postID ) {
	if ( false == rsvp_lockdown ) {
		rsvp_lockdown = true;
		
		object = rsvp_get_info_objects( false );
	
		if ( rsvp_lockdown ) {
			rsvp_social_connection_done( object, postID, 'manual' );
		}
	}

	return false;
}

function rsvp_social_connection(postID,socialtype){
	if(rsvp_lockdown == false){
		if(socialtype == 'twitter' || socialtype == 'wp'){
			var object = new Object();
			object.postID = postID;
			object.answer = jQuery('.cit_rsvp .cit_rsvpselect li.active').attr('data-slug');
			if(typeof(rsvp_vars.event_rdate) !== 'undefined' && rsvp_vars.event_rdate != '' && rsvp_vars.event_rdate != ',' && rsvp_vars.event_rdate != ' ,' && rsvp_vars.event_rdate != ' , ' && rsvp_vars.event_rdate != ', '){
				object.event_rdate = rsvp_vars.event_rdate
			}
			connections_type_rsvp.loginby(socialtype,'rsvp_social_connection_done',object)
		} else {
			connections_type_rsvp.loginby(socialtype,'rsvp_social_connection_done',postID)
		}

		return false;
	}
}

function rsvp_social_connection_done(tmpdata,postID,datatype) {
	var object = new Object();
	object.rsvp_fields = new Object(); 
	object.rsvp_fields = rsvp_get_info_objects(true);	
	object.rsvp_answer = jQuery('.cit_rsvp .cit_rsvpselect li.active').attr('data-slug');
	object.comment = jQuery('.cit_rsvp #rsvp_comment').val();	
	
	
	if(jQuery('.cit_rsvp #rsvp_sendemail').is(':checked')){
		object.rsvp_sendemail = 1;
	} else {
		object.rsvp_sendemail = 0;
	}
	
	if(object.rsvp_answer == undefined || object.rsvp_answer == 'undefined'){
		rsvp_error_alert(rsvp_vars.pleaseAttend);
		rsvp_lockdown = false;
		return false;
	}
	
	if(jQuery('.cit_rsvp .cit_rsvpselect li.active').hasClass('disabled')){
		rsvp_error_alert(rsvp_vars.limitReached);
		rsvp_lockdown = false;
		return false;
	}	
	
	
	
	if(datatype =='manual'){
		object.rsvp_extern = 'MANUAL';	
	} else if(datatype =='facebook'){
		if(typeof (tmpdata.error)  != 'undefined'){
			if(typeof (tmpdata.error.code) != 'undefined'){
				rhc_rating_social_connect(postID,'facebook');
				rsvp_lockdown = false;
				return false;
			}
		}
		
		object.rsvp_firstname = tmpdata.first_name;	
		object.rsvp_lastname = tmpdata.last_name;
		
		if(typeof(tmpdata.email)  != 'undefined'){
			object.rsvp_email = tmpdata.email;
		} else {
			object.rsvp_email = tmpdata.id+'@facebook.com';
		}
		object.rsvp_extern = 'FACEBOOK';	
		object.rsvp_externUrl = tmpdata.link;	
		object.rsvp_externUrlPhoto = tmpdata.picture.data.url;
	} else if(datatype =='googleplus') {
		object.rsvp_firstname = tmpdata.name.givenName;	
		object.rsvp_lastname = tmpdata.name.familyName;
		object.rsvp_email = tmpdata.id+'@googleplus';
		object.rsvp_extern = 'GOOGLEPLUS';	
		object.rsvp_externUrl = tmpdata.url;	
		object.rsvp_externUrlPhoto = tmpdata.image.url.split("?")[0];
	} else if(datatype =='linkedin') {
		object.rsvp_firstname = tmpdata.firstName;	
		object.rsvp_lastname = tmpdata.lastName;
		object.rsvp_email = tmpdata.id+'@linkedin';
		object.rsvp_extern = 'LINKEDIN';	
		object.rsvp_externUrl = tmpdata.publicProfileUrl;	
		object.rsvp_externUrlPhoto = tmpdata.pictureUrl;
	} else if(datatype =='twitter') {
		object.rsvp_firstname = tmpdata.firstName;	
		object.rsvp_lastname = tmpdata.lastName;
		object.rsvp_email = '@'+tmpdata.screen_name;
		object.rsvp_extern = 'TWITTER';	
		object.rsvp_externUrl = 'https://twitter.com/'+tmpdata.screen_name;	
		object.rsvp_externUrlPhoto = tmpdata.profile_image_url;
	} else if(datatype =='wp') {
		object.rsvp_firstname = tmpdata.firstname;	
		object.rsvp_lastname = tmpdata.lastname;
		object.rsvp_email = tmpdata.email;
		object.rsvp_extern = 'WP';	
		object.rsvp_externUrl = '';	
		object.rsvp_externUrlPhoto = '';
	}

	object.action = 'rsvp_saveanswer';
	object.rsvp_postID = postID;
	object.organizer = rsvp_vars.organizer;
	object.venue = rsvp_vars.venue;
	object.event_rdate = rsvp_vars.event_rdate;

	jQuery('.cit_rsvp .fc-view-loading').css({ opacity: 0.0 }).show().fadeTo( "slow" , 1.0);

	jQuery.post( rsvp_vars.ajaxurl, object, function( data ) {
		rsvp_operatedata_data( data );
			
		if ( datatype == 'facebook' && data.FBPUB && ( data.AnswerAllow == 'true' || data.AnswerAllow == true || data.AnswerAllow == 1 || data.AnswerAllow == '1' ) ) {
			FB.ui({
				method: 'feed',
				name: data.FBTITLE,
				link: data.LINK,
				picture: data.THUMBNAIL,
				caption: data.FBSUBTITLE,
				description: data.FBTEXT
			});	
		}
	}, 'json' );
}


function rsvp_hide_attending(){
		jQuery('.cit_rsvp .rsvp_attending_list_bg').css({ opacity: 1.0 }).show().delay(500).fadeTo( "slow" , 0.0, function(){
			jQuery('.cit_rsvp .rsvp_attending_list_bg').hide();
		});
		jQuery('.cit_rsvp .rsvp_attending_list_inner').css({ opacity: 1.0 }).show().fadeTo( "slow" , 0.0);
}

function rsvp_change_attending( tmp_this, tmp_ID, tmp_status ) {
	jQuery( document ).ready( function( $ ) {		
		$( tmp_this ).parent().find( 'a' ).removeClass( 'active' );
		$( tmp_this ).addClass( 'active' );
		
		$( tmp_this ).parent().parent().parent().parent().parent().find( '.rsvp_attending_checkin_date' ).html( '...' );
		
		var data = {
			'action': 'rsvp_change_attending',
			'rsvp_ID': tmp_ID,
			'rsvp_status': tmp_status,
			'tmp_this': ''
		};

		$.post( rsvp_vars.ajaxurl, data, function( response ) {
			$( tmp_this ).parent().parent().parent().parent().parent().find( '.rsvp_attending_checkin_date' ).html( response.DATESTR );
		}, 'json');
	});
}

function rsvp_show_attending( postID, event_rdate ) {
	jQuery( document ).ready(function( $ ) {
		$( '.cit_rsvp .rsvp_attending_main_data_container' ).html( '' );
		$( '.cit_rsvp .rsvp_attending_list_bg' ).css({ opacity: 0.0 }).show().fadeTo( 'slow' , 1.0 );
		$( '.cit_rsvp .rsvp_attending_list_inner' ).css({ opacity: 0.0 }).show().delay( 500 ).fadeTo( 'slow', 1.0 );

		var data = {
			'action': 'retrive_attendinglist',
			'rsvp_postID': postID,
			'event_rdate': event_rdate
		}

		$.post( rsvp_vars.ajaxurl, data, function( response ) {
			$( '.cit_rsvp .rsvp_attending_main_data_container' ).css({ opacity: 0.0 }).html( response.RETURNVALUE ).fadeTo( 'slow' , 1.0 );
		}, 'json');
	});
}

function rsvp_operatedata_data(data){
	if(data.R=='DONE'){
		jQuery('.cit_rsvp .rsvp_infobox input.rsvp_info_input').val('');
		jQuery('.cit_rsvp #rsvp_comment').val('');
	
		jQuery('.cit_rsvp .fc-view-loading').hide();
		jQuery('.cit_rsvp .rsvp_success_dialog .text').html(data.MSG);
		jQuery('.cit_rsvp .rsvp_success_dialog').css({ opacity: 0.0 }).show().fadeTo( "slow" , 1.0);

		if(typeof(data.CHANGE) !== 'undefined' && data.CHANGE != '' && data.CHANGE != 'undefined'){
			if(typeof(data.REMOVEID) !== 'undefined' && data.REMOVEID != '' && data.REMOVEID != 'undefined'){
				jQuery('#rsvp_scroll_attending .rsvp_scroll .avatarid'+data.REMOVEID).remove();
			}
			

			
			jQuery('.cit_rsvp .cit_rsvpselect li[data-slug="'+data.CHANGEANSWER+'"] .rsvp_number').html(parseFloat(parseFloat(jQuery('.cit_rsvp .cit_rsvpselect li[data-slug="'+data.CHANGEANSWER+'"] .rsvp_number').html())-1));
		
			if(parseFloat(jQuery('.cit_rsvp .cit_rsvpselect li[data-slug="'+data.CHANGEANSWER+'"] .rsvp_number').html()) < parseFloat(jQuery('.cit_rsvp .cit_rsvpselect li[data-slug="'+data.CHANGEANSWER+'"]').attr('data-limit')) && parseFloat(jQuery('.cit_rsvp .cit_rsvpselect li[data-slug="'+data.CHANGEANSWER+'"]').attr('data-limit')) != 0){
				jQuery('.cit_rsvp .cit_rsvpselect li[data-slug="'+data.CHANGEANSWER+'"]').removeClass('disabled');
				jQuery('.cit_rsvp .cit_rsvpselect li[data-slug="'+data.CHANGEANSWER+'"] .rsvp_solgout').remove();
			}
		
		
		
			jQuery('.cit_rsvp .cit_rsvpselect .active .rsvp_number').html(parseFloat(parseFloat(jQuery('.cit_rsvp .cit_rsvpselect .active .rsvp_number').html())+1));
		} else {
			jQuery('.cit_rsvp .cit_rsvpselect .active .rsvp_number').html(parseFloat(parseFloat(jQuery('.cit_rsvp .cit_rsvpselect .active .rsvp_number').html())+1));
		}

		if(typeof(data.AVATAR) !== 'undefined' && data.AVATAR != '' && data.AVATAR != 'undefined'){
			jQuery('.cit_rsvp .rsvp_scroll').append(data.AVATAR);
			jQuery('.cit_rsvp #rsvp_scroll_attending').show();
		}	
		
		if(parseFloat(jQuery('.cit_rsvp .cit_rsvpselect li.active .rsvp_number').html()) >= parseFloat(jQuery('.cit_rsvp .cit_rsvpselect li.active').attr('data-limit')) && parseFloat(jQuery('.cit_rsvp .cit_rsvpselect li.active').attr('data-limit')) != 0){
			jQuery('.cit_rsvp .cit_rsvpselect li.active').addClass('disabled');
		}
		

		jQuery('.cit_rsvp .cit_rsvpselect li').removeClass('active');
		rsvp_lockdown = false;
		
		return false;
	} else {
		jQuery('.cit_rsvp .fc-view-loading').hide();
		rsvp_error_alert(data.MSG);
		rsvp_lockdown = false;
		return false;
	}
}