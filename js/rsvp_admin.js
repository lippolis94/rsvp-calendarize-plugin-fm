function cit_rsvp_createbutton(tmp_text,tmp_default){
	jQuery(document).ready(function($){
		if(tmp_default == undefined || tmp_default == ''){
			tmp_default = 0;
		}
		
		tmp_buttom = '<div class="pt-option pt-option-onoff pt-option-onoff-rsvp ">';
		tmp_buttom += 	'<span class="pt-label pt-type-onoff">'+tmp_text+'</span>';
		tmp_buttom += 	'<div class="pop-onoff-control">';
		tmp_buttom += 		'<input type="hidden" value="'+tmp_default+'" name="enable_rsvb_box" id="enable_rsvb_box">';
		tmp_buttom += 		'<div class="pop-onoff">';
		tmp_buttom += 			'<button rel="enable_rsvb_box" value="1" class="pop-onoff-btn';
		if(tmp_default == '1' || tmp_default == 1 || tmp_default == 'true' || tmp_default == true){
			tmp_buttom += ' checked';	
		}
		tmp_buttom += 			'" type="button">On</button>';
		tmp_buttom += 			'<button rel="enable_rsvb_box" value="0" class="pop-onoff-btn';
		
		if(tmp_default == '0' || tmp_default == 0 || tmp_default == 'false' || tmp_default == false || tmp_default == '' || tmp_default == undefined || tmp_default == 'undefined'){
			tmp_buttom += ' checked';	
		}
		
		tmp_buttom += 			'" type="button">Off</button>';
		tmp_buttom += 	'</div></div></div>';

	
		jQuery(tmp_buttom).insertBefore('#rhc-layout-options .inside .pt-option:last');
		
		jQuery('.pt-option-onoff-rsvp button').on('click',function(e){
			$(this).parent().find('button').removeClass('checked');
			$(this).addClass('checked');
			
			$(this).parent().parent().find('input[type=hidden]').val( $(this).attr('value') );
		});
	})
}